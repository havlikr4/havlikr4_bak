using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Base for an object which can spawn another object on certain conditions.
 */
public abstract class Spawnpoint : NetworkBehaviour
{
    [Header("Object to spawn")]
    [SerializeField] public GameObject spawn;

    [Header("Cooldown between spawns")]
    [SerializeField] private float cooldown;

    [Header("Position of spawn relative to spawnpoint")]
    [SerializeField] private Vector3 spawnPosition;


    // Helper properties
    private float timeSinceLast = 0;
    public GameObject lastSpawn;
    private Vector3 spawnScale;

    // Customizable spawn condition
    public abstract bool NewSpawnCondition();

    // Customizable initialization
    public abstract void InitializeSelf();

    // Customizable method called after spawning
    public abstract void AfterSpawn();
    
    // Initialization
    void Start()
    {
        InitializeSelf();

        spawnScale = spawn.transform.localScale;
        SpawnAction();
    }

    // Checks the condition and if it passes, spawns an object
    void Update()
    {
        if(NewSpawnCondition())
        {
            timeSinceLast += Time.deltaTime;
            if (timeSinceLast >= cooldown)
            {
                SpawnAction();
            }
        } 
    }

    // Spawns the spawnobject on server and clients
    public void SpawnAction()
    {
        lastSpawn = Instantiate(spawn);
        lastSpawn.SetActive(true);
        lastSpawn.transform.position = transform.TransformPoint(spawnPosition);
        lastSpawn.transform.localScale = spawnScale;

        NetworkServer.Spawn(lastSpawn);

        timeSinceLast = 0;

        AfterSpawn();
    }
}
