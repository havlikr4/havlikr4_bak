using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Simple spawnpoint which spawns a new object after the last one is no longer active.
 */
public class SpawnpointActive : Spawnpoint
{
    public override void AfterSpawn()
    {

    }

    public override void InitializeSelf()
    {

    }

    public override bool NewSpawnCondition()
    {
        return !lastSpawn.activeSelf;
    }

}
