using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ReactionCharacterManager;

/*
 * Spawnpoint which spawns new object after the old one is far enough.
 * Can spawn a variety of objects randomly from a list from a SpawnerManager.
 */
public class SpawnpointPosition : Spawnpoint
{
    [Header("Distance of last spawn to spawn new one")]
    [SerializeField]
    public float maxDist = 0.5f;

    [Header("Spawner Manager used to get a list of potential spawns")]
    [SerializeField]
    SpawnerManager spawnerManager;

    // List of all objects, which can be spawned
    List<GameObject> potentialSpawns = new List<GameObject>();

    // Randomizes the next spawn object after spawning
    public override void AfterSpawn()
    {
        int randomIndex = UnityEngine.Random.Range(0, potentialSpawns.Count);
        spawn = potentialSpawns[randomIndex];
    }

    // Gets the potential spawn list from its Spawn manager
    public override void InitializeSelf()
    {
        if(spawnerManager == null) { gameObject.SetActive(false); }

        potentialSpawns = spawnerManager.spawns;

        int randomIndex = UnityEngine.Random.Range(0, potentialSpawns.Count);
        spawn = potentialSpawns[randomIndex];

    }

    // Checks if the last spawn is beyond a set distance
    public override bool NewSpawnCondition()
    {
        float dist = Mathf.Abs(Vector3.Distance(lastSpawn.transform.position, transform.position));

        return dist > maxDist;
    }
}
