using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Simple component used for changing Object Brush modes.
 * Object Brush ray can hit an object with this component and on user input change the coloring mode.
 */
public class ColoringModeSwitcher : MonoBehaviour
{
    // Tag
    public const string ObjectTag = "ColoringModeSwitcher";

    [Header("Coloring mode set on activation")]
    [SerializeField]
    public ObjectBrush.ColoringModes coloringMode;

    // Initialization
    void Start()
    {
        gameObject.tag = ObjectTag;
    }
}
