using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

/*
 * Component used for getting color value from rayhit on RGB wheel.
 */
public class ColorPalette : MonoBehaviour
{
    // Palette Transform Settings
    [SerializeField]
    private float radius = 5f;
    [SerializeField]
    private Vector3 XBase = new Vector3(1, 0, 0);
    [SerializeField]
    private Vector3 YBase = new Vector3(0, 0, 1);

    // Plane space base vectors
    private Vector3 BaseXVec;
    private Vector3 BaseYVec;

    [Header("Static value for V in HSV")]
    [SerializeField]
    private float componentV = 0.8f;

    // Tag
    public const string ObjectTag = "Palette";

    // Initialization
    void Start()
    {
        gameObject.tag = ObjectTag; 
    }

    // Calculates base vectors for current transformation
    void Update()
    {
        BaseXVec = (gameObject.transform.rotation * XBase).normalized;
        BaseYVec = (gameObject.transform.rotation * YBase).normalized;
    }

    // Calculates color value from a position on the RGB wheel
    public Color GetColorFromPosition(Vector3 position)
    {
        // Calculates the coordinates in the plane space
        Vector3 center = transform.position;
        Vector3 positionInPlane3D = position - center;
        Vector2 planeCoords = new Vector2(Vector3.Dot(BaseXVec, positionInPlane3D), Vector3.Dot(BaseYVec, positionInPlane3D));

        // Calculates HSV values from the coordinates
        float H, S, V;
        float angle = Mathf.Atan2(planeCoords.x, planeCoords.y) * Mathf.Rad2Deg;
        if(angle < 0) angle += 360;

        H = Mathf.Clamp01(angle / 360);
        S = Mathf.Clamp01(Vector2.Distance(Vector2.zero, planeCoords) / radius);
        V = componentV;

        return Color.HSVToRGB(H, S, V);
    }
}
