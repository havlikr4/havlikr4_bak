using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

/*
 * Raycasting component used for coloring objects, drawing lines and erasing them.
 * Attached to users hand. Controlled by user input.
 * Has 3 modes changing the action on user inputL:
 * fill (colors whole object), line (draws a line) and eraser (erases drawn lines).
 * Modes can be changed by Color Mode Swicher.
 * Color of coloring and drawing can be changed by Color Palette.
 */
[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(Loggable))]
public class ObjectBrush : MonoBehaviour
{
    [Header("Ray Specification")]
    [SerializeField]
    private Vector3 rayDirection = Vector3.forward;
    [SerializeField]
    private float rayMaxLenght = 30f;

    [Header("Activation Input Action")]
    [SerializeField]
    private InputActionReference activationInput;
    float lastinputValue;

    // Raycast properties
    bool hit;
    RaycastHit rayHit;
    Vector3 rayStart;
    Vector3 rayEnd;

    // Renderers
    public LineRenderer lineRenderer;
    public MeshRenderer meshRenderer;

    // Main Color property
    public Color myColor;

    // Helper properties
    ColorableObject lastSelected;
    private bool changedLast = false;
    private GameObject currentObject;
    private LineRenderer currentDrawing;
    private int lineIndex;
    [HideInInspector]
    public bool drawing = false;
    [HideInInspector]
    public bool power = true;

    [Header("Visual highlight value")]
    [SerializeField]
    private float visualHighlighAmount = 0.5f;

    [Header("Width of brush lines")]
    [SerializeField] 
    private float brushWidth = 0.08f;

    [Header("Mode representation objects")]
    [SerializeField]
    List<GameObject> modeGizmos;

    // Logging
    Loggable loggable;

    // Coloring mode properties
    public enum ColoringModes
    {
        fill,
        line,
        eraser
    }
    public ColoringModes myMode = ColoringModes.fill;

    // Setters
    public void SetMode(ColoringModes mode)
    {
        myMode = mode;
        int modeIndex = (int)myMode;

        for (int i = 0; i < modeGizmos.Count; i++)
        {
            if (modeIndex == i)
            {
                modeGizmos[i].SetActive(true);
            }
            else
            {
                modeGizmos[i].SetActive(false);
            }
        }
    }

    public void SwitchMode()
    {
        ColoringModes newMode = myMode + 1;

        if ((int)newMode == Enum.GetNames(typeof(ColoringModes)).Length)
        {
            newMode = (ColoringModes)0;
        }

        SetMode(newMode);
    }

    public void SetPower(bool newPower)
    {
        power = newPower;
    }

    // Initialization
    private void OnEnable()
    {
        InitializeProperties();
        StartCoroutine(InitializeMode());
    }

    void Start()
    {
        InitializeProperties();
        StartCoroutine(InitializeMode());
    }

    private void InitializeProperties()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = 2;

        meshRenderer = GetComponent<MeshRenderer>();

        lastinputValue = 0;

        lastSelected = null;

        myColor = meshRenderer.material.color;


        loggable = gameObject.GetComponent<Loggable>();
    }

    IEnumerator InitializeMode()
    {
        yield return null;

        SetMode(myMode);

        foreach (GameObject gizmo in modeGizmos)
        {
            MeshRenderer gizmoRenderer = gizmo.GetComponent<MeshRenderer>();
            if (gizmoRenderer != null)
            {
                gizmoRenderer.material = meshRenderer.material;
            }
        }
    }

    // Update
    void Update()
    {
        if (!power)
        {
            lineRenderer.enabled = false;
            return;
        }
        else
        {
            lineRenderer.enabled = true;
        }

        HandleRayCast();

        HandleLineRendering();

        if (myMode != ColoringModes.eraser) HandleColorableHit();

        if (activationInput != null) HandleInput();

        HandleColors();

        HandleDrawing();
    }

    // Handles raycasting, ignores drawings in coloring and drawing mode
    void HandleRayCast()
    {
        rayStart = gameObject.transform.position;
        rayEnd = gameObject.transform.position + (gameObject.transform.rotation * rayDirection * rayMaxLenght);

        LayerMask layerMask = ~0;
        layerMask -= Physics.IgnoreRaycastLayer;

        if (myMode != ColoringModes.eraser) layerMask -= LayerMask.GetMask("Drawing");

        hit = Physics.Raycast(rayStart, gameObject.transform.rotation * rayDirection, out rayHit, rayMaxLenght, layerMask);
    }

    // Handles raycast line rendering
    void HandleLineRendering()
    {
        lineRenderer.SetPosition(0, rayStart);

        if (hit)
        {
            lineRenderer.SetPosition(1, rayHit.point);
        }
        else
        {
            lineRenderer.SetPosition(1, rayEnd);
        }
    }

    // Sets same color for all renderers
    void HandleColors()
    {
        meshRenderer.material.color = myColor;
        lineRenderer.material.color = myColor;
        lineRenderer.startColor = myColor;
        lineRenderer.endColor = myColor;
    }

    // Handles hitting a colorable object - highlighting it if in coloring mode
    void HandleColorableHit()
    {
        changedLast = false;

        if (hit && rayHit.collider.gameObject.CompareTag(ColorableObject.ObjectTag))
        {
            ColorableObject colorableObject = rayHit.collider.gameObject.GetComponent<ColorableObject>();

            if (lastSelected == null)
            {
                lastSelected = colorableObject;
                if(myMode == ColoringModes.fill) lastSelected.highlightable.Lighten(visualHighlighAmount);
            }
            else if (colorableObject != lastSelected)
            {
                if (myMode == ColoringModes.fill) lastSelected.highlightable.Darken(visualHighlighAmount);
                lastSelected = colorableObject;
                if (myMode == ColoringModes.fill) lastSelected.highlightable.Lighten(visualHighlighAmount);
                changedLast = true;
            }
        }
        else if (lastSelected != null)
        {
            if (myMode == ColoringModes.fill) lastSelected.highlightable.Darken(visualHighlighAmount);
            lastSelected = null;
        }

    }

    // Handles user input
    void HandleInput()
    {
        float inputValue = activationInput.action.ReadValue<float>();
        if (inputValue > 0.5f && lastinputValue < 0.5f && hit) { ObjectHit(); }
        lastinputValue = inputValue;
    }

    // Handles line drawing - start, new point and end
    void HandleDrawing()
    {
        if (myMode == ColoringModes.line && hit && drawing && lastSelected != null && !changedLast)
        {
            Vector3 hitOnTop = rayHit.point + ((rayStart - rayHit.point).normalized * 0.01f);

            Draw(hitOnTop);
        }
        else
        {
            EndLineDrawing();
        }
    }

    // Handles user input while hitting an object 
    void ObjectHit()
    {
        // ERASER
        if (myMode == ColoringModes.eraser && rayHit.collider.gameObject.CompareTag("Drawing"))
        {
            rayHit.collider.gameObject.SetActive(false);

            loggable.LogEvent(LoggingGlobs.EventKeys.erase);
        }

        // MODE SWITCH
        if (rayHit.collider.gameObject.CompareTag(ColoringModeSwitcher.ObjectTag))
        {
            ColoringModeSwitcher switcher = rayHit.collider.gameObject.GetComponent<ColoringModeSwitcher>();
            if(switcher != null) SetMode(switcher.coloringMode);

        }

        // DRAWING AND COLORING
        if (lastSelected != null)
        {
            switch (myMode)
            {
                // COLORING
                case ColoringModes.fill:
                    lastSelected.highlightable.Darken(visualHighlighAmount);
                    lastSelected.ChangeColor(meshRenderer.material.color);
                    lastSelected.highlightable.Lighten(visualHighlighAmount);

                    loggable.LogEvent(LoggingGlobs.EventKeys.color);

                    break;

                // DRAWING
                case ColoringModes.line:
                    drawing = !drawing;
                    break;

                default: break;
            }

        } // PALETE
        else if (rayHit.collider.gameObject.CompareTag(ColorPalette.ObjectTag))
        {
            ColorPalette colorPalette = rayHit.collider.gameObject.GetComponent<ColorPalette>();
            if (colorPalette != null)
            {
                myColor = colorPalette.GetColorFromPosition(rayHit.point);
            }
        }
    }

    // Handles line creation and adding a new point to it
    private void Draw(Vector3 drawPosition)
    {
        if (currentDrawing == null)
        {
            lineIndex = 0;
            currentObject = new GameObject();
            currentDrawing = currentObject.AddComponent<LineRenderer>();
            currentDrawing.material = new Material(lineRenderer.material);
            currentDrawing.material.color = myColor;
            currentDrawing.startWidth = currentDrawing.endWidth = brushWidth;
            currentDrawing.positionCount = 1;
            currentDrawing.SetPosition(0, drawPosition);

            currentDrawing.gameObject.tag = "Drawing";
            currentDrawing.gameObject.layer = LayerMask.NameToLayer("Drawing");
        }
        else
        {
            var currentPos = currentDrawing.GetPosition(lineIndex);
            if (Vector3.Distance(currentPos, drawPosition) > 0.01f)
            {
                lineIndex++;
                currentDrawing.positionCount = lineIndex + 1;
                currentDrawing.SetPosition(lineIndex, drawPosition);
            }
        }
    }

    // Ends the current line and bakes it
    private void EndLineDrawing()
    {
        if (currentDrawing != null)
        {
            MeshCollider lineMeshCollider = currentObject.AddComponent<MeshCollider>();

            Mesh lineMesh = new Mesh();
            currentDrawing.BakeMesh(lineMesh, true);
            lineMeshCollider.sharedMesh = lineMesh;

            currentObject = null;
            currentDrawing = null;

            loggable.LogEvent(LoggingGlobs.EventKeys.draw);
        }
    }

}
