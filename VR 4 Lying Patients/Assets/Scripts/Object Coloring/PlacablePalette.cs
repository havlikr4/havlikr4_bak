using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

/*
 * Raycasting component used for placing a palette object used for Object Brush changing.
 * Cast ray from users hand.
 * Places the palette on rayhit position on user input.
 */
[RequireComponent(typeof(LineRenderer))]
public class PlacablePalette : MonoBehaviour
{
    [Header("Object to place")]
    [SerializeField]
    private GameObject paletteObject;

    [Header("Activation Input Action")]
    [SerializeField]
    private InputActionReference placeInput;
    float lastinputValue;

    [Header("Ray Settings")]
    [SerializeField]
    private Vector3 rayDirection = Vector3.forward;
    [SerializeField]
    private float rayMaxLenght = 30f;

    [Header("Palette Settings")]
    [SerializeField]
    private float sizeAtMinDist = 0.8f;
    [SerializeField]
    private float sizeAtMaxDist = 3.0f;
    [SerializeField]
    private float positionOffset = 0.01f;

    // Line rendering properties
    public LineRenderer lineRenderer;
    public Color lineColor;

    // Raycast properties
    bool hit;
    RaycastHit rayHit;
    Vector3 rayStart;
    Vector3 rayEnd;

    // State indicator, used when another interaction takes precedence in Interactor Group Manager
    private bool power = true;

    // Initialiazation
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = 2;

        lastinputValue = 0;

        paletteObject.SetActive(false);
        paletteObject.transform.parent = null;
    }

    // Sets the state indicator
    public void SetPower(bool newPower)
    {
        power = newPower;
    }

    // Synchronizes active state of this object and the placed object
    private void OnEnable()
    {
        if(paletteObject != null && !paletteObject.activeSelf) paletteObject.SetActive(true);
    }

    private void OnDisable()
    {
        if(paletteObject != null && paletteObject.activeSelf) paletteObject.SetActive(false);
    }

    void Update()
    {
        if (power)
        {
            HandleRayCast();
            HandleLineRendering();
            HandleInput();
        }
    }

    // Handles raycasting
    void HandleRayCast()
    {
        rayStart = gameObject.transform.position;
        rayEnd = gameObject.transform.position + (gameObject.transform.rotation * rayDirection * rayMaxLenght);

        hit = Physics.Raycast(rayStart, gameObject.transform.rotation * rayDirection, out rayHit, rayMaxLenght);
    }

    // Handles raycast line rendering
    void HandleLineRendering()
    {
        lineRenderer.SetPosition(0, rayStart);
        if (hit)
        {
            lineRenderer.SetPosition(1, rayHit.point);
        }
        else
        {
            lineRenderer.SetPosition(1, rayEnd);
        }
    }

    // Handles user input
    void HandleInput()
    {
        if (hit && rayHit.point != null)
        {
            float inputValue = placeInput.action.ReadValue<float>();
            if (inputValue > 0.5f && lastinputValue < 0.5f) { PlacePallete(); }
            lastinputValue = inputValue;
        }
    }

    // Places the palette object, turns it towards the user and scales it for better targeting
    void PlacePallete()
    {
        paletteObject.SetActive(true);

        Vector3 newUp = rayHit.normal;
        Vector3 newDir = (transform.position - rayHit.point).normalized;

        Vector3 hitPosition = rayHit.point;
        hitPosition.y = transform.position.y;

        Quaternion paletteRotation = Quaternion.LookRotation(newDir, newUp);

        paletteObject.transform.rotation = paletteRotation;
        paletteObject.transform.position = rayHit.point + newDir*positionOffset;
        paletteObject.transform.localScale = Vector3.one * Mathf.Lerp(sizeAtMinDist, sizeAtMaxDist, Mathf.Clamp01(rayHit.distance / rayMaxLenght));
    }

}
