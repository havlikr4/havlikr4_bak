using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Component for changing color of an object via Object Brush.
 * Handles highlighting when targeted by Object Brush.
 */

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(HighlightableObject))]
public class ColorableObject : NetworkBehaviour
{
    // Highlightable component
    public HighlightableObject highlightable;

    // Tag
    public const string ObjectTag = "Colorable";

    // Initialization
    void Start()
    {
        gameObject.tag = ObjectTag;
        highlightable = GetComponent<HighlightableObject>();
    }

    // Changes the color of the object via Highlightable since it synchronizes color on the client
    public void ChangeColor(Color color)
    {
        if (isServer) highlightable.myColor = color;
    }
}
