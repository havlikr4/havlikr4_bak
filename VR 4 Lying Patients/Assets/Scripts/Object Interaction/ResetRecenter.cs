using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Activable object which resets the recentering of the User Origin
 */

public class ResetRecenter : ActivableObject
{
    // Resets the recentering of the User Origin
    public override void MyFunction()
    {
        originManager.Recenter();
    }
}
