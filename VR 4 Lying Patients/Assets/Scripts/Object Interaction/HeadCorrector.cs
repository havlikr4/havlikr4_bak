using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Activable object which compensates the posture of the User Origin
 */
public class HeadCorrector : ActivableObject
{
    // Compensates the posture of the User Origin
    public override void MyFunction()
    {
        originManager.SetHeadTransform();
    }
}
