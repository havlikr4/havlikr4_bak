using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Unity.Mathematics;


/*
 * Object which can activate the nearest activable object it collides with.
 * Takes user input for activation.
 */
[RequireComponent (typeof(Rigidbody))]
public class HandActivator : MonoBehaviour
{
    [Header("Activation Input Action")]
    [SerializeField] private InputActionReference activateInput;
    public float lastInputValue;

    [Header("Visual highlight value")]
    [SerializeField]
    private float visualHighlighAmount = 0.5f;

    // Activables in range
    public List<ActivableObject> activables = new List<ActivableObject>();

    // The nearest activable in range
    ActivableObject nearestActivable;

    // Initialization
    private void Start()
    {
        lastInputValue = 0;

        Rigidbody rigidbody = gameObject.GetComponent<Rigidbody>();
        rigidbody.isKinematic = true;
        rigidbody.useGravity = false;
    }

    // Calculates the nearest colliding activable and handles user input
    private void Update()
    {
        FindNearestActivable();

        float inputValue = activateInput.action.ReadValue<float>();
        if (inputValue > 0.5f && lastInputValue < 0.5f) { Activate(); }
        lastInputValue = inputValue;
    }

    // Detects new activables which collide
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag(ActivableObject.ObjectTag))
        {
            ActivableObject activable = other.gameObject.GetComponent<ActivableObject>();

            if(activable != null) { activables.Add(activable); }
        }
    }

    // Detects activables leaving the collider
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag(ActivableObject.ObjectTag))
        {
            ActivableObject activable = other.gameObject.GetComponent<ActivableObject>();

            if (activable != null) { activables.Remove(activable); }
        }
    }

    // Finds the nearest activable and highlights it
    private void FindNearestActivable()
    {
        float distMin = math.INFINITY;
        ActivableObject currentNearest = null;

        foreach (ActivableObject activableObject in activables)
        {
            GameObject activableGameObject = activableObject.gameObject;

            float dist = Vector3.Distance(this.gameObject.transform.position, activableGameObject.transform.position);

            if (dist < distMin)
            {
                distMin = dist;
                currentNearest = activableObject;
            }
        }

        if (currentNearest != null)
        {
            if (nearestActivable != null)
            {
                if (currentNearest != nearestActivable)
                {
                    currentNearest.highlightable.Lighten(visualHighlighAmount);
                    nearestActivable.highlightable.Darken(visualHighlighAmount);
                }
            }
            else
            {
                currentNearest.highlightable.Lighten(visualHighlighAmount);
            }
        }
        else if (nearestActivable != null)
        {
            nearestActivable.highlightable.Darken(visualHighlighAmount);
        }

        nearestActivable = currentNearest;
    }

    // Activates the nearest activable
    private void Activate()
    {
        if (nearestActivable != null)
        {
            nearestActivable.Activate();
        }
    }
}
