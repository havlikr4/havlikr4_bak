using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Base for an object activable by an activator.
 * Highlights when colliding with the activator.
 * On activation does its own customizable method.
 */
[RequireComponent(typeof(HighlightableObject))]
public abstract class ActivableObject : MonoBehaviour
{
    // User Origin reference
    public UserOriginManager originManager = null;

    // Highlightable reference
    public HighlightableObject highlightable;

    // Tag
    public const string ObjectTag = "Activable";

    // Initialization
    void Start()
    {
        gameObject.tag = ObjectTag;

        highlightable = GetComponent<HighlightableObject>();
    }

    // On activation
    public void Activate()
    {
        if (originManager == null)
        {
            FinduserOrigin();
        }
        if(originManager != null)
        {
            MyFunction();
        }
    }

    // Customizable method
    public abstract void MyFunction();

    // Finds the User Origin
    public void FinduserOrigin()
    {
        GameObject userOriginObject = GameObject.FindGameObjectWithTag(UserOriginManager.ObjectTag);
        if (userOriginObject != null)
        {
            originManager = userOriginObject.GetComponent<UserOriginManager>();
        }
    }

}
