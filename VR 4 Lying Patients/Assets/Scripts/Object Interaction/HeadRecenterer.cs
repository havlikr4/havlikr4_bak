using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Activable object which recenters the User Origin
 */
public class HeadRecenterer : ActivableObject
{
    // Recenters the User Origin
    public override void MyFunction()
    {
        originManager.Recenter();
    }
}
