using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Activable object which switches the interaction mode of the User Origin
 */

public class ModeSwitcher : ActivableObject
{
    // Switches the interaction mode of the User Origin
    public override void MyFunction()
    {
        originManager.SwitchInteractionMode();
    }
}
