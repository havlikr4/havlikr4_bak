using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Activable object which resets the posture compensation of the User Origin
 */
public class HeadReseter : ActivableObject
{
    // Resets the posture compensation of the User Origin
    public override void MyFunction()
    {
        originManager.ResetHeadTransform();
    }
}
