using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Activable object which switches the movement mode of the User Origin (normal and grab movement)
 */
public class GrabSwitcher : ActivableObject
{
    // Switches the movement mode of the User Origin (normal and grab movement)
    public override void MyFunction()
    {
        originManager.SetGrabMovement(!originManager.grabMovement);
    }

}
