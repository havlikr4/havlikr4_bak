using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Plays a sound on collision with a sound area (CollisionSoundArea).
 * The audioclip is derived from the area.
 */
[RequireComponent(typeof(AudioSource))]
public class CollisionSoundObject : MonoBehaviour
{
    [Header("Cooldown properties")]
    [SerializeField]
    float soundCooldown = 0.5f;
    float cooldownTimer = 0;

    [Header("Does this use trigger instead of collision?")]
    [SerializeField]
    bool trigger = false;

    // Audiosource reference
    AudioSource audioSource;

    // Initialization
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Handles cooldown timer
    void Update()
    {
        if (cooldownTimer < soundCooldown)
        {
            cooldownTimer += Time.deltaTime;
        }
    }

    // Detects new collision, if it is with a sound area, plays a sound
    private void OnCollisionEnter(Collision collision)
    {
        if (trigger) return;
        if (!collision.gameObject.CompareTag(CollisionSoundArea.ObjectTag)) return;

        TryPlaySound(collision.gameObject.GetComponent<CollisionSoundArea>());
    }

    // Detects new trigger collision, if it is with a sound area, plays a sound
    private void OnTriggerEnter(Collider other)
    {
        if (!trigger) return;
        if (!other.CompareTag(CollisionSoundArea.ObjectTag)) return;

        TryPlaySound(other.gameObject.GetComponent<CollisionSoundArea>());
    }

    // Plays a sound from the area it is colliding with if not on cooldown
    void TryPlaySound(CollisionSoundArea soundArea)
    {
        if(soundArea == null) return;
        if (cooldownTimer < soundCooldown) return;

        AudioClip soundClip = soundArea.GetCollisionClip();
        audioSource.PlayOneShot(soundClip);

        cooldownTimer = 0;
    }


}
