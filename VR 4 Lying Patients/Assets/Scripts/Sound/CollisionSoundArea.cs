using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Keeps the audioclips associated with this area.
 * Clips are played by a sound object (CollisionSoundObject) on collision with this area.
 */
public class CollisionSoundArea : MonoBehaviour
{
    [Header("Clips to be played on collision")]
    [SerializeField]
    List<AudioClip> collisionClips = new List<AudioClip>();

    // Tag
    public const string ObjectTag = "CollisionSound";
    
    // Initialization
    void Start()
    {
        gameObject.tag = ObjectTag;
    }

    // Gives a random collision sound to the sound object to play
    public AudioClip GetCollisionClip()
    {
        int randomIndex = Random.Range(0, collisionClips.Count);
        return collisionClips[randomIndex];
    }


}
