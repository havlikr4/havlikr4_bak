using Mirror;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/*
 * Manages object which is spawned for every player.
 * Used for User Origin properties synchronization for clients.
 */
public class UserOriginNetworkManager : NetworkBehaviour
{
    // Server Origin Objects
    private GameObject serverHead;
    private GameObject serverLeftHand;
    private GameObject serverRightHand;

    [Header("Client Origin Objects")]
    [SerializeField]
    public GameObject clientHead;
    [SerializeField]
    public GameObject clientCam;
    [SerializeField]
    public GameObject clientLeftHand;
    [SerializeField]
    public GameObject clientRightHand;
    [SerializeField]
    public ObjectBrush clientObjectBrush;

    // Object Brush SyncVars
    [SyncVar]
    Color myColor = Color.white;
    [SyncVar]
    ObjectBrush.ColoringModes coloringMode = ObjectBrush.ColoringModes.fill;
    [SyncVar]
    bool drawing = false;

    // Mode SyncVars
    [SyncVar]
    public UserOriginManager.InteractionMode interactionMode;
    [SyncVar]
    public bool controllerMode = true;

    // Server Origin Objects
    private UserOriginManager originManager;
    private GameObject palette;
    private ObjectBrush serverObjectBrush;

    // Host check
    [SyncVar] public bool isHost = false;

    void Start()
    {
        if (isServer)
        {
            if (isLocalPlayer)
            {
                // Host on server initialization
                serverHead = GameObject.FindWithTag("MainCamera");
                originManager = GameObject.FindWithTag(UserOriginManager.ObjectTag).GetComponent<UserOriginManager>();
                    
                SetCorrectHandObjects();

                MeshRenderer lmr = clientLeftHand.GetComponent<MeshRenderer>();
                MeshRenderer rmr = clientRightHand.GetComponent<MeshRenderer>();
                lmr.enabled = false;
                rmr.enabled = false;

                clientObjectBrush.gameObject.SetActive(false);
                clientCam.SetActive(false);

                // Sets host objects to be used in logging
                GameObject networkManagerObject = GameObject.FindGameObjectWithTag(ServerManager.ObjectTag);
                if(networkManagerObject != null)
                {
                    MyLogger logger = networkManagerObject.GetComponent<MyLogger>();
                    if(logger != null)
                    {
                        logger.SetParts(this);
                    }
                }
                isHost = true;
            }
            else
            {
                gameObject.SetActive(false);
            }
        }
        else
        {
            if (isHost)
            {
                Debug.Log("I am the host " + name + " with ID " + netId);
                StartCoroutine(HostInitialization());
            }
            else
            {
                Debug.Log("I am the client " + name + " with ID " + netId);
                gameObject.SetActive(false);
            }
        }
    }

    // Host instance on client initialization
    IEnumerator HostInitialization()
    {
        yield return new WaitForFixedUpdate();

        originManager = GameObject.FindWithTag(UserOriginManager.ObjectTag).GetComponent<UserOriginManager>();
        if (originManager != null)
        {
            for (int i = 0; i < originManager.transform.childCount; i++)
            {
                originManager.transform.GetChild(i).gameObject.SetActive(false);
            }

            originManager.vrCamera.gameObject.SetActive(false);
            GameObject VRCamera = originManager.vrCamera;
            if (VRCamera != null) VRCamera.SetActive(false);
        }

        clientCam.SetActive(true);
        clientCam.tag = "MainCamera";
        palette = originManager.paletteObject;
    }

    private void Update()
    {
        if (isServer)
        {
            interactionMode = originManager.myMode;

            GetServerObjectBrushInfo();
        }

        if (originManager != null)
        {
            SetCorrectHandObjects();
            SetColorMode();
        }

        if (isClient)
        {
            HandleClientBrush();
        }

    }

    // Updates variables for the client Object Brush
    void GetServerObjectBrushInfo()
    {
        if (controllerMode)
        {
            serverObjectBrush = originManager.rightController.objectBrush;
        }
        else
        {
            serverObjectBrush = originManager.rightXRHand.objectBrush;
        }
        if (serverObjectBrush != null && serverObjectBrush.meshRenderer != null)
        {
            myColor = serverObjectBrush.myColor;
            coloringMode = serverObjectBrush.myMode;
            drawing = serverObjectBrush.drawing;
        }
    }

    // Handles changes of hand representation in User Origin (controllers and hand tracking)
    void SetCorrectHandObjects()
    {
        if (originManager.leftControllerObject.activeSelf)
        {
            serverLeftHand = originManager.leftControllerObject;
            if (isServer) controllerMode = true;
        }
        if (originManager.rightControllerObject.activeSelf) serverRightHand = originManager.rightControllerObject;
        if (originManager.leftXRHandObject.activeSelf)
        {
            serverLeftHand = originManager.leftXRHandObject;
            if (isServer) controllerMode = false;
        }
        if (originManager.rightXRHandObject.activeSelf) serverRightHand = originManager.rightXRHandObject;
    }

    // Updates client object brush mode
    void SetColorMode()
    {
        if (interactionMode == UserOriginManager.InteractionMode.color)
        {
            if (!isServer) clientObjectBrush.gameObject.SetActive(true);
            
            clientObjectBrush.SetMode(coloringMode);
            clientObjectBrush.myColor = myColor;

            if(palette != null) palette.SetActive(true);
        }
        else
        {
            clientObjectBrush.gameObject.SetActive(false);
        }
    }

    // Handles client Object Brush drawing
    void HandleClientBrush()
    {
        if (interactionMode == UserOriginManager.InteractionMode.color && clientObjectBrush != null)
        {
            clientObjectBrush.drawing = drawing;
        }
    }

    // Copies transforms of User Origin head and hands
    private void LateUpdate()
    {
        if (serverHead != null)
        {
            transform.SetLocalPositionAndRotation(serverHead.transform.position, serverHead.transform.rotation);
            if (serverLeftHand != null)
            {
                clientLeftHand.transform.SetPositionAndRotation(serverLeftHand.transform.position, serverLeftHand.transform.rotation);
            }
            if (serverRightHand != null)
            {
                clientRightHand.transform.SetPositionAndRotation(serverRightHand.transform.position, serverRightHand.transform.rotation);
            }
        }
    }
}
