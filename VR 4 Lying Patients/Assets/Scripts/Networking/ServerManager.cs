using Mirror;
using Mirror.Discovery;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Management;

/*
 * Component of Network Manager object. Dictates if the app instance is a server.
 */

[RequireComponent(typeof(NetworkManager))]
[RequireComponent(typeof(NetworkDiscovery))]
[RequireComponent(typeof(HeadKeeper))]
public class ServerManager : MonoBehaviour
{
    [Header("Should this instance start a server")]
    [SerializeField] bool StartServer;
    [Header("Client GUI canvas to be turned off on server")]
    [SerializeField] GameObject ClientGUICanvas;

    // Networking components 
    NetworkManager networkManager;
    NetworkDiscovery networkDiscovery;

    // Tag
    public const string ObjectTag = "NetworkManager";

    // Handles server starting
    void Start()
    {
        gameObject.tag = ObjectTag;

        if (StartServer)
        {
            networkManager = gameObject.GetComponent<NetworkManager>();
            networkDiscovery = gameObject.GetComponent<NetworkDiscovery>();

            networkManager.StartHost();
            networkDiscovery.AdvertiseServer();

            Debug.Log("Starting server...");

            ClientGUICanvas.SetActive(false);
        }
    }


}
