using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Manages Hand Location Teleporters in scene.
 * Used for teleportation control from the mobile app.
 */
public class LocationManager : NetworkBehaviour
{
    // User Origin references
    GameObject userOrigin;
    UserOriginManager originManager;

    [Header("Locations")]
    [SerializeField] public List<HandLocationTeleporter> teleports = new List<HandLocationTeleporter>();
    public int currentLocationIndex = 0;
    
    // Tag
    public const string ObjectTag = "LocationManager";

    // Initialization
    void Start()
    {
        gameObject.tag = ObjectTag;
        FindOrigin();

        GameObject clientMenu = GameObject.FindGameObjectWithTag(ClientMenuController.ObjectTag);
        if (clientMenu != null)
        {
            ClientMenuController clientMenuController = clientMenu.GetComponent<ClientMenuController>();
            if (clientMenuController != null)
            {
                clientMenuController.PrepareLocationDropdown(teleports);
            }
        }
    }

    // Finds User Origin references
    void FindOrigin()
    {
        userOrigin = GameObject.FindWithTag(UserOriginManager.ObjectTag);
        if (userOrigin != null)
        {
            originManager = userOrigin.GetComponent<UserOriginManager>();
        }
    }

    // Teleporation actions
    public void TeleportToNextLocation()
    {
        currentLocationIndex++;
        if (currentLocationIndex >= teleports.Count)
        {
            currentLocationIndex = 0;
        }
        TeleportAction();
    }

    public void TeleportToLastLocation()
    {
        currentLocationIndex--;
        if (currentLocationIndex < 0)
        {
            currentLocationIndex = teleports.Count - 1;
        }
        TeleportAction();
    }

    public void TeleportToLocation(int locationIndex)
    {
        if (locationIndex > -1 && locationIndex < teleports.Count)
        {
            currentLocationIndex = locationIndex;
            TeleportAction();
        }
    }

    private void TeleportAction()
    {
        if (originManager == null)
        {
            FindOrigin();
        }
        if (originManager != null)
        {
            teleports[currentLocationIndex].MyFunction();
        }
    }

    // COMMANDS -- called from client (mobile app)
    [Command(requiresAuthority = false)]
    public void CmdToNextLocation(NetworkConnectionToClient sender = null)
    {
        TeleportToNextLocation();
    }

    [Command(requiresAuthority = false)]
    public void CmdToLastLocation(NetworkConnectionToClient sender = null)
    {
        TeleportToLastLocation();
    }

    [Command(requiresAuthority = false)]
    public void CmdToLocation(int locationIndex, NetworkConnectionToClient sender = null)
    {
        TeleportToLocation(locationIndex);
    }

}
