using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

/*
 * Chargable object used for teleporting to specified location.
 */
[RequireComponent(typeof(Loggable))]
public class HandLocationTeleporter : ChargableObject
{
    [Header("Teleport destination")]
    [SerializeField]
    private Vector3 teleportPosition;
    [SerializeField]
    private Vector3 destinationRotation;
    [Header("Teleport location name")]
    [SerializeField]
    public string locationName = "lokace";

    // Logging
    Loggable loggable;

    // User Origin reference
    private UserOriginManager userOriginManager;

    // Location Manager refernce
    private LocationManager locationManager;

    // Teleports User Origin to specified location
    public override void MyFunction()
    {
        userOriginManager.TeleportOrigin(transform.TransformPoint(teleportPosition));
        userOriginManager.TurnOrigin(destinationRotation);

        loggable.LogEvent(LoggingGlobs.EventKeys.location_change);
    }

    // Finds needed object references in scene
    public override IEnumerator FindMyObjects()
    {
        yield return new WaitForFixedUpdate();

        loggable = GetComponent<Loggable>();

        GameObject origin = GameObject.FindGameObjectWithTag(UserOriginManager.ObjectTag);

        if (origin != null)
        {
            userOriginManager = origin.GetComponent<UserOriginManager>();
        }
    }
}
