using Editor;
using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using static SceneGlobs;

/*
 * Manages scene changing.
 * Used for scene changing by the mobile app.
 */
[RequireComponent(typeof(Loggable))]
public class NetworkSceneManager : NetworkBehaviour
{
    // Manager references
    private NetworkManager networkManager;
    [SerializeField] private LoggerManager loggerManager;

    // Logging
    Loggable loggable;

    // Tag
    public const string ObjectTag = "SceneManager";

    // Initialization
    void Start()
    {
        gameObject.tag = ObjectTag;

        FindNetworkManager();

        loggable = GetComponent<Loggable>();
    }

    // Finds manager reference
    void FindNetworkManager()
    {
        GameObject manager = GameObject.FindWithTag(ServerManager.ObjectTag);
        if (manager != null)
        {
            networkManager = manager.GetComponent<NetworkManager>();
        }
    }

    // Changes scene to a new one
    public void ChangeScene(string sceneName)
    {
        if (networkManager == null)
        {
            FindNetworkManager();
        }
        if (networkManager != null)
        {
            StartCoroutine(ChangeSceneDelayed(sceneName));
        }
    }

    // Changes scene after a delay to let the fading animation play
    private IEnumerator ChangeSceneDelayed(string sceneName)
    {
        loggable.LogEvent(LoggingGlobs.EventKeys.scene_change);
        if (loggerManager != null) loggerManager.SceneTransition(sceneName);


        Fader fader = GameObject.FindObjectOfType<Fader>();
        if (fader != null) fader.StandartFadeOut();

        yield return new WaitForSeconds(fader.standartDuration);

        networkManager.ServerChangeScene(sceneName);
    }


    // COMMANDS -- called from client (mobile app)
    [Command(requiresAuthority = false)]
    public void CmdChangeToSelectedScene(int sceneIndex, NetworkConnectionToClient sender = null)
    {
        string sceneName = ((SceneGlobs.SceneNames)sceneIndex).ToString();
        ChangeScene(sceneName);
    }


}

