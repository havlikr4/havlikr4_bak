using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

/*
 * Chargable object which changes the scene on activation.
 */

public class SceneTeleporter : ChargableObject
{
    [Header("Destination Scene Name")]
    [SerializeField] SceneGlobs.SceneNames sceneName;

    // SceneManager reference
    private NetworkSceneManager myManager;

    // Changes the scene
    public override void MyFunction()
    {
        myManager.ChangeScene(sceneName.ToString());
    }

    // Finds Scene Manager reference
    public override IEnumerator FindMyObjects()
    {
        yield return new WaitForFixedUpdate();

        GameObject managerObject = GameObject.FindWithTag(NetworkSceneManager.ObjectTag);
        if (managerObject != null)
        {
            myManager = managerObject.GetComponent<NetworkSceneManager>();
        }
    }
}
