using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static UnityEngine.Rendering.DebugUI;

/*
 * Controller of the menu in mobile (client) app.
 * Provides methods for Buttons in the menu.
 * Manages dropdowns data.
 */

public class ClientMenuController : MonoBehaviour
{
    [Header("Dropdowns")]
    [SerializeField]
    private Dropdown sceneDropdown;
    [SerializeField]
    private Dropdown locationDropdown;

    [Header("Grab Movement spline slider")]
    [SerializeField]
    private Slider splineSlider;

    // Object references
    private UserOriginManager originManager;
    private LocationManager locationManager;
    private NetworkSceneManager sceneManager;
    private LoggerManager loggerManager;

    // Tag
    public const string ObjectTag = "ClientGUI";

    // Initialization
    void Start()
    {
        StartCoroutine(FindMyObjects());

        this.tag = ObjectTag;
    }

    void OnEnable()
    {
        StartCoroutine(FindMyObjects());

        this.tag = ObjectTag;
    }


    IEnumerator FindMyObjects()
    {
        yield return null;

        FindLocationManager();
        FindOrigin();
        PrepareSceneDropdown();
    }

    // Manager reference Finders

    void FindLocationManager()
    {
        GameObject managerObject = GameObject.FindWithTag(LocationManager.ObjectTag);
        if (managerObject != null)
        {
            locationManager = managerObject.GetComponent<LocationManager>();
        }
    }

    void FindSceneManager()
    {
        GameObject managerObject = GameObject.FindWithTag(NetworkSceneManager.ObjectTag);
        if (managerObject != null)
        {
            sceneManager = managerObject.GetComponent<NetworkSceneManager>();
        }
    }

    void FindOrigin()
    {
        GameObject userOrigin = GameObject.FindWithTag(UserOriginManager.ObjectTag);
        if (userOrigin != null)
        {
            originManager = userOrigin.GetComponent<UserOriginManager>();
        }
    }

    void FindLoggerManager()
    {
        GameObject managerObject = GameObject.FindWithTag(LoggerManager.ObjectTag);
        if (managerObject != null)
        {
            loggerManager = managerObject.GetComponent<LoggerManager>();
        }
    }

    // Dropdowns data preparation

    void PrepareSceneDropdown()
    {
        if (sceneDropdown.options.Count > 0) return;

        List<Dropdown.OptionData> sceneOptionData = new List<Dropdown.OptionData>();

        for (int i = 0; i < Enum.GetNames(typeof(SceneGlobs.SceneNames)).Length; i++)
        {
            Dropdown.OptionData sceneData = new Dropdown.OptionData();
            
            sceneData.text = SceneGlobs.SceneNicknames[i];

            sceneOptionData.Add(sceneData);
        }

        sceneDropdown.AddOptions(sceneOptionData);
    }


    public void PrepareLocationDropdown(List<HandLocationTeleporter> teleporters)
    {

        List<Dropdown.OptionData> locationOptionData = new List<Dropdown.OptionData>();

        for (int i = 0; i < teleporters.Count; i++)
        {
            Dropdown.OptionData locationData = new Dropdown.OptionData();

            locationData.text = teleporters[i].locationName;

            locationOptionData.Add(locationData);
        }

        if(locationOptionData.Count > 0) locationDropdown.ClearOptions();

        locationDropdown.AddOptions(locationOptionData);
    }


    // COMMANDS for buttons
    // Finds affected object if missing its refernce and calls a method on that object

    public void NextLocationCommand()
    {
        if(locationManager == null)
        {
            FindLocationManager();
        }
        if(locationManager != null)
        {
            locationManager.CmdToNextLocation();
        }
    }

    public void PreviousLocationCommand()
    {
        if (locationManager == null)
        {
            FindLocationManager();
        }
        if (locationManager != null)
        {
            locationManager.CmdToLastLocation();
        }
    }

    public void ChangeSceneCommand()
    {
        if(sceneManager == null)
        {
            FindSceneManager();
        }
        if(sceneManager != null && sceneDropdown != null)
        {
            sceneManager.CmdChangeToSelectedScene(sceneDropdown.value);
        }
    }

    public void SwitchInteractionModeCommand()
    {
        if(originManager == null)
        {
            FindOrigin();
        }
        if(originManager != null)
        {
            originManager.CmdSwitchMode();
        } 
    }

    public void SwitchGrabMovementCommand()
    {
        if (originManager == null)
        {
            FindOrigin();
        }
        if (originManager != null)
        {
            originManager.CmdSwitchGrab();
        }
    }

    public void RotateHeadCommand()
    {
        if (originManager == null)
        {
            FindOrigin();
        }
        if (originManager != null)
        {
            originManager.CmdSetHeadRotation();
        }
    }

    public void ResetHeadRotationCommand()
    {
        if (originManager == null)
        {
            FindOrigin();
        }
        if (originManager != null)
        {
            originManager.CmdResetHeadRotation();
        }
    }

    public void RecenterCommand()
    {
        if (originManager == null)
        {
            FindOrigin();
        }
        if (originManager != null)
        {
            originManager.CmdRecenter();
        }
    }

    public void ResetRecenterCommand()
    {
        if (originManager == null)
        {
            FindOrigin();
        }
        if (originManager != null)
        {
            originManager.CmdResetRecenter();
        }
    }

    public void StartLog()
    {
        if(loggerManager == null)
        {
            FindLoggerManager();
        }
        if(loggerManager != null)
        {
            loggerManager.CmdStartLogging();
        }
    }

    public void EndLog()
    {
        if (loggerManager == null)
        {
            FindLoggerManager();
        }
        if (loggerManager != null)
        {
            loggerManager.CmdEndLogging();
        }
    }

    public void SwitchBrushMode()
    {
        if (originManager == null)
        {
            FindOrigin();
        }
        if (originManager != null)
        {
            originManager.CmdSwitchBrushMode();
        }
    }

    public void TeleportToLocation()
    {
        if (locationManager == null)
        {
            FindLocationManager();
        }
        if (locationManager != null)
        {
            locationManager.CmdToLocation(locationDropdown.value);
        }
    }

    public void SetSplineDistance()
    {
        if (originManager == null)
        {
            FindOrigin();
        }
        if (originManager != null)
        {
            originManager.CmdSetSplineT(splineSlider.value);
        }
    }
}
