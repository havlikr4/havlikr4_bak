using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Component that makes an object rotate towards a certain camera.
 * The rotation is around the Y axis.
 */
public class RotateTowardsCamera : NetworkBehaviour
{
    [Header("Camera to rotate towards")]
    [SerializeField] GameObject mainCamera;

    // Initialization
    void Start()
    {
        if(mainCamera == null)
        {
            FindCamera();
        }
    }

    // Finds the main camera reference
    void FindCamera()
    {
        mainCamera = GameObject.FindWithTag("MainCamera");
    }

    // Sets the rotation
    private void SetRotationTowardsCamera()
    {
        Vector3 myCameraPosition = mainCamera.transform.position;
        myCameraPosition.y = transform.position.y;

        transform.rotation = Quaternion.LookRotation(myCameraPosition - transform.position);
    }

    // If this is on server, rotates towards camera (client rotation is handled by NetworkTransform)
    void Update()
    {
        if (isServer)
        {
            if (mainCamera == null)
            {
                FindCamera();
            }
            if (mainCamera != null)
            {
                SetRotationTowardsCamera();
            }
        } 
    }
}
