using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

/*
 * Sets the text size of multiple TMP Texts to the same value.
 * The value is based on auto text size that is the smallest in the group of texts.
 */
public class AutoTextSizeGroup : MonoBehaviour
{
    // Tag for texts not be affected
    public const string DontTakeTextTag = "TextDont";

    [Header("TMP Texts to be affected")]
    [SerializeField]
    List<TMP_Text> texts = new List<TMP_Text>();

    // Initialization
    void Start()
    {
        StartCoroutine(InitializeTexts());
    }

    IEnumerator InitializeTexts()
    {
        yield return null;
        FindAllTexts();
        SetSameTextSize();
    }

    // Finds TMP Texts in children of affected text objects
    void FindAllTexts()
    {
        foreach (TMP_Text text in transform.GetComponentsInChildren(typeof(TMP_Text)))
        {
            if(!text.CompareTag(DontTakeTextTag)) texts.Add(text);
        }
    }

    // Finds the minimum size and sets all texts to that size
    void SetSameTextSize()
    {
        float textSizeMininum = float.PositiveInfinity;

        foreach (TMP_Text text in texts)
        {
            float size = text.fontSize;
            if(size < textSizeMininum)
            {
                textSizeMininum = size;
            }
        }

        foreach (TMP_Text text in texts)
        {
            text.fontSizeMax = textSizeMininum;
        }
    }

}
