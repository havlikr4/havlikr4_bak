using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Object which helps visually indicate the charging process of chargable objects.
 * Transparent by default, increses opacity while charging up to full when charged.
 * Synchronizes color between server and client.
 */
[RequireComponent(typeof(MeshRenderer))]
public class GizmoObject : NetworkBehaviour
{
    // Synchronized color
    [SyncVar]
    public Color gizmoColor;

    [HideInInspector]
    public MeshRenderer gizmoRenderer;

    // Initialization
    private void Start()
    {
        gizmoRenderer = gameObject.GetComponent<MeshRenderer>();

        gizmoColor = gizmoRenderer.material.color;
    }

    // Accesed by chargable object to change the opacity of this object
    public void ChangeAlpha(float a)
    {
        if(isServer) gizmoColor.a = a;
    }

    // Sets the synchronized color for all affected renderers
    void Update()
    {
        gizmoRenderer.material.color = gizmoColor;
    }
}
