using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Simple method for Buttons to use for turning other Objects (such as menus) on and off.
 */
public class MenuSwitch : MonoBehaviour
{
    [Header("Menu Object")]
    [SerializeField] GameObject menu;

    // Activates and deactivates the menu object
    public void SwitchMenu()
    {
        menu.SetActive(!menu.activeSelf);
    }
}
