using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Component that alows visual highlighting of an object.
 * Highlighting is achieved by increasing the V in 
 * HSV representation of the material color of the object.
 * Synchronizes the color of an object between server and client.
 */

public class HighlightableObject : NetworkBehaviour
{
    // Synchronized color
    [SyncVar]
    public Color myColor;

    [Header("Should this also highlight children?")]
    [SerializeField]
    private bool targetChildren = false;

    // Affected renderers refernce
    [HideInInspector]
    public List<MeshRenderer> meshRenderers = new List<MeshRenderer>();

    // State indicator
    private bool highlighted = false;

    // Initialization
    private void Start()
    {
        FindRender();
        if(targetChildren) FindChildRenderes();
        if (meshRenderers.Count == 0)
        {
            gameObject.SetActive(false);
            return;
        }
            
        myColor = meshRenderers[0].material.color;
    }

    // Sets the synchronized color for all affected renderers
    void Update()
    {
        foreach(MeshRenderer renderer in meshRenderers)
        {
            renderer.material.color = myColor;
        }
    }

    // Finds a renderer reference on the main object
    void FindRender()
    {
        MeshRenderer meshRenderer = gameObject.GetComponent<MeshRenderer>();
        if (meshRenderer != null) meshRenderers.Add(meshRenderer);
    }

    // Finds renderer references in children
    void FindChildRenderes()
    {
        foreach(MeshRenderer renderer in transform.GetComponentsInChildren(typeof(MeshRenderer)))
        {
            meshRenderers.Add(renderer);
        }
    }

    // Increases the V value in HSV of the synchronized color
    public void Lighten(float vChange)
    {
        if (highlighted) { return; }

        float H, S, V;
        Color.RGBToHSV(myColor, out H, out S, out V);

        V += vChange;

        if (isServer) myColor = Color.HSVToRGB(H, S, V);

        highlighted = true;
    }

    // Decreases the V value in HSV of the synchronized color
    public void Darken(float vChange)
    {
        if (!highlighted) { return; }

        float H, S, V;
        Color.RGBToHSV(myColor, out H, out S, out V);

        V -= vChange;

        if (isServer) myColor = Color.HSVToRGB(H, S, V);

        highlighted = false;
    }

}
