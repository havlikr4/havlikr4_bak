using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoggingGlobs 
{
    // Enum for event keys / names
    public enum EventKeys
    {
        grab,
        let_go,
        combine_correct,
        combine_wrong,
        color,
        draw,
        erase,
        location_change,
        scene_change,
        mode_change
    }

   
}
