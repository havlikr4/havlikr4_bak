using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneGlobs 
{
    // Enum for scene file names
    public enum SceneNames
    {
        StartScene,
        FarmScene,
        MountainScene,
        ChoosingScene,
        HarborScene,
        ParkScene,
        BlankScene
    }

    // Enum for displaying scene names in GUI
    public static readonly string[] SceneNicknames = { "�vodn� sc�na",
        "Va�en� a houby",
        "Horsk� proch�zka",
        "V�b�r malov�n�", 
        "Malov�n� - p��stav",
        "Malov�n� - park",
        "Malov�n� - kanvas"
    };



}
