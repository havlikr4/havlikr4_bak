using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.InputSystem;
using System;

/*
 * This collector is contolled by user input.
 * Usually attached to users hands.
 * Detects collectibles inside of collider and highlights the closest one.
 * On ipnut attached the closes collectible in range or detaches the current collectible.
 * Can only hold one obejct.
 */

[RequireComponent(typeof(Loggable))]
public class HandCollector : Collector
{
    [Header("Activation Input Value")]
    [SerializeField]
    private InputActionReference grabInput;
    private float lastGrabValue;

    [Header("Snap Settings")]
    [SerializeField]
    private bool snapOnAttach = false;
    [SerializeField]
    private Vector3 snapPosition = Vector3.zero;

    [Header("Vibration Settings")]
    [SerializeField] bool vibration = true;
    [SerializeField] ActionBasedController controller;
    [SerializeField] float vibrationAmplitude = 0.3f;
    [SerializeField] float vibrationDuration = 0.5f;

    [Header("Visual highlight value")]
    [SerializeField]
    private float visualHighlighAmount = 0.5f;

    // Helper properties
    public bool holding;
    CollectibleObject myCollectibleObject;
    public List<CollectibleObject> collectibleObjectsInRange;
    private Vector3 lastPosition;
    private CollectibleObject nearestCollectible = null;

    // Tag
    public const string ObjectTag = "Collector";

    // Logging
    private Loggable loggable;

    // Initialization
    void Start()
    {
        gameObject.tag = ObjectTag;

        holding = false;
        myCollectibleObject = null;

        lastPosition = gameObject.transform.position;
        lastGrabValue = 0;

        collectibleObjectsInRange = new List<CollectibleObject>();


        loggable = gameObject.GetComponent<Loggable>();
    }

    // Detects new collectibles in range
    private void OnTriggerEnter(Collider other)
    {
        if (Enum.IsDefined(typeof(CollectibleObject.CollectibleTags), other.gameObject.tag))
        {
            CollectibleObject collectibleObject = other.gameObject.GetComponent<CollectibleObject>();
            if (collectibleObject != null)
            {
                if (!collectibleObjectsInRange.Contains(collectibleObject)) collectibleObjectsInRange.Add(collectibleObject);
            }
        }
    }

    // Detects collectibles leaving the collider
    private void OnTriggerExit(Collider other)
    {
        if (Enum.IsDefined(typeof(CollectibleObject.CollectibleTags), other.gameObject.tag))
        {
            CollectibleObject collectibleObject = other.gameObject.GetComponent<CollectibleObject>();
            if (collectibleObject != null)
            {
                collectibleObjectsInRange.Remove(collectibleObject);
            }
        }
    }


    void Update()
    {
        float grabValue = grabInput.action.ReadValue<float>();

        if (grabValue > 0.5f && lastGrabValue < 0.5f) { TryToGrab(); }

        lastGrabValue = grabValue;

        lastPosition = gameObject.transform.position;

        FindNearestCollectible();
    }

    // Calculates the closest collectible in range and higlights it, if it is not being held
    void FindNearestCollectible()
    {
        // Calculation of teh closest
        float distMin = math.INFINITY;
        CollectibleObject currentNearest = null;

        foreach (CollectibleObject collectibleObject in collectibleObjectsInRange)
        {
            GameObject collectibleGameObject = collectibleObject.gameObject;
            if (nearestCollectible != null && collectibleGameObject == nearestCollectible) continue;

            float dist = Vector3.Distance(this.gameObject.transform.position, collectibleGameObject.transform.position);

            if (dist < distMin)
            {
                distMin = dist;
                currentNearest = collectibleObject;
            }
        }

        // Highlighting
        if (currentNearest != null)
        {
            if (nearestCollectible != null)
            {
                if (currentNearest != nearestCollectible)
                {
                    currentNearest.highlightable.Lighten(visualHighlighAmount);
                    nearestCollectible.highlightable.Darken(visualHighlighAmount);
                }
            }
            else
            {
                currentNearest.highlightable.Lighten(visualHighlighAmount);
            }      
        }
        else if (nearestCollectible != null)
        {
            nearestCollectible.highlightable.Darken(visualHighlighAmount);
        }

        nearestCollectible = currentNearest;
    }

    // Detaches the current collectible if it exists, attaches closest collectible if it exists 
    void TryToGrab()
    {
        if (holding)
        {
            DetachObject(myCollectibleObject);
            loggable.LogEvent(LoggingGlobs.EventKeys.let_go);
        }
        else
        {
            if (nearestCollectible != null)
            {
                if (nearestCollectible.collected)
                {
                    nearestCollectible.myCollector.DetachObject(nearestCollectible);
                }
                AttachObject(nearestCollectible);
                loggable.LogEvent(LoggingGlobs.EventKeys.grab);

            }
        }
    }

    // Attachees a collectible
    public override void AttachObject(CollectibleObject collectibleObject)
    {
        myCollectibleObject = collectibleObject;
        collectibleObject.highlightable.Darken(visualHighlighAmount);

        myCollectibleObject.AttachTo(this);
        if (snapOnAttach) { myCollectibleObject.gameObject.transform.localPosition = snapPosition; }

        holding = true;

        if (vibration)
        {
            controller.SendHapticImpulse(vibrationAmplitude, vibrationDuration);
        }
    }

    // Detaches the current collectible with leaving force simulation
    public override void DetachObject(CollectibleObject collectibleObject)
    {
        myCollectibleObject.DetachFrom();
        myCollectibleObject.myRigidBody.velocity = (gameObject.transform.position - lastPosition) / Time.deltaTime;

        collectibleObjectsInRange.Remove(myCollectibleObject);

        myCollectibleObject = null;
        holding = false;
    }
}
