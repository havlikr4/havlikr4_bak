using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Uncollected collectibles are automatically attached to this collector. 
 * Can hold multiple collectibles.
 */
public class StorageCollector : Collector
{
    // helper properties
    [HideInInspector] public List<CollectibleObject> myCollectibles = new List<CollectibleObject>();

    // Used when the collector itself is moving, used for simulating force on detach
    private Vector3 lastPosition;

    // initialization
    private void Start()
    {
        lastPosition = gameObject.transform.position;
    }

    // Updates position
    private void Update()
    {
        lastPosition = gameObject.transform.position;
    }

    // Simple attach
    public override void AttachObject(CollectibleObject collectibleObject)
    {
        collectibleObject.AttachTo(this);
        myCollectibles.Add(collectibleObject);
    }

    // Detach with force simulation
    public override void DetachObject(CollectibleObject collectibleObject)
    {
        collectibleObject.DetachFrom();
        collectibleObject.myRigidBody.velocity = (gameObject.transform.position - lastPosition) / Time.deltaTime;
        myCollectibles.Remove(collectibleObject);
    }

    // Automatic attaching
    private void OnTriggerStay(Collider other)
    {
        if (Enum.IsDefined(typeof(CollectibleObject.CollectibleTags), other.gameObject.tag))
        {
            CollectibleObject collectibleObject = other.gameObject.GetComponent<CollectibleObject>();
            if (collectibleObject != null && !collectibleObject.collected)
            {
                AttachObject(collectibleObject);
            }
        }
    }


}
