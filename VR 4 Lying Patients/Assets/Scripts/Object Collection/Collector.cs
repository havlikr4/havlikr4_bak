using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Base for different types of collectors.
 */
public abstract class Collector : MonoBehaviour
{
    // Customizable attach method
    public abstract void AttachObject(CollectibleObject collectibleObject);

    // Customizable detach method
    public abstract void DetachObject(CollectibleObject collectibleObject);

}
