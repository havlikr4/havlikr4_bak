using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Component for objects which are able to be collected by collectors.
 * Collected objects are children of collectors.
 */
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(HighlightableObject))]
[RequireComponent(typeof(NetworkTransformReliable))]
public class CollectibleObject : MonoBehaviour
{
    // State of being collected
    [HideInInspector] public bool collected;
    // Current collector
    [HideInInspector] public Collector myCollector;
    // Rigidbody for physics simualtion
    [HideInInspector] public Rigidbody myRigidBody;
    // Component for highlighting when targeted by a collector
    [HideInInspector] public HighlightableObject highlightable;

    // Tag
    public const string ObjectTag = "Collectible";

    // Collectible objects are usually also ingredients 
    public enum CollectibleTags
    {
        Collectible,
        Ingredient
    }

    // Initialization
    void Start()
    {
        if (gameObject.tag != "Ingredient") gameObject.tag = ObjectTag;

        collected = false;
        myCollector = null;

        myRigidBody = GetComponent<Rigidbody>();
        highlightable = GetComponent<HighlightableObject>();

        NetworkTransformReliable networkTransform = GetComponent<NetworkTransformReliable>();
        networkTransform.coordinateSpace = CoordinateSpace.World;
    }

    // Attach to a collector
    public void AttachTo(Collector collector)
    {
        myRigidBody.isKinematic = true;
        gameObject.transform.parent = collector.gameObject.transform;
        collected = true;
        myCollector = collector;
    }

    // Detach from current collector
    public void DetachFrom()
    {
        myRigidBody.isKinematic = false;
        gameObject.transform.parent = null;
        collected = false;
        myCollector = null;
    }

}
