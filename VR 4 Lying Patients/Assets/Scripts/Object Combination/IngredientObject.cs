using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Object which can be combined upon collision with combiner.
 * Creatable ingredients can also be created from the combination in combiner based on their recipe.
 */

[RequireComponent(typeof(CollectibleObject))]
public class IngredientObject : MonoBehaviour
{
    [Header("Unique Name")]
    [SerializeField]
    public string ingredientName; // identifier used in combination

    [Header("Creating Settings")]
    [SerializeField]
    public bool creatable = false;
    [SerializeField]
    public List<string> recipe = new List<string>(); // identifiers of ingredients in recipes

    // Tag
    public const string ObjectTag = "Ingredient";

    // Initialization
    void Start()
    {
        gameObject.tag = "Ingredient";

        if (!creatable)
        {
            recipe.Clear();
        }
    }
}
