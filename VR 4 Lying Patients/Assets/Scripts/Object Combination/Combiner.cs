using Mirror;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/*
 * Object which can combine ingredient objects into another.
 * Combination occurs when colliding with all ingredients in recipe.
 * Upon reaching max number of ingredients the combiner creates an object representing a wrong result.
 */

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Loggable))]
public class Combiner : NetworkBehaviour
{
    [Header("Time delay for one combination")]
    [SerializeField]
    private float CombineTime = 1.0f;
    private float CombineCountdown = 0;
    private int toBeCreated = 0;

    [Header("Max number of ingredients")]
    [SerializeField]
    private int maxIngredients = 3;


    [Header("Local Spawn Position")]
    [SerializeField]
    private Vector3 spawnPosition = Vector3.zero;


    [Header("Creatable Objects")]
    [SerializeField]
    public List<IngredientObject> results = new List<IngredientObject>();
    public IngredientObject wrongResult;


    [Header("Combining Animation Settings")]
    [SerializeField]
    public ParticleSystem particleSystem;

    [Header("Sound Effects")]
    [SerializeField]
    List<AudioClip> addingClips = new List<AudioClip>();
    [SerializeField]
    List<AudioClip> combiningClips = new List<AudioClip>();
    private AudioSource audioSource;

    public List<IngredientObject> ingredients = new List<IngredientObject>();

    // Combiner passes information about almost completing a recipe to the character.
    [Header("Connected reaction character")]
    [SerializeField]
    ReactionCharacterManager reactionCharacterManager; 

    // Logging
    Loggable loggable;

    // Initialization
    void Start()
    {
        Rigidbody rigidbody = gameObject.GetComponent<Rigidbody>();
        if (rigidbody != null)
        {
            rigidbody.isKinematic = true;
        }

        audioSource = gameObject.GetComponent<AudioSource>();

        foreach (IngredientObject result in results.ToList())
        {
            if (!result.creatable || result.recipe.Count < 1)
            {
                results.Remove(result);
            }
        }

        loggable = gameObject.GetComponent<Loggable>();
    }

    // Countdown before creating the result
    void Update()
    {
        if (CombineCountdown > 0)
        {
            CombineCountdown -= Time.deltaTime;

            if (CombineCountdown <= 0)
            {
                SpawnResult();
            }
        }
    }

    // Detects new ingredients and tries to combine them
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(IngredientObject.ObjectTag))
        {

            IngredientObject ingredient = other.GetComponent<IngredientObject>();
            if (ingredient != null && !ingredient.creatable && !ingredients.Contains(ingredient))
            {
                ingredients.Add(ingredient);
                PlayAddSound();
                CombineObjects();
            }
        }
    }

    // Detects ingredients leaving the collider
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag(IngredientObject.ObjectTag))
        {
            IngredientObject ingredient = other.GetComponent<IngredientObject>();
            if (ingredient != null)
            {
                ingredients.Remove(ingredient);
            }
        }
    }

    // Tries to find a creatable recipe and creates it.
    private void CombineObjects()
    {
        List<List<IngredientObject>> toBeCombined = new List<List<IngredientObject>>();
        List<List<string>> recipes = new List<List<string>>();

        foreach (IngredientObject result in results)
        {
            toBeCombined.Add(new List<IngredientObject>());
            recipes.Add(result.recipe.ToList());
        }

        foreach (IngredientObject ingredient in ingredients)
        {
            for (int i = 0; i < recipes.Count; i++)
            {
                foreach (string ingredientName in recipes[i].ToList())
                {
                    if (ingredientName == ingredient.ingredientName)
                    {
                        toBeCombined[i].Add(ingredient);
                        recipes[i].Remove(ingredientName);
                        break;
                    }
                }
            }
        }

        for (int i = 0; i < recipes.Count; i++)
        {
            List<string> recipe = recipes[i];
            if (recipe.Count == 0)
            {
                foreach (IngredientObject ingredient in toBeCombined[i])
                {
                    ingredient.gameObject.SetActive(false);
                    ingredients.Remove(ingredient);
                }

                CombineCountdown = CombineTime;
                toBeCreated = i;

                PlayCombineSound();
                particleSystem.Play();

                loggable.LogEvent(LoggingGlobs.EventKeys.combine_correct);

                return;
            }
        }

        // If the number of colliding ingredients reaches its maximum and no recipe can be used wrong result is created.
        if(ingredients.Count >= maxIngredients)
        {
            foreach (IngredientObject ingredient in ingredients)
            {
                ingredient.gameObject.SetActive(false);
            }

            List<string> almostResults = new List<string>();

            for (int i = 0; i < recipes.Count; i++)
            {
                List<string> recipe = recipes[i];
                if(recipe.Count == 1)
                {
                    almostResults.Add(results[i].ingredientName);
                    Debug.Log("to bylo skoro " + results[i].ingredientName);
                }
            }

            if (reactionCharacterManager != null && almostResults.Count > 0)
            {
                int randomIndex = Random.Range(0, almostResults.Count);
                reactionCharacterManager.almostCorrectReaction(almostResults[randomIndex]);
            }

            particleSystem.Play();

            PlayCombineSound();
            ingredients.Clear();
            SpawnWrongResult();

            loggable.LogEvent(LoggingGlobs.EventKeys.combine_wrong);
        }
    }

    // Spawns the result object
    private void SpawnResult()
    {
        GameObject result = Instantiate(results[toBeCreated].gameObject);

        Vector3 localSpawnPosition = transform.position + spawnPosition;
        result.transform.position = localSpawnPosition;

        NetworkServer.Spawn(result);
    }

    // Spawns the object representing the wrong result
    private void SpawnWrongResult()
    {
        GameObject result = Instantiate(wrongResult.gameObject);

        Vector3 localSpawnPosition = transform.position + spawnPosition;
        result.transform.position = localSpawnPosition;

        NetworkServer.Spawn(result);
    }

    // Sound Effects
    void PlayAddSound()
    {
        int randomIndex = Random.Range(0, addingClips.Count);
        audioSource.PlayOneShot(addingClips[randomIndex]);
    }

    void PlayCombineSound()
    {
        int randomIndex = Random.Range(0, combiningClips.Count);
        audioSource.PlayOneShot(combiningClips[randomIndex]);
    }
}
