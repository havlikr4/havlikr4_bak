using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.InputSystem.XR;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Management;

/*
 * Manages the main functionallity of the users representation.
 * Keeps track of all main parts.
 * Handles movement - teleportation, snap turns and grab movement on spline.
 * Controlls recentering of the user camera (VR Camera).
 * Controlls posture compensation - transformation of offset objects to make 
 * the user see as if they were standing if they are not.
 */

[RequireComponent(typeof(NetworkIdentity))]
[RequireComponent(typeof(XROrigin))]
[RequireComponent(typeof(Loggable))]
public class UserOriginManager : NetworkBehaviour
{
    [Header("Origin Parts")]
    [SerializeField] private GameObject locationOffset; // object transformed for posture compensation
    [SerializeField] private GameObject rotationOffset; // object transformed for posture compensation
    [SerializeField] public GameObject vrCamera; // Headset representation - users camera
    [SerializeField] public InteractorGroupManager leftController; // Controller represenation - interactor group
    [SerializeField] public InteractorGroupManager rightController; // Controller represenation - interactor group
    [SerializeField] public InteractorGroupManager leftXRHand; // Tracked hand represenation - interactor group
    [SerializeField] public InteractorGroupManager rightXRHand; // Tracked hand represenation - interactor group

    [HideInInspector]
    public GameObject leftControllerObject; // Controller represenation
    [HideInInspector]
    public GameObject rightControllerObject; // Controller represenation
    [HideInInspector]
    public GameObject leftXRHandObject; // Tracked hand represenation
    [HideInInspector]
    public GameObject rightXRHandObject; // Tracked hand represenation

    [Header("Grab Movement Properties")]
    [SerializeField] private float grabMovementMultiplier = 1; // modifies speed of grab movement
    [SerializeField] public MovableSpline movableSpline; // spline to move on
    [SerializeField] private GameObject leftControllerStick; // visual objects used when using grab movement
    [SerializeField] private GameObject leftHandStick; // visual objects used when using grab movement
    [SerializeField] private GameObject rightControllerStick; // visual objects used when using grab movement
    [SerializeField] private GameObject rightHandStick; // visual objects used when using grab movement


    [Header("Grab Movement")]
    [SerializeField] public bool grabMovement = true; // grab movement state indicator

    [Header("Recenter on start settings")]
    [SerializeField] private bool recenterOnStart = false;
    [SerializeField] private float timeBeforeRecenter = 2;

    // Grab movement helper properties
    private float lastLeftControllerDistance = 0;
    private float lastRightControllerDistance = 0;
    private float lastLeftHandDistance = 0;
    private float lastRightHandDistance = 0;

    // Pallete object reference
    public GameObject paletteObject;


    // Interaction mode properties - changes which interactors are active
    public enum InteractionMode
    {
        grab,
        color
    }

    [SerializeField] public InteractionMode myMode;

    // Headkeeper object reference - used for keeping posture compensation between scenes
    HeadKeeper headKeeper;

    // Tag
    public const string ObjectTag = "Origin";

    // Logging
    Loggable loggable;


    // INITIALIZATION
    void Start()
    {
        gameObject.tag = ObjectTag;

        leftControllerObject = leftController.gameObject;
        rightControllerObject = rightController.gameObject;
        leftXRHandObject = leftXRHand.gameObject;
        rightXRHandObject = rightXRHand.gameObject;

        loggable = gameObject.GetComponent<Loggable>();

        StartCoroutine(LateStart());
    }

    IEnumerator LateStart()
    {
        yield return new WaitForFixedUpdate();

        SetInteractionMode(myMode);
        SetGrabMovement(grabMovement);
        FindHeadKeeper();

        yield return new WaitForSeconds(timeBeforeRecenter);

        if(recenterOnStart) Recenter();
    }

    // UPDATE

    // Handles all changes
    void Update()
    {
        if (grabMovement)
        {
            HandleGrabMovement();
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            Recenter();
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            ResetRecenter();
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            SetHeadTransform();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            ResetHeadTransform();
        }

    }

    // Finds reference to the HeadKeeper object and uses its data for posture compensation
    // User Origin does not persist between scenes, HeadKeeper does
    private void FindHeadKeeper()
    {
        GameObject networkManagerObejct = GameObject.FindGameObjectWithTag(ServerManager.ObjectTag);
        if (networkManagerObejct != null)
        {
            headKeeper = networkManagerObejct.GetComponent<HeadKeeper>();

            if (headKeeper != null)
            {
                Vector3 originRotationBefore = gameObject.transform.eulerAngles;
                gameObject.transform.eulerAngles = Vector3.zero;

                locationOffset.transform.localEulerAngles = headKeeper.locationOffsetLocalEuler;
                locationOffset.transform.localPosition = headKeeper.locationOffsetLocalPosition;
                rotationOffset.transform.localEulerAngles = headKeeper.rotationOffsetLocalEuler;

                gameObject.transform.eulerAngles = originRotationBefore;
            }
        }
    }

    // MODES

    // Interaction mode setters and switches
    public void SwitchInteractionMode()
    {
        myMode++;
        if((int) myMode == Enum.GetNames(typeof(InteractionMode)).Length)
        {
            myMode = (InteractionMode)0;
        }
        SetInteractionMode(myMode);
    }

    public void SetInteractionMode(InteractionMode mode)
    {
        myMode = mode;

        loggable.LogEvent(LoggingGlobs.EventKeys.mode_change);
    }

    // Object Brush coloring mode setters and switches
    public void SetObjectBrushMode(ObjectBrush.ColoringModes mode)
    {
        leftController.SetObjectBrushMode(mode);
        rightController.SetObjectBrushMode(mode);
        leftXRHand.SetObjectBrushMode(mode);
        rightXRHand.SetObjectBrushMode(mode);
    }

    public void SwitchObjectBrushMode()
    {
        leftController.SwitchObjectBrushMode();
        rightController.SwitchObjectBrushMode();
        leftXRHand.SwitchObjectBrushMode();
        rightXRHand.SwitchObjectBrushMode();
    }

    // TELEPORTATION

    // Teleportation of the Origin
    public void TeleportOrigin(Vector3 position)
    {
        if (grabMovement)
        {
            SetGrabMovement(!grabMovement);
        }
        Recenter();
        gameObject.transform.position = position;
    }

    // GRAB MOVEMENT

    // If hand representation objects move towards the user camera,
    // changes of their distance is added to the distance on movable spline. 
    private void HandleGrabMovement()
    {
        float distanceSum = 0f;

        float currentLeftHandDistance = Vector3.Distance(vrCamera.transform.position, leftXRHand.transform.position);
        float currentRightHandDistance = Vector3.Distance(vrCamera.transform.position, rightXRHand.transform.position);
        float currentLeftControllerDistance = Vector3.Distance(vrCamera.transform.position, leftController.transform.position);
        float currentRightControllerDistance = Vector3.Distance(vrCamera.transform.position, rightController.transform.position);

        if (currentLeftHandDistance < lastLeftHandDistance) distanceSum += lastLeftHandDistance - currentLeftHandDistance;
        if (currentRightHandDistance < lastRightHandDistance) distanceSum += lastRightHandDistance - currentRightHandDistance;
        if (currentLeftControllerDistance < lastLeftControllerDistance) distanceSum += lastLeftControllerDistance - currentLeftControllerDistance;
        if (currentRightControllerDistance < lastRightControllerDistance) distanceSum += lastRightControllerDistance - currentRightControllerDistance;

        lastLeftHandDistance = currentLeftHandDistance;
        lastRightHandDistance = currentRightHandDistance;
        lastLeftControllerDistance = currentLeftControllerDistance;
        lastRightControllerDistance = currentRightControllerDistance;

        if(movableSpline != null)
        {
            distanceSum *= grabMovementMultiplier;
            movableSpline.AddToDistance(distanceSum);
            Vector3 newPosition = movableSpline.GetCurrentPositionFromDistance();
            transform.position = newPosition;

            Vector3 newFoward = movableSpline.GetCurrentRotationFromDistance();
            newFoward.y = 0f;

            transform.rotation = Quaternion.LookRotation(newFoward, Vector3.up);
        }
    }

    // Sets distance on movable spline
    void SetSplineDistance01(float t)
    {
        if(movableSpline != null)
        {
            movableSpline.SetT(t);
        }
    }


    // Setter for grab movement state
    public void SetGrabMovement(bool state)
    {
        grabMovement = state;

        leftControllerStick.gameObject.SetActive(state);
        rightControllerStick.gameObject.SetActive(state);
        leftHandStick.gameObject.SetActive(state);
        rightHandStick.gameObject.SetActive(state);
    }

    // HEAD ROTATION

    // Posture compensation
    public void SetHeadTransform()
    {
        ResetRecenter();
        ResetHeadEulerX();

        Recenter();
        RecenterHeadEulerX();
    }

    // Resets posture compensation
    public void ResetHeadTransform()
    {
        ResetRecenter();
        ResetHeadEulerX();

        Recenter();
    }

    // Offsets user camera rotation around the X axis
    public void RecenterHeadEulerX()
    {
        Vector3 newEuler = new Vector3(-vrCamera.transform.localEulerAngles.x, 0, 0);
        rotationOffset.transform.localEulerAngles = newEuler;
        if (headKeeper != null) headKeeper.rotationOffsetLocalEuler = newEuler;
    }

    // Resets the offset of the user camera rotation around the X axis
    public void ResetHeadEulerX()
    {
        rotationOffset.transform.localEulerAngles = Vector3.zero;
        if (headKeeper != null) headKeeper.rotationOffsetLocalEuler = Vector3.zero;
    }

    // Recenters the user camera
    public void Recenter()
    {
        ResetRecenter();
        Vector3 originRotationBefore = gameObject.transform.eulerAngles;
        gameObject.transform.eulerAngles = Vector3.zero;

        Vector3 newEuler = new Vector3(0, -vrCamera.transform.localEulerAngles.y, 0);
        locationOffset.transform.localEulerAngles = newEuler;

        float xDifference = gameObject.transform.position.x - vrCamera.transform.position.x;
        float yDifference = gameObject.transform.position.y - vrCamera.transform.position.y;
        float zDifference = gameObject.transform.position.z - vrCamera.transform.position.z;

        Vector3 newPosition = new Vector3(xDifference, -vrCamera.transform.localPosition.y, zDifference);

        locationOffset.transform.localPosition = newPosition;

        gameObject.transform.eulerAngles = originRotationBefore;

        if (headKeeper != null)
        {
            headKeeper.locationOffsetLocalPosition = newPosition;
            headKeeper.locationOffsetLocalEuler = newEuler;
        }
    }

    // Resets the camera recenter
    public void ResetRecenter() 
    {
        locationOffset.transform.localPosition = Vector3.zero;
        locationOffset.transform.localEulerAngles = Vector3.zero;
        if (headKeeper != null)
        {
            headKeeper.locationOffsetLocalPosition = Vector3.zero;
            headKeeper.locationOffsetLocalEuler = Vector3.zero;
        }
    }


    // TURNING

    // Discrete rotation around the Y axis by an angle
    public void SnapTurn(float angle)
    {
        transform.RotateAround(transform.position, Vector3.up, angle);
    }

    // Setter for rotation
    public void TurnOrigin(Vector3 rotation)
    {
        gameObject.transform.localRotation = Quaternion.Euler(rotation);
    }



    // COMMANDS -- called from client (mobile app)

    [Command(requiresAuthority = false)]
    public void CmdSwitchMode(NetworkConnectionToClient sender = null)
    {
        SwitchInteractionMode();
    }

    [Command(requiresAuthority = false)]
    public void CmdSwitchGrab(NetworkConnectionToClient sender = null)
    {
        SetGrabMovement(!grabMovement);
    }

    [Command(requiresAuthority = false)]
    public void CmdSetHeadRotation(NetworkConnectionToClient sender = null)
    {
        SetHeadTransform();
    }

    [Command(requiresAuthority = false)]
    public void CmdResetHeadRotation(NetworkConnectionToClient sender = null)
    {
        ResetHeadTransform();
    }

    [Command(requiresAuthority = false)]
    public void CmdRecenter(NetworkConnectionToClient sender = null)
    {
        Recenter();
    }

    [Command(requiresAuthority = false)]
    public void CmdResetRecenter(NetworkConnectionToClient sender = null)
    {
        ResetRecenter();
    }

    [Command(requiresAuthority = false)]
    public void CmdSwitchBrushMode(NetworkConnectionToClient sender = null)
    {
        SwitchObjectBrushMode();
    }

    [Command(requiresAuthority = false)]
    public void CmdSetSplineT(float t, NetworkConnectionToClient sender = null)
    {
        SetSplineDistance01(t);
    }
}
