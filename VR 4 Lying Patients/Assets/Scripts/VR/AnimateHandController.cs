using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

/*
 * Component which animates hand models for controllers based on user input.
 * Based on a script from https://fistfullofshrimp.com/unity-vr-inputs-and-hand-animation/
 */

[RequireComponent(typeof(Animator))]

public class AnimateHandController : MonoBehaviour
{
    [Header("Input Actions")]
    [SerializeField]
    public InputActionReference gripIAR;
    [SerializeField]
    public InputActionReference triggerIAR;

    // Helper properties
    private Animator handAnimator;
    private float gripvalue;
    private float triggervalue;

    // Initialization
    void Start()
    {
        handAnimator = GetComponent<Animator>();
    }

    // Updates the animation
    void Update()
    {
        AnimateGrip();
        AnimateTrigger();
    }

    // Animates grip input
    private void AnimateGrip()
    {
        gripvalue = gripIAR.action.ReadValue<float>();
        handAnimator.SetFloat("Grip", gripvalue);
    }

    // Animates trigger input
    private void AnimateTrigger()
    {
        triggervalue = triggerIAR.action.ReadValue<float>();
        handAnimator.SetFloat("Trigger", triggervalue);
    }
}
