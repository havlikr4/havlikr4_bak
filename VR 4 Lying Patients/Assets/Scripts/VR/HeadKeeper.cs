using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Simple component which is not destroyed on scene change and keeps data 
 * needed for posture compensation of the User Origin
 */
public class HeadKeeper : MonoBehaviour
{
    // Data for posture compensation of the User Origin
    public Vector3 rotationOffsetLocalEuler = Vector3.zero;
    public Vector3 locationOffsetLocalPosition = Vector3.zero;
    public Vector3 locationOffsetLocalEuler = Vector3.zero;
}
