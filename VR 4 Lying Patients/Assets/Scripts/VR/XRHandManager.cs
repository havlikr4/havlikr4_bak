using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * In this state of development this just sets the tag on start.
 */
public class XRHandManager : MonoBehaviour
{
    // Tag
    public const string ObjectTag = "Hand";

    // Sets the tag
    void Start()
    {
        gameObject.tag = ObjectTag;
    }

}
