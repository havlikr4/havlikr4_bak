using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Object in front of camera that fades based on other objects.
 * user for fading on scene change.
 */
[RequireComponent(typeof(Renderer))]
public class Fader : MonoBehaviour
{
    [Header("Standart Fade Duration")]
    [SerializeField]
    public float standartDuration = 2.0f;

    [Header("Fade in on start")]
    [SerializeField]
    private bool fadeInOnStart = true;

    // Renderer reference
    Renderer renderer;

    // Tag
    public const string ObjectTag = "Fader";

    // Initialization
    void Start()
    {
        renderer = GetComponent<Renderer>();
        if(fadeInOnStart) FadeIn(standartDuration);
    }

    // Standart full fades
    public void StandartFadeIn()
    {
        StartCoroutine(TimedFade(standartDuration, 1, 0));
    }

    public void StandartFadeOut()
    {
        StartCoroutine(TimedFade(standartDuration, 0, 1));
    }

    // Full fades with custom duration
    public void FadeIn(float duration)
    {
        StartCoroutine(TimedFade(duration, 1, 0));
    }

    public void FadeOut(float duration)
    {
        StartCoroutine(TimedFade(duration, 0, 1));
    }

    // Customizable fade
    public IEnumerator TimedFade(float duration, float AlphaStart, float AlphaEnd)
    {
        Color fadeColor = renderer.material.color;
        float currentTime = 0;

        while (currentTime < duration)
        {
            yield return null;
            currentTime += Time.deltaTime;

            fadeColor.a = Mathf.Lerp(AlphaStart, AlphaEnd, currentTime / duration);
            renderer.material.color = fadeColor;
        }
        fadeColor.a = AlphaEnd;
        renderer.material.color = fadeColor; 
    }
}
