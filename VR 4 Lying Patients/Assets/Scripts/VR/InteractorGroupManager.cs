using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

/*
 * Manages different interactor components and objects on one XR Hand or Controller.
 * 
 * Managed interactions are: 
 * Teleport Interactor 
 * Hand Collector
 * Object Brush 
 * Hand Charge Ray
 * Activator
 * 
 * Activator and Hand Charge Ray are always active and take precedence unless holding something in Hand Collector.
 * Teleport Interactor can turn off Hand Collector, Object Brush and Placable Palette.
 * Hand Collector and Object Brush are active while 
 * in their corresponding interaction mode.
 */

public class InteractorGroupManager : MonoBehaviour
{
    [Header("Teleport Interactor")]
    [SerializeField]
    GameObject teleporter;
    [SerializeField]
    InputActionReference teleportAction;

    [Header("Collecting Interactor")]
    [SerializeField]
    private GameObject collectorObject;
    [HideInInspector]
    public HandCollector collector;

    [Header("Coloring Interactor")]
    [SerializeField]
    private GameObject coloringObject;
    [HideInInspector]
    private PlacablePalette placablePalette;
    [HideInInspector]
    public ObjectBrush objectBrush;

    [Header("Activation Interactor")]
    [SerializeField]
    private GameObject activatorObject;
    [HideInInspector]
    public HandActivator activator;

    [Header("Charge Ray Interacto")]
    [SerializeField]
    private GameObject chargeObject;
    [HideInInspector]
    public HandChargeRay charger;
    private bool hasCharger = false;

    // User Origin and its interaction mode references
    private UserOriginManager userOriginManager;
    private UserOriginManager.InteractionMode myMode = UserOriginManager.InteractionMode.grab;

    // Initialization
    void Start()
    {
        StartCoroutine(FindInteractorComponents());
        StartCoroutine(FindUserOrigin());
    }

    // Finds Components in set interactor objects
    IEnumerator FindInteractorComponents()
    {
        yield return new WaitForFixedUpdate();

        collector = collectorObject.GetComponent<HandCollector>();
        placablePalette = coloringObject.GetComponent<PlacablePalette>();
        objectBrush = coloringObject.GetComponent<ObjectBrush>();
        activator = activatorObject.GetComponent<HandActivator>();
        if (chargeObject != null)
        {
            charger = chargeObject.GetComponent<HandChargeRay>();
            if (charger != null) hasCharger = true;
        }
    }

    // Finds the User Origin reference
    IEnumerator FindUserOrigin()
    {
        yield return new WaitForFixedUpdate();

        GameObject origin = GameObject.FindGameObjectWithTag(UserOriginManager.ObjectTag);

        if (origin != null)
        {
            userOriginManager = origin.GetComponent<UserOriginManager>();

            myMode = userOriginManager.myMode;
            switch (myMode)
            {
                case UserOriginManager.InteractionMode.grab:
                    SetGrabMode();
                    break;

                case UserOriginManager.InteractionMode.color:
                    SetColorMode();
                    break;

                default:
                    break;
            }
        }

        if (userOriginManager == null)
        {
            gameObject.SetActive(false);
        }
    }

    // Sets Objects Brush coloring mode
    public void SetObjectBrushMode(ObjectBrush.ColoringModes coloringMode)
    {
        if(objectBrush != null)
        {
            objectBrush.SetMode(coloringMode);
        }
    }

    // Switches Objects Brush coloring mode
    public void SwitchObjectBrushMode()
    {
        if (objectBrush != null)
        {
            objectBrush.SwitchMode();
        }
    }

    // Turns off Hand Collector and Object Brush objects
    void TurnOffAllInteractors()
    {
        collectorObject.SetActive(false);
        coloringObject.SetActive(false);
    }

    // Turns Hand Collector on and Object Brush off
    void SetGrabMode()
    {
        TurnOffAllInteractors();
        collectorObject.SetActive(true);
    }

    // Turns Hand Collector off and Object Brush on
    void SetColorMode()
    {
        TurnOffAllInteractors();
        coloringObject.SetActive(true);
    }

    // Turns Hand Collector and
    void SetInterctorsForCurrentMode(bool active)
    {
        switch (myMode)
        {
            case UserOriginManager.InteractionMode.grab:
                if (collectorObject != null) collectorObject.SetActive(active);
                break;

            case UserOriginManager.InteractionMode.color:
                if (objectBrush != null) objectBrush.SetPower(active);
                if (placablePalette != null) placablePalette.SetPower(active);

                break;

            default: break;
        }
    }

    // Delegates interaction mode of the User Origin to the interactor objects
    void HandleInteractionMode()
    {
        if (userOriginManager != null && myMode != userOriginManager.myMode)
        {
            myMode = userOriginManager.myMode;
            switch (myMode)
            {
                case UserOriginManager.InteractionMode.grab:
                    SetGrabMode();
                    break;

                case UserOriginManager.InteractionMode.color:
                    SetColorMode();
                    break;

                default:
                    break;
            }
        }
    }

    // Turns off Activator when holding something in Hand Collector
    void HandleCollector()
    {
        if (collector.holding)
        {
            activatorObject.SetActive(false);
        }
        else
        {
            activatorObject.SetActive(true);
        }
    }

    // Turns off interactors id charging something with Hand Charge ray 
    void HandleCharger()
    {
        if (charger.currentCharge > 0)
        {
            SetInterctorsForCurrentMode(false);
        }
    }

    // Handles all mode changes and cases
    private void Update()
    {
        HandleInteractionMode();

        if (hasCharger)
        {
            HandleCharger();
        }

        Vector2 teleportValue = teleportAction.action.ReadValue<Vector2>();

        if (activator != null && activator.activables.Count > 0)
        {
            SetInterctorsForCurrentMode(false);
        }
        else if (teleporter != null && (Mathf.Abs(teleportValue.x) > 0.5 || Mathf.Abs(teleportValue.y) > 0.5))
        {
            teleporter.SetActive(true);
            SetInterctorsForCurrentMode(false);

        }
        else
        {
            if (teleporter != null)
            {
                teleporter.SetActive(false);
            }

            SetInterctorsForCurrentMode(true);
        }

        if (myMode == UserOriginManager.InteractionMode.grab && collector != null) HandleCollector();
    }
}
