using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

/*
 * Object used for snap turning when usinf hand tracking.
 * Object is charged when colliding with an XR Hand.
 * When Charging the object increases its opacity.
 * When fully charged (and thus fully opaque) user input 
 * can be used for turning of the User Origin.
 */

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(Rigidbody))]
public class HandTurner : MonoBehaviour
{
    [Header("Time to charge up")]
    [SerializeField]
    public float chargeUpTime = 2f;


    [Header("Time before charging")]
    [SerializeField]
    public float opacityDelay = 2f;


    [Header("Turn Activation Input Actions")]
    [SerializeField]
    private InputActionReference turnInput;
    float lastinputValue;


    [Header("Angle of rotation")]
    [SerializeField]
    private float rotateAngle;


    [Header("User Origin to turn")]
    [SerializeField]
    private UserOriginManager userOriginManager;


    // Rendering helper properties
    private MeshRenderer meshRenderer;
    private Color meshColor;


    // Colision helper properties
    public List<GameObject> hands = new List<GameObject>();
    private bool somethingIn = false;
    

    // Charging helper properties
    private float fullChargeUpTime;
    private float currentCharge = 0;

    // Initialization
    void Start()
    {
        fullChargeUpTime = chargeUpTime + opacityDelay;

        Rigidbody rigidbody = GetComponent<Rigidbody>();
        if(rigidbody != null)
        {
            rigidbody.isKinematic = true;
            rigidbody.useGravity = false;
        }

        meshRenderer = GetComponent<MeshRenderer>();
        if ((meshRenderer != null))
        {
            meshColor = meshRenderer.material.color;
            meshRenderer.enabled = false;
        }        
    }

    // Handles all changes
    void Update()
    {
        HandleChargeUp();
        HandleOpacity();
        if(currentCharge == fullChargeUpTime) // Handles user input when fully charged
        {
            HandleInput();
        }
    }

    // Turns the User Origin on user Input
    void HandleInput()
    {
        float inputValue = turnInput.action.ReadValue<float>();
        if (inputValue > 0.5f && lastinputValue < 0.5f) { userOriginManager.SnapTurn(rotateAngle); }
        lastinputValue = inputValue;
    }

    // Handles charging calculation
    void HandleChargeUp()
    {
        if (currentCharge < fullChargeUpTime && hands.Count > 0)
        {
            currentCharge += Time.deltaTime;
            if (currentCharge > fullChargeUpTime) { currentCharge = fullChargeUpTime; }
        }
        else if (hands.Count == 0)
        {
            currentCharge = 0;
        }

        if (!userOriginManager.leftXRHand.gameObject.activeSelf)
        {
            currentCharge = 0;
        }
    }

    // Handles opacity change based on charging
    void HandleOpacity()
    {
        if(currentCharge <= opacityDelay)
        {
            meshRenderer.enabled = false;
            return;
        }
        meshRenderer.enabled = true;
        meshColor.a = Mathf.Clamp01((currentCharge - opacityDelay) / chargeUpTime);
        meshRenderer.material.color = meshColor;
    }

    // Clears the list of colliding hands
    // (they can get out without being detected on rapid movements and this fixxes it)
    private void FixedUpdate()
    {
        if (!somethingIn)
        {
            hands.Clear();
        }

        somethingIn = false;
    }

    // Detects XR Hands entering the collider and adds them to a list of colliding hands
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(XRHandManager.ObjectTag))
        {
            hands.Add(other.gameObject);
        }
    }

    // Detects colliding XR Hands leaving the collider and removes them from a list of colliding hands
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(XRHandManager.ObjectTag))
        {
            hands.Remove(other.gameObject);
        }
    }

    // Checks if some hands are still colliding to fix errors with detection on rapid movements
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(XRHandManager.ObjectTag))
        {
            somethingIn = true;
        }
    }

}
