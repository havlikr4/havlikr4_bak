using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using static Unity.Burst.Intrinsics.Arm;
using UnityEngine.UIElements;
using UnityEngine.XR.Interaction.Toolkit.Filtering;
using Unity.Mathematics;
using Unity.VisualScripting;

/*
 * Controls a rigged humanoid model and makes do reactions.
 * Reactions are predefined animations and audioclips.
 * Reacts to certain objects in specified collider usually results from combining.
 * Introduces itself at start and sometimes makes a random comment.
 */

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class ReactionCharacterManager : MonoBehaviour
{
    [Header("Random comment properties")]
    [SerializeField]
    float randomCommentTime = 20f;
    [HideInInspector]
    public float randomCommentTimer = 0;
    [SerializeField]
    float maxDist;

    [Header("Audioclips")]
    [SerializeField] List<AudioClip> rajskaClips = new List<AudioClip>();
    [SerializeField] List<AudioClip> smazeniceClips = new List<AudioClip>();
    [SerializeField] List<AudioClip> segedinClips = new List<AudioClip>();
    [SerializeField] List<AudioClip> bramborackaClips = new List<AudioClip>();
    [SerializeField] List<AudioClip> lecoClips = new List<AudioClip>();
    [SerializeField] List<AudioClip> cesneckaClips = new List<AudioClip>();
    [SerializeField] List<AudioClip> svickovaClips = new List<AudioClip>();
    [SerializeField] List<AudioClip> kureClips = new List<AudioClip>();
    [SerializeField] List<AudioClip> omeletaClips = new List<AudioClip>();

    [SerializeField] List<AudioClip> introductionClips = new List<AudioClip>();

    [SerializeField] List<AudioClip> poisonClips = new List<AudioClip>();

    [SerializeField] List<AudioClip> failureClipList = new List<AudioClip>();

    [SerializeField] List<AudioClip> commentClipList = new List<AudioClip>();

    [SerializeField] List<AudioClip> nearClipList = new List<AudioClip>();

    [SerializeField] List<AudioClip> namesClipList = new List<AudioClip>();

    [Header("Delays")]
    public const float TastingDelay = 2.2f;
    public const float IntroductionDelay = 3f;

    // Object used in tasting animation
    [SerializeField]
    GameObject LadleObject;

    // Object references
    Animator animator;
    AudioSource audioSource;
    UserOriginManager originManager;

    // Enums for animation triggers for the Animator component
    public enum AnimationTriggers
    {
        Idle,
        Applause,
        Crying,
        Coughing,
        Angry,
        Laughing,
        Exhausted,
        Watch,
        Tasting,
        Hungry,
        Wave
    }

    public enum FailureAnimationTriggers
    {
        Crying,
        Coughing,
        Exhausted
    }

    public enum RandomAnimationTriggers
    {
        Watch,
        Hungry
    }

    // Helper properties
    bool almostCorrect = false;
    int almostCorrectindex = 0;

    string[] names = { "Rajska" , "Svickova" , "Bramboracka" ,
        "Segedin" , "Leco", "Cesnecka",
        "Smazenice", "KureNaPaprice", "Omeleta"};



    // Initalization
    void Start()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        LadleObject.SetActive(false);
        StartCoroutine(PlayIntroduction());
    }

    // Detects new objects in the collider
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(IngredientObject.ObjectTag))
        {
            
            IngredientObject ingredient = other.gameObject.GetComponent<IngredientObject>();
            if (ingredient != null)
            {
                switch (ingredient.ingredientName)
                {
                    case "Rajska":
                        StartCoroutine(PlayDelayedAnimation(AnimationTriggers.Applause, TastingDelay));
                        StartCoroutine(PlayRandomSoundFromList(rajskaClips, TastingDelay));
                        other.gameObject.SetActive(false);
                        break;

                    case "Svickova":
                        StartCoroutine(PlayDelayedAnimation(AnimationTriggers.Applause, TastingDelay));
                        StartCoroutine(PlayRandomSoundFromList(svickovaClips, TastingDelay));
                        other.gameObject.SetActive(false);
                        break;

                    case "Bramboracka":
                        StartCoroutine(PlayDelayedAnimation(AnimationTriggers.Applause, TastingDelay));
                        StartCoroutine(PlayRandomSoundFromList(bramborackaClips, TastingDelay));
                        other.gameObject.SetActive(false);
                        break;

                    case "Segedin":
                        StartCoroutine(PlayDelayedAnimation(AnimationTriggers.Applause, TastingDelay));
                        StartCoroutine(PlayRandomSoundFromList(segedinClips, TastingDelay));
                        other.gameObject.SetActive(false);
                        break;

                    case "Leco":
                        StartCoroutine(PlayDelayedAnimation(AnimationTriggers.Applause, TastingDelay));
                        StartCoroutine(PlayRandomSoundFromList(lecoClips, TastingDelay));
                        other.gameObject.SetActive(false);
                        break;

                    case "Cesnecka":
                        StartCoroutine(PlayDelayedAnimation(AnimationTriggers.Applause, TastingDelay));
                        StartCoroutine(PlayRandomSoundFromList(cesneckaClips, TastingDelay));
                        other.gameObject.SetActive(false);
                        break;

                    case "Smazenice":
                        StartCoroutine(PlayDelayedAnimation(AnimationTriggers.Applause, TastingDelay));
                        StartCoroutine(PlayRandomSoundFromList(smazeniceClips, TastingDelay));
                        other.gameObject.SetActive(false);
                        break;

                    case "KureNaPaprice":
                        StartCoroutine(PlayDelayedAnimation(AnimationTriggers.Applause, TastingDelay));
                        StartCoroutine(PlayRandomSoundFromList(kureClips, TastingDelay));
                        other.gameObject.SetActive(false);
                        break;

                    case "Omeleta":
                        StartCoroutine(PlayDelayedAnimation(AnimationTriggers.Applause, TastingDelay));
                        StartCoroutine(PlayRandomSoundFromList(omeletaClips, TastingDelay));
                        other.gameObject.SetActive(false);
                        break;

                    case "Wrong":
                        StartCoroutine(PlayDelayedFailureAnimation(TastingDelay));
                        other.gameObject.SetActive(false);
                        break;

                    case "Poisonous":
                        StartCoroutine(PlayRandomSoundFromList(poisonClips, 0));
                        break;

                    default:
                        Debug.Log(other.name + "inside");
                        randomCommentTimer = 0;
                        break;
                }
            }
        }
    }

    // Gives the user a clue when the combination was not correct but close
    public void almostCorrectReaction(string almostResultName)
    {
        almostCorrect = true;
        for (int i = 0; i < names.Length; i++)
        {
            if(almostResultName == names[i])
            {
                almostCorrectindex = i;
            }
        }
    }

    // Random comment handling
    private void Update()
    {
        HandleRandomComment();
    }

    // Plays the introduction clip and animation
    IEnumerator PlayIntroduction()
    {
        yield return new WaitForSeconds(IntroductionDelay);
        PlayAnimation(AnimationTriggers.Wave);
        StartCoroutine(PlayRandomSoundFromList(introductionClips, 0));

        FindUserOrigin();
    }

    // Finds User Origin reference
    void FindUserOrigin()
    {
        GameObject originObject = GameObject.FindGameObjectWithTag(UserOriginManager.ObjectTag);
        if (originObject != null)
        {
            originManager = originObject.GetComponent<UserOriginManager>();
        }
    }

    // Checks if the user is near and makes a random comment after a timer runs down
    void HandleRandomComment()
    {
        randomCommentTimer += Time.deltaTime;

        if(originManager == null)
        {
            FindUserOrigin();
        }
        if(originManager == null)
        {
            return;
        }

        float distance = Vector3.Distance(transform.position, originManager.transform.position);

        if(distance < maxDist && randomCommentTimer >= randomCommentTime)
        {
            PlayRandomComment();
            randomCommentTimer = 0;
        }
    }

    // Plays the random comment clip and animation
    void PlayRandomComment()
    {
        int randomIndex = UnityEngine.Random.Range(0, Enum.GetNames(typeof(RandomAnimationTriggers)).Length);

        animator.SetTrigger(((RandomAnimationTriggers)randomIndex).ToString());
        audioSource.PlayOneShot(commentClipList[randomIndex]);
    }

    // Plays a specified animation
    public void PlayAnimation(AnimationTriggers trigger)
    {
        animator.SetTrigger(trigger.ToString());
    }

    // Plays a random clip from a specified list
    public IEnumerator PlayRandomSoundFromList(List<AudioClip> clips, float delay)
    {
        yield return new WaitForSeconds(delay);
        if (clips != null && clips.Count > 0)
        {
            int randomIndex = UnityEngine.Random.Range(0, clips.Count);
            audioSource.PlayOneShot(clips[randomIndex]);
        } 
    }

    // Plays an animation after a delay
    public IEnumerator PlayDelayedAnimation(AnimationTriggers trigger, float delay)
    {
        LadleObject.SetActive(true);
        PlayAnimation(AnimationTriggers.Tasting);
        yield return new WaitForSeconds(delay);
        LadleObject.SetActive(false);
        PlayAnimation(trigger);
        randomCommentTimer = 0;
    }

    // Plays an animation reacting to wrong result after a delay
    public IEnumerator PlayDelayedFailureAnimation(float delay)
    {
        LadleObject.SetActive(true);
        PlayAnimation(AnimationTriggers.Tasting);
        yield return new WaitForSeconds(delay);
        LadleObject.SetActive(false);
        PlayRandomFailureAnimation();
        randomCommentTimer = 0;
    }

    // Plays a random animation reacting to wrong result
    public void PlayRandomFailureAnimation()
    {
        int randomIndex = UnityEngine.Random.Range(0, Enum.GetNames(typeof(FailureAnimationTriggers)).Length);

        animator.SetTrigger(((FailureAnimationTriggers)randomIndex).ToString());

        if (almostCorrect) StartCoroutine(playAlmostCorrectReaction());
        else audioSource.PlayOneShot(failureClipList[randomIndex]);
    }

    // Plays a random animation reacting to correct result
    public IEnumerator playAlmostCorrectReaction()
    {
        int randomIndex = UnityEngine.Random.Range(0, nearClipList.Count);

        audioSource.PlayOneShot(nearClipList[randomIndex]);
        yield return new WaitForSeconds(nearClipList[randomIndex].length);
        audioSource.PlayOneShot(namesClipList[almostCorrectindex]);

        almostCorrect = false;
    }
}



