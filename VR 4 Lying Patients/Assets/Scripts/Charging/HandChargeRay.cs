using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

/*
 * Raycasting object located in users hand.
 * Used for charging chargable objects.
 */

[RequireComponent(typeof(LineRenderer))]
public class HandChargeRay : MonoBehaviour
{
    [Header("Ray Specification")]
    [SerializeField]
    private Vector3 rayDirection = Vector3.forward;
    [SerializeField]
    private float rayMaxLenght = 20f;

    // Charging helper properties
    private float chargeUpTime = 2f;
    public float currentCharge = 0;

    // Raycast properties
    bool hit;
    RaycastHit rayHit;
    Vector3 rayStart;
    Vector3 rayEnd;

    // Line rendering properties
    public LineRenderer lineRenderer;
    public Color lineColor;

    // Helper properties
    ChargableObject lastTargeted = null;


    [Header("Activation Input Action")]
    [SerializeField]
    private InputActionReference activationInput;
    float lastinputValue;

    // Initialization
    private void OnEnable()
    {
        InitializeProperties();
    }

    void Start()
    {
        InitializeProperties();
    }

    private void InitializeProperties()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = 2;
        lastinputValue = 0;
    }

    // Handles per update methods
    void Update()
    {
        HandleRayCast();
        HandleLineRendering();
        HandleOpacity();

        if (lastTargeted != null && lastTargeted.currentCharge == lastTargeted.chargeUpTime)
        {
            HandleInput();
        }
    }

    // Handles raycasting - if ray hits chargable object, handles charging
    void HandleRayCast()
    {
        rayStart = gameObject.transform.position;
        rayEnd = gameObject.transform.position + (gameObject.transform.rotation * rayDirection * rayMaxLenght);

        hit = Physics.Raycast(rayStart, gameObject.transform.rotation * rayDirection, out rayHit, rayMaxLenght);

        if (hit && rayHit.collider.gameObject.CompareTag(ChargableObject.ObjectTag))
        {
            ChargableObject hitChargable = rayHit.collider.gameObject.GetComponent<ChargableObject>();

            if (hitChargable != null)
            {
                hitChargable.charging = true;

                if (lastTargeted != null && hitChargable != lastTargeted)
                {
                    lastTargeted.charging = false;
                }

                lastTargeted = hitChargable;

                chargeUpTime = hitChargable.chargeUpTime;
                currentCharge = hitChargable.currentCharge;
            }
        }
        else if (lastTargeted != null)
        {
            lastTargeted.charging = false;
            lastTargeted = null;
            currentCharge = 0;
        }
    }

    // Handles raycast line rendering
    void HandleLineRendering()
    {
        lineRenderer.SetPosition(0, rayStart);
        if (hit)
        {
            lineRenderer.SetPosition(1, rayHit.point);
        }
        else
        {
            lineRenderer.SetPosition(1, rayEnd);
        }
    }

    // Handles line opacity
    void HandleOpacity()
    {
        if (currentCharge <= chargeUpTime/6)
        {
            lineRenderer.enabled = false;
            return;
        }
        lineRenderer.enabled = true;

        Color lineColor = lineRenderer.material.color;
        lineColor.a = Mathf.Clamp01(currentCharge / chargeUpTime);
        lineRenderer.material.color = lineColor;
    }

    // Handles chargable activation based on user input
    void HandleInput()
    {
        float inputValue = activationInput.action.ReadValue<float>();
        if (inputValue > 0.5f && lastinputValue < 0.5f && hit) { lastTargeted.MyFunction(); }
        lastinputValue = inputValue;
    }
}
