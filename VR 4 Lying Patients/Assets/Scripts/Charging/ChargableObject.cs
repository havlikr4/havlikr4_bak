using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

/* 
 * Base for objects chargable with Hand Charge ray. 
 * Objects are charged for by being hit by the ray. 
 * Charging is indicated by change of alpha of a gizmo object.
 * Upon reaching full charge, object can be activated by user input.
 */

[RequireComponent(typeof(NetworkIdentity))]
[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]
public abstract class ChargableObject : NetworkBehaviour
{
    [Header("Time to charge up")]
    [SerializeField]
    public float chargeUpTime = 3f;

    [Header("Error Window Settings")]
    [SerializeField]
    private float errorTime = 1f;
    private float errorTimer = 0;

    [Header("Gizmo Object")]
    [SerializeField]
    GizmoObject gizmoObject;
    [SerializeField]
    private float minOpacity = 0.1f;

    [Header("Distance from player to switch off")]
    [SerializeField]
    private float turnOffDist = 2f;

    // Charging helper properties
    public float currentCharge = 0;
    public bool charging;

    // Colision helper components
    BoxCollider collider;
    Rigidbody rigidbody;

    // Layer properties
    const int defaultLayer = 0;
    const int ignoreRayCast = 2;

    // User Origin
    UserOriginManager originManager;

    // Tag
    public const string ObjectTag = "Chargable";

    // Customizable function used after full charge up
    public abstract void MyFunction();

    // Customizable initializion
    public abstract IEnumerator FindMyObjects();


    void Start()
    {
        // Turn off on client to reduce visual clutter
        if (!isServer)
        {
            this.enabled = false;
            Debug.Log("Disabled " + this.gameObject.name);
        }

        // Initialization
        gameObject.tag = ObjectTag;
        charging = false;
        StartCoroutine(FindMyObjects());
        collider = GetComponent<BoxCollider>();
        rigidbody = GetComponent<Rigidbody>();
        if (rigidbody != null)
        {
            rigidbody.isKinematic = true;
        }
    }


    void Update()
    {
        HandleDistance();
        HandleChargeUp();
        HandleOpacity();
    }

    // Initialization in case object was deactivated
    private void OnEnable()
    {
        gameObject.tag = ObjectTag;
        charging = false;
        StartCoroutine(FindMyObjects());
        collider = GetComponent<BoxCollider>();
        rigidbody = GetComponent<Rigidbody>();
        if (rigidbody != null)
        {
            rigidbody.isKinematic = true;
        }
    }

    // Charge up on 
    void HandleChargeUp()
    {
        if (currentCharge < chargeUpTime && charging)
        {
            currentCharge += Time.deltaTime;
            errorTimer = 0;
            if (currentCharge > chargeUpTime) { currentCharge = chargeUpTime; }
        }
        else if (!charging)
        {
            errorTimer += Time.deltaTime;
            if (errorTimer > errorTime)
            {
                currentCharge = 0;
            }
        }
    }

    // Handles alpha change of gizmo object
    void HandleOpacity()
    {
        gizmoObject.ChangeAlpha(Mathf.Clamp01(currentCharge + minOpacity * chargeUpTime / chargeUpTime));
    }

    // Power is switched on and off based on proximity to User Origin
    public void PowerSwitch()
    {
        gizmoObject.gizmoRenderer.enabled = !gizmoObject.gizmoRenderer.enabled;
        if(gameObject.layer == defaultLayer)
        {
            gameObject.layer = ignoreRayCast;
        }
        else gameObject.layer = defaultLayer;
    }

    public void SwitchOff()
    {
        gizmoObject.gizmoRenderer.enabled = false;
        gameObject.layer = ignoreRayCast;
    }

    public void SwitchOn()
    {
        gizmoObject.gizmoRenderer.enabled = true;
        gameObject.layer = defaultLayer;
    }


    void FindUserOrigin()
    {
        GameObject origin = GameObject.FindWithTag(UserOriginManager.ObjectTag);
        if(origin != null) originManager = origin.GetComponent<UserOriginManager>();
    }

    // Handles power switching based on proximity to user
    void HandleDistance()
    {
        if(originManager == null)
        {
            FindUserOrigin();
        }
        if(originManager != null)
        {
            float dist = Vector3.Distance(originManager.gameObject.transform.position, transform.position);

            if(dist < turnOffDist) SwitchOff();
            else SwitchOn();
        }
    }
}
