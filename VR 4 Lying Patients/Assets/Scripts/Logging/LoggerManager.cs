using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * Script used for logging control from the mobile app.
 */
public class LoggerManager : NetworkBehaviour
{
    // Logger reference
    MyLogger logger;

    // Tag
    public const string ObjectTag = "LoggerManager";

    // Initialization
    void Start()
    {
        gameObject.tag = ObjectTag;

        StartCoroutine(InitializeManagers());
    }

    IEnumerator InitializeManagers()
    {
        yield return null;

        GameObject networkManager = GameObject.FindWithTag(ServerManager.ObjectTag);
        if (networkManager != null)
        {
            logger = networkManager.GetComponent<MyLogger>();
        } 
    }

    // Methods user by commands from the mobile app
    void StartLogging()
    {
        if(logger != null)
        {
            logger.StartLog();
        }
    }

    void EndLogging()
    {
        if (logger != null)
        {
            logger.EndLog();
        }
    }

    public void SceneTransition(string newSceneName)
    {
        if (logger != null)
        {
            logger.SceneTransition(newSceneName);
        }
    }

    // COMMANDS -- called from client (mobile app)
    [Command(requiresAuthority = false)]
    public void CmdStartLogging(NetworkConnectionToClient sender = null)
    {
        StartLogging();
    }


    [Command(requiresAuthority = false)]
    public void CmdEndLogging(NetworkConnectionToClient sender = null)
    {
        EndLogging();
    }
}
