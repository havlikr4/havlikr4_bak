using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Simple component that enables another script to log specified event.
 */
public class Loggable : MonoBehaviour
{
    // Logger reference
    MyLogger myLogger;

    // Initialization
    void Start()
    {
        FindMyLogger();
    }

    // Finds logger reference
    void FindMyLogger()
    {
        GameObject managerObject = GameObject.FindGameObjectWithTag(ServerManager.ObjectTag);
        if (managerObject != null)
        {
            myLogger = managerObject.GetComponent<MyLogger>();
        }
    }

    // Method called by another script to log a certain event
    public void LogEvent(LoggingGlobs.EventKeys key)
    {
        if(myLogger == null)
        {
            FindMyLogger();
        }
        if(myLogger != null)
        {
            myLogger.LogEvent(key);
        }
    }
}
