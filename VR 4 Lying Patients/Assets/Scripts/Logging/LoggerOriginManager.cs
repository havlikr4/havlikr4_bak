using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Object used for logging of head and hands. Persists through scenes.
 */
public class LoggerOriginManager : MonoBehaviour
{
    // Objects used for logging
    [SerializeField]
    GameObject logHead;
    [SerializeField]
    GameObject logRightHand;
    [SerializeField]
    GameObject logLeftHand;

    // Objects used as refernces, parts of Network Origin, set by MyLogger.
    [HideInInspector] public GameObject head;
    [HideInInspector] public GameObject rightHand;
    [HideInInspector] public GameObject leftHand;

    private void LateUpdate()
    {
        if(head != null) logHead.transform.SetPositionAndRotation(head.transform.position, head.transform.rotation);
        if(leftHand != null) logLeftHand.transform.SetPositionAndRotation(leftHand.transform.position, leftHand.transform.rotation);
        if(rightHand != null) logRightHand.transform.SetPositionAndRotation(rightHand.transform.position, rightHand.transform.rotation);
    }

}
