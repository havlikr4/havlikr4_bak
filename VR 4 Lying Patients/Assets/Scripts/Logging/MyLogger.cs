using Editor;
using Editor.Classes;
using Mirror;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static TMPro.SpriteAssetUtilities.TexturePacker_JsonArray;

/*
 * Script used to control logging to VrDashboard via VrLogger.
 */
[RequireComponent(typeof(VrLogger))]
public class MyLogger : MonoBehaviour
{
    [Header("Logger for VR DashBoard")]
    [SerializeField]
    private VrLogger vrLogger;

    [Header("Dropdown used for participant choosing")]
    [SerializeField]
    private Dropdown participantDropdown;

    [Header("LoggerOrigin used for head and hands logging")]
    [SerializeField]
    private LoggerOriginManager logOriginManager;

    // Participants in app
    List<Participant> myParticipants = new List<Participant>();

    // Server manager reference
    ServerManager serverManager;

    // Logging state
    public bool logging = false;

    // Initialization
    void Start()
    {
        if(vrLogger == null)
        {
            Debug.Log("No vrLogger set");
        }

        StartCoroutine(InitializeManager());

        vrLogger.SetOrganisation("dlDEky");

        GetPariticpants();
    }

    IEnumerator InitializeManager()
    {
        yield return null;

        GameObject managerObject = GameObject.FindWithTag("NetworkManager");
        if (managerObject != null) serverManager = managerObject.GetComponent<ServerManager>();
    }

    // Gets participants from VR Dashboard server
    public void GetPariticpants()
    {
        vrLogger.GetParticipants(list =>
        {
            // Check if list is null -> if yes, than request completed with error!
            if (list == null)
            {
                return;
            }

            List<Dropdown.OptionData> participantOptionData = new List<Dropdown.OptionData>();

            myParticipants.Clear();

            foreach ( var participant in list)
            {
                Dropdown.OptionData participantData = new Dropdown.OptionData();
                 

                participantData.text = participant.nickname;

                participantOptionData.Add(participantData);
                myParticipants.Add(participant);
            }

            participantDropdown.AddOptions(participantOptionData);
        });
    }

    // Sets object references 
    public void SetParts(UserOriginNetworkManager networkOrigin)
    {
        logOriginManager.head = networkOrigin.clientHead;
        logOriginManager.rightHand = networkOrigin.clientRightHand;
        logOriginManager.leftHand = networkOrigin.clientLeftHand;
    }

    // Starts logging an activity
    public void StartLog()
    {
        if (logging) return;

        vrLogger.SetParticipant(myParticipants[participantDropdown.value].id);

        vrLogger.InitializeLogger();

        vrLogger.SetCustomData("{\"custom\": 1}");

        vrLogger.StartLogging(SceneManager.GetActiveScene().name);

        logging = true;
    }

    // Sets new environment on scene change
    public void SceneTransition(string newSceneName)
    {
        if(!logging) return;

        vrLogger.SetEnvironment(newSceneName);
    }

    // Ends logging of an activity
    public void EndLog()
    {
        if (!logging) return;

        vrLogger.StopLogging();

        // Send logged Activity to server, optional can set saving logs to local file
        vrLogger.SendActivity(response =>
        {
            Debug.Log(response);
        }, false);

        logging = false;
    }

    // Used by Loggable objects to log events
    public void LogEvent(LoggingGlobs.EventKeys key)
    {
        if (!logging) return;

        vrLogger.SetEvent(key.ToString());
    }

}
