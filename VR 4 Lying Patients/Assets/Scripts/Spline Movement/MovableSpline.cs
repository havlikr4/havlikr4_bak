using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Splines;

/*
 * Spline used for grab movement.
 * Stores the current distance.
 * Used to get position and direction on the spline.
 */
[RequireComponent(typeof(SplineContainer))]
public class MovableSpline : MonoBehaviour
{
    // Spline reference
    public SplineContainer splineContainer;

    // Spline position and distance properties
    public Vector3 currentPosition = Vector3.zero;
    private float maxLenght = 0f;
    public float currentDistance = 0f;
    public float currentT = 0f;
    
    // Initialization
    void Start()
    {
        splineContainer = GetComponent<SplineContainer>();
        maxLenght = splineContainer.Spline.GetLength();
    }

    // Sets the parameter t and calculates the current distance
    public void SetT(float t)
    {
        float clampedT = Mathf.Clamp01(t);
        currentDistance = clampedT * maxLenght;
    }

    // Adds to the current distance on the spline
    public void AddToDistance(float distance)
    {
        currentDistance += distance;
        currentT = Mathf.Clamp01(currentDistance / maxLenght);
    }

    // Returns current position from the current distance
    public Vector3 GetCurrentPositionFromDistance()
    {
        currentPosition = splineContainer.Spline.EvaluatePosition(currentT);
        currentPosition += transform.position;

        return currentPosition;
    }

    // Returns current rotation / direction from the current distance
    public Vector3 GetCurrentRotationFromDistance()
    {
        Vector3 newRotation = splineContainer.Spline.EvaluateTangent(currentT);

        return newRotation;
    }
}
