﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtualFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

struct ConcurrentDictionary_2_tB4EC2B83D67D63FEC255B86DA96A1B9F0FEEAC3F;
struct Dictionary_2_tCAD1DB714FE7CE64DFBD12C9AC15D2C23DDC6329;
struct Dictionary_2_t182C3AD5398AA6215D318D59CC83975DFFD64E6F;
struct Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA;
struct Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37;
struct IDictionary_2_tE26BE4E8A62DBCA4C006BD61298BE336187E5691;
struct IDictionary_2_t252871A70AEDDC797EB0E329D3126F877A7C7C08;
struct IDictionary_2_t823399AD16F88CDEB25958D8CB61771376A5ADED;
struct IDictionary_2_t675DE936AD55F2E45DFA9EFF1224405BF88820F8;
struct IDictionary_2_t9C08D9353AC1C7C3DDFB157D85D7F9AB8971821E;
struct IEnumerable_1_tF95C9E01A913DD50575531C8305932628663D9E9;
struct IEnumerable_1_t349E66EC5F09B881A8E52EE40A1AB9EC60E08E44;
struct IEqualityComparer_1_t23FB11DB53CCF7E88AE0439AD18EBC6FA8F7ADEA;
struct IEqualityComparer_1_t2CA7720C7ADCCDECD3B02E45878B4478619D5347;
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
struct KeyCollection_t990C0D1DCFB14BE5EC9B9DC1C42CD64924DA289C;
struct KeyCollection_tF2F6703F1C0384120503C3A90BD9DD7B8D252DC8;
struct KeyCollection_tB45A861D090B15129521119AE48ED3813820A974;
struct KeyCollection_tF8B8A6459322625D7F08C99265BDC0AACB1B6682;
struct List_1_t01207CE5982A7640E56B1F9F672A06F96B09367A;
struct List_1_t56AD165E2D1D1C6E29E2BFFEE4F17D5D635D72B7;
struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD;
struct ValueCollection_t7910E0A81521B10ADD61F07E087C6766DE84E2B4;
struct ValueCollection_tDE08377185B0ACD11DD5DE09F914558808A609BA;
struct ValueCollection_t0DE9E59612CC98D0AEF2349FD23F6B4E1A991BBE;
struct EntryU5BU5D_tE3A30317A6A3AEAF1537ACD84BF40D61AEA4EF0E;
struct EntryU5BU5D_tA9C23EE48D3914D3F9664B29C40F824EE667C9F3;
struct EntryU5BU5D_tCB9C69B6DE7C6C9047A311D1B9E3937200569013;
struct Int32U5BU5DU5BU5D_t179D865D5B30EFCBC50F82C9774329C15943466E;
struct Asn1EncodableU5BU5D_t9868017FA96E51C6C30CD73898AFD5C50CF466AF;
struct BigIntegerU5BU5D_tFB92569678018498E1F36219AFF02E9250782B39;
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct DerObjectIdentifierU5BU5D_tD2CFD10A2D7EFA54014F4A5FDA4A3DA8A852F42F;
struct ECFieldElementU5BU5D_t973BBFE465E171F079533BA486672CB4206ACDB9;
struct GeneralNameU5BU5D_t07BC3E42A4CC987214EE637308752076E0F0957A;
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
struct UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA;
struct AlgorithmIdentifier_t5DDD4F0B54E24F2F4C577EA87742195C38682D40;
struct AnssiObjectIdentifiers_t844F7B339921BED06CC1D1EE7670EB2FAAE8CD2B;
struct ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263;
struct Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389;
struct Asn1EncodableVector_t57A80683315794E6BD14B3D2368B95FE29304EC3;
struct Asn1Object_t88A4CCA55F497755711F405EE399C0720E53976B;
struct Asn1OctetString_tAF69E28A303117F468ADBD6806BEC3BA4F787158;
struct Asn1Sequence_t90471C8D847766B1B77A7AB098A9E8A7DF38A263;
struct Asn1TaggedObject_t4172254B2BA64C9B954F304147C2E057FCE20DD2;
struct BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB;
struct CultureAwareComparer_t5822A6535A6EB4C448D1B7736067D1188BAEE8CD;
struct DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7;
struct DerSequence_tCC88E8724998083FAABF65D386A19D9C8C88C962;
struct DigestInfo_tCA55423EC3809B4806046638EC406D849FFEC74A;
struct ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094;
struct ECEndomorphism_t839F172C91D24A2AADEBDC5DED0E8EAAA4545117;
struct ECFieldElement_t806952B11C8B5CF0C11442149FD35DF4327029ED;
struct ECMultiplier_tFD2488B9A30A4B152F5B9967BF8049A727D731F0;
struct ECPoint_t0CE8D3D2A088BBE0E67B3DD87EA91AA0956659BC;
struct FpCurve_t9C4D086FD235C2976984476721DF8DEAF8BA86A1;
struct FpPoint_tE44568768E01C534DA64F9FE4CA0D52D97FD5588;
struct GeneralName_tF34D1432388833854897D2F99AE6DC6A9896C6A5;
struct GeneralNames_t9B20892C17FAA17729EE7445077B67D79DFEF3F4;
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
struct IFiniteField_t9ACA5E39A46FFF2DE2675384AEFF91CB9391738E;
struct LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23;
struct OrdinalCaseSensitiveComparer_t581CA7CB51DCF00B6012A697A4B4B3067144521A;
struct OrdinalIgnoreCaseComparer_t8BAE11990A4C855D3BCBBFB42F4EF8D45088FBB0;
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
struct String_t;
struct StringComparer_t6268F19CA34879176651429C0D8A3D0002BB8E06;
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
struct X509Name_t02B17BA8A942D5E353104A8874F958D86337D136;
struct X509NameEntryConverter_tE492BAC5C74EBD3CE9E4ED2CD0391AE7897C16ED;
struct X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88;
struct X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76;
struct X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F;
struct X9FieldID_t0ACE1620F63F09DEAC1AC8F3EA68E13012242EFB;
struct Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791;

IL2CPP_EXTERN_C RuntimeClass* AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AnssiObjectIdentifiers_t844F7B339921BED06CC1D1EE7670EB2FAAE8CD2B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Asn1EncodableVector_t57A80683315794E6BD14B3D2368B95FE29304EC3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DerSequence_tCC88E8724998083FAABF65D386A19D9C8C88C962_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t182C3AD5398AA6215D318D59CC83975DFFD64E6F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tCAD1DB714FE7CE64DFBD12C9AC15D2C23DDC6329_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FpCurve_t9C4D086FD235C2976984476721DF8DEAF8BA86A1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Hex_tE1EF40FEEC6279861917D4DFB9CCC75FD8B36CA1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringComparer_t6268F19CA34879176651429C0D8A3D0002BB8E06_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WNafUtilities_tBF72FF38EF1BD4A84B2D2430BEE2D65AFB43891E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* X509Name_t02B17BA8A942D5E353104A8874F958D86337D136_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral1614BBC64F278A24D44F6470C16864678D530B54;
IL2CPP_EXTERN_C String_t* _stringLiteral2309CB260B39EE33B2041CC894F06287D8CACD0B;
IL2CPP_EXTERN_C String_t* _stringLiteralB387E0FFBFB7595C7AC8F04331A06CE8077E4BE9;
IL2CPP_EXTERN_C String_t* _stringLiteralB5C4768C151D248CB78691E1386433A0653C5D1F;
IL2CPP_EXTERN_C String_t* _stringLiteralB608113BA5D284442B18D8F123B8242EFFD9B50E;
IL2CPP_EXTERN_C String_t* _stringLiteralBCDB7300C3DB42608F4FD5306F6DB1A47E67D4CB;
IL2CPP_EXTERN_C String_t* _stringLiteralD7926AF4A18D1FAF85D168A423D77A8BEF3439BF;
IL2CPP_EXTERN_C String_t* _stringLiteralE7AAE942B604FEEFC2327E2399341CDCAD41B668;
IL2CPP_EXTERN_C const RuntimeMethod* CollectionUtilities_GetValueOrNull_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_TisString_t_mD7022EF1BC5D7215073BAB672FE85947718874EA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CollectionUtilities_GetValueOrNull_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_TisX9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76_mFFF6119751EFFE0F4BFE1CEABE6B98DE4BB7887D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CollectionUtilities_GetValueOrNull_TisString_t_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_mFE4804AAB5D611FAA5449945BFB5D25099CFCB29_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CollectionUtilities_Proxy_TisString_t_m6E2841A61A0D5E4BA6D3130680264F4BE597A573_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m60955C5A4F99886288BAED2052459C790963A0A5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m6E2846ACFDB0C08FCDF5CA041846F3DD94AE525F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mE8A0BC751794ED911A168EEC22705B6AE1281D52_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m3C0AEA796137AC0AE0830766066A09DB45BA1FA8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m73D6F97375AB4C5B363D6E7F565EAAEBC8194468_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mAB97FFC5A0BBFBF98AFCEE0A10E07EB1D77736FC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Keys_mCEE30023E7EA59ED772D73F6CB371999419CB44F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedCertificate__ctor_m3DB0B15D87C2BE7091C77D0B7BE13B0D827CBD50_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
struct Dictionary_2_tCAD1DB714FE7CE64DFBD12C9AC15D2C23DDC6329  : public RuntimeObject
{
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets;
	EntryU5BU5D_tE3A30317A6A3AEAF1537ACD84BF40D61AEA4EF0E* ____entries;
	int32_t ____count;
	int32_t ____freeList;
	int32_t ____freeCount;
	int32_t ____version;
	RuntimeObject* ____comparer;
	KeyCollection_t990C0D1DCFB14BE5EC9B9DC1C42CD64924DA289C* ____keys;
	ValueCollection_t7910E0A81521B10ADD61F07E087C6766DE84E2B4* ____values;
	RuntimeObject* ____syncRoot;
};
struct Dictionary_2_t182C3AD5398AA6215D318D59CC83975DFFD64E6F  : public RuntimeObject
{
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets;
	EntryU5BU5D_tA9C23EE48D3914D3F9664B29C40F824EE667C9F3* ____entries;
	int32_t ____count;
	int32_t ____freeList;
	int32_t ____freeCount;
	int32_t ____version;
	RuntimeObject* ____comparer;
	KeyCollection_tF2F6703F1C0384120503C3A90BD9DD7B8D252DC8* ____keys;
	ValueCollection_tDE08377185B0ACD11DD5DE09F914558808A609BA* ____values;
	RuntimeObject* ____syncRoot;
};
struct Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37  : public RuntimeObject
{
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets;
	EntryU5BU5D_tCB9C69B6DE7C6C9047A311D1B9E3937200569013* ____entries;
	int32_t ____count;
	int32_t ____freeList;
	int32_t ____freeCount;
	int32_t ____version;
	RuntimeObject* ____comparer;
	KeyCollection_tF8B8A6459322625D7F08C99265BDC0AACB1B6682* ____keys;
	ValueCollection_t0DE9E59612CC98D0AEF2349FD23F6B4E1A991BBE* ____values;
	RuntimeObject* ____syncRoot;
};
struct KeyCollection_tF8B8A6459322625D7F08C99265BDC0AACB1B6682  : public RuntimeObject
{
	Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37* ____dictionary;
};
struct U3CPrivateImplementationDetailsU3E_t71B56FD9E998D4F52FFF04CC1A26784A143731EE  : public RuntimeObject
{
};
struct AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA  : public RuntimeObject
{
};
struct AnssiObjectIdentifiers_t844F7B339921BED06CC1D1EE7670EB2FAAE8CD2B  : public RuntimeObject
{
};
struct Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389  : public RuntimeObject
{
};
struct Asn1EncodableVector_t57A80683315794E6BD14B3D2368B95FE29304EC3  : public RuntimeObject
{
	Asn1EncodableU5BU5D_t9868017FA96E51C6C30CD73898AFD5C50CF466AF* ___elements;
	int32_t ___elementCount;
	bool ___copyOnWrite;
};
struct BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB  : public RuntimeObject
{
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ___magnitude;
	int32_t ___sign;
	int32_t ___nBits;
	int32_t ___nBitLength;
};
struct ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094  : public RuntimeObject
{
	RuntimeObject* ___m_field;
	ECFieldElement_t806952B11C8B5CF0C11442149FD35DF4327029ED* ___m_a;
	ECFieldElement_t806952B11C8B5CF0C11442149FD35DF4327029ED* ___m_b;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___m_order;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___m_cofactor;
	int32_t ___m_coord;
	RuntimeObject* ___m_endomorphism;
	RuntimeObject* ___m_multiplier;
	RuntimeObject* ___m_preCompTable;
};
struct ECPoint_t0CE8D3D2A088BBE0E67B3DD87EA91AA0956659BC  : public RuntimeObject
{
	ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* ___m_curve;
	ECFieldElement_t806952B11C8B5CF0C11442149FD35DF4327029ED* ___m_x;
	ECFieldElement_t806952B11C8B5CF0C11442149FD35DF4327029ED* ___m_y;
	ECFieldElementU5BU5D_t973BBFE465E171F079533BA486672CB4206ACDB9* ___m_zs;
	RuntimeObject* ___m_preCompTable;
};
struct String_t  : public RuntimeObject
{
	int32_t ____stringLength;
	Il2CppChar ____firstChar;
};
struct StringComparer_t6268F19CA34879176651429C0D8A3D0002BB8E06  : public RuntimeObject
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};
struct X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76  : public RuntimeObject
{
	ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* ___m_curve;
	X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88* ___m_parameters;
};
struct AbstractFpCurve_t6B02E9A3B6AC21D51607A74C4D3DF58EA16958C0  : public ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094
{
};
struct Asn1Object_t88A4CCA55F497755711F405EE399C0720E53976B  : public Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389
{
};
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	bool ___m_value;
};
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	uint8_t ___m_value;
};
struct DigestInfo_tCA55423EC3809B4806046638EC406D849FFEC74A  : public Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389
{
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___digest;
	AlgorithmIdentifier_t5DDD4F0B54E24F2F4C577EA87742195C38682D40* ___algID;
};
struct GeneralName_tF34D1432388833854897D2F99AE6DC6A9896C6A5  : public Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389
{
	int32_t ___m_tag;
	Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389* ___m_object;
};
struct GeneralNames_t9B20892C17FAA17729EE7445077B67D79DFEF3F4  : public Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389
{
	GeneralNameU5BU5D_t07BC3E42A4CC987214EE637308752076E0F0957A* ___m_names;
};
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	int32_t ___m_value;
};
struct IntPtr_t 
{
	void* ___m_value;
};
struct LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23  : public Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389
{
	DigestInfo_tCA55423EC3809B4806046638EC406D849FFEC74A* ___mDigest;
	GeneralName_tF34D1432388833854897D2F99AE6DC6A9896C6A5* ___mCertLocation;
	X509Name_t02B17BA8A942D5E353104A8874F958D86337D136* ___mCertIssuer;
	GeneralNames_t9B20892C17FAA17729EE7445077B67D79DFEF3F4* ___mCACerts;
};
struct OrdinalComparer_tBB06915E213A5D4C8C617ED5478E8BF30C2B2170  : public StringComparer_t6268F19CA34879176651429C0D8A3D0002BB8E06
{
	bool ____ignoreCase;
};
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};
struct X509Name_t02B17BA8A942D5E353104A8874F958D86337D136  : public Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389
{
	List_1_t56AD165E2D1D1C6E29E2BFFEE4F17D5D635D72B7* ___m_ordering;
	X509NameEntryConverter_tE492BAC5C74EBD3CE9E4ED2CD0391AE7897C16ED* ___converter;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___m_values;
	List_1_t01207CE5982A7640E56B1F9F672A06F96B09367A* ___m_added;
	Asn1Sequence_t90471C8D847766B1B77A7AB098A9E8A7DF38A263* ___seq;
};
struct X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88  : public Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389
{
	X9FieldID_t0ACE1620F63F09DEAC1AC8F3EA68E13012242EFB* ___fieldID;
	ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* ___curve;
	X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F* ___g;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___n;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___h;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___seed;
};
struct X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F  : public Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389
{
	Asn1OctetString_tAF69E28A303117F468ADBD6806BEC3BA4F787158* ___encoding;
	ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* ___c;
	ECPoint_t0CE8D3D2A088BBE0E67B3DD87EA91AA0956659BC* ___p;
};
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3__padding[1024];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D1032_t7114CFA064AF3D04F8E9AF9A3F185F34AB8419BA 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D1032_t7114CFA064AF3D04F8E9AF9A3F185F34AB8419BA__padding[1032];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D11_t28A27E28E6854B15E7BDDCC03EBBABD075CCB33C 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D11_t28A27E28E6854B15E7BDDCC03EBBABD075CCB33C__padding[11];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D112_t294E07C274A143443438C5FFF354DC21C14DC7D6 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D112_t294E07C274A143443438C5FFF354DC21C14DC7D6__padding[112];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D1152_tC1CC4A1DD00CBFF8FFC237F6E7216D5AC09D320B 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D1152_tC1CC4A1DD00CBFF8FFC237F6E7216D5AC09D320B__padding[1152];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D116_tAFCAA0A6BBF3C7D612817453792131CAF1600CD7 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D116_tAFCAA0A6BBF3C7D612817453792131CAF1600CD7__padding[116];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C__padding[12];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D120_tAD8E3F872D03771C28D56F19BA94D0F528874DAE 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_tAD8E3F872D03771C28D56F19BA94D0F528874DAE__padding[120];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D124_t1CA7986512862C38F256E518A9043A19DAEABA20 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D124_t1CA7986512862C38F256E518A9043A19DAEABA20__padding[124];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D128_t132F46F03AB9DDB64721E907BC73DEC9034183A1 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D128_t132F46F03AB9DDB64721E907BC73DEC9034183A1__padding[128];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D132_tFCBB1508E3F4AC3E15548F957F3C60AFADE17D8B 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D132_tFCBB1508E3F4AC3E15548F957F3C60AFADE17D8B__padding[132];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D14_t2779C6F98DE4C55CC13496418B79E30D26219928 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D14_t2779C6F98DE4C55CC13496418B79E30D26219928__padding[14];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED__padding[16];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D160_tD5E7DEAF0A9ACE47497C4D5203883BE2605035EC 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D160_tD5E7DEAF0A9ACE47497C4D5203883BE2605035EC__padding[160];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D17_t23DF19AEF84B77E016C2F3523830A08A46E6C217 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D17_t23DF19AEF84B77E016C2F3523830A08A46E6C217__padding[17];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D19_t72AF42BF3C6C3B98D8DEF0B56A8AB18A714BA712 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D19_t72AF42BF3C6C3B98D8DEF0B56A8AB18A714BA712__padding[19];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D192_t619ED94196E112EB663AFC2D8F81A9DEA2E2450D 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D192_t619ED94196E112EB663AFC2D8F81A9DEA2E2450D__padding[192];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D20_tBBC315B0EB56B25AE323C40681A72237C83BC90F 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_tBBC315B0EB56B25AE323C40681A72237C83BC90F__padding[20];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6__padding[2048];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D20648_t27EE9291CDFE60D18EDABFB517449291C19E5C5E 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20648_t27EE9291CDFE60D18EDABFB517449291C19E5C5E__padding[20648];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D216_t0D931079AC5D95785A4A8844BC246984FD2B6674 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D216_t0D931079AC5D95785A4A8844BC246984FD2B6674__padding[216];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D22_t5DD5387080650A0BE7578DFCAC7EFE4D422723C5 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D22_t5DD5387080650A0BE7578DFCAC7EFE4D422723C5__padding[22];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D236_tEF7D1050144F7923187B26AB548E1192336CA9FF 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D236_tEF7D1050144F7923187B26AB548E1192336CA9FF__padding[236];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D24_t9C623179D452F924B08B5EF245F32CC43EB892F0 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t9C623179D452F924B08B5EF245F32CC43EB892F0__padding[24];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4__padding[256];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D26_t1C03749A4A33D1E820B914DD0655B85875449DED 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D26_t1C03749A4A33D1E820B914DD0655B85875449DED__padding[26];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D28_t74A83EEED063F1FEFB3725CCE71DE1346BBAD6DB 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_t74A83EEED063F1FEFB3725CCE71DE1346BBAD6DB__padding[28];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D3_tB267326290FFDB80F17947B498E53DFBE2E545B7 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D3_tB267326290FFDB80F17947B498E53DFBE2E545B7__padding[3];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D30_t544F9EB116E4E221AE72CCFF94E585AA9AB5E478 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D30_t544F9EB116E4E221AE72CCFF94E585AA9AB5E478__padding[30];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE__padding[32];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D336_t4E7B2D63CE5081C13619A5BCFD777B3C8B051420 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D336_t4E7B2D63CE5081C13619A5BCFD777B3C8B051420__padding[336];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D36_tC76222D9D03BE44B28CD1F000F06D5DE87E7D592 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D36_tC76222D9D03BE44B28CD1F000F06D5DE87E7D592__padding[36];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D384_t7C09BC68D7D62B97E3F3F0D128792BAC28ABD995 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D384_t7C09BC68D7D62B97E3F3F0D128792BAC28ABD995__padding[384];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68__padding[40];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D404_tFECF215CCE6D0F9CCE83B0EDCD25F93B2CBD119C 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D404_tFECF215CCE6D0F9CCE83B0EDCD25F93B2CBD119C__padding[404];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D428_t52DA8CA48AEBECCA0646D0532DA8C778DCB09026 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D428_t52DA8CA48AEBECCA0646D0532DA8C778DCB09026__padding[428];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D44_t13DCADB8174913B4D9701699BF923C63704E55D9 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D44_t13DCADB8174913B4D9701699BF923C63704E55D9__padding[44];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D48_tEC15BE247DDDB470663E08241EFD84020150BDB7 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D48_tEC15BE247DDDB470663E08241EFD84020150BDB7__padding[48];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D480_t0FCDCCC5474AD52A475FC222DE9FDD13BDA5A2A1 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D480_t0FCDCCC5474AD52A475FC222DE9FDD13BDA5A2A1__padding[480];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D496_t81C0DB88BF5776CB0A9ED7C072B5B520B5E5A238 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D496_t81C0DB88BF5776CB0A9ED7C072B5B520B5E5A238__padding[496];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D511_t62A72AC54B1602A8784EF26AB5F3636C3A630BB4 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D511_t62A72AC54B1602A8784EF26AB5F3636C3A630BB4__padding[511];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D512_t49E2483AE6711806E3693BD916C7186613C3AC86 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D512_t49E2483AE6711806E3693BD916C7186613C3AC86__padding[512];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D5400_t464D2484AC9397E2B5527968C0CBC08F017C5899 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D5400_t464D2484AC9397E2B5527968C0CBC08F017C5899__padding[5400];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D544_t0D948C327964649D914DC7E252F628592C2CCAEC 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D544_t0D948C327964649D914DC7E252F628592C2CCAEC__padding[544];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D56_tB99B60BDC6F99AA8CB6DE18EE851EF73191EA65F 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D56_tB99B60BDC6F99AA8CB6DE18EE851EF73191EA65F__padding[56];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D576_tB40BAF500EB62A2E3F5ADB313458530228A9A2E3 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D576_tB40BAF500EB62A2E3F5ADB313458530228A9A2E3__padding[576];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D6_tD6D7047C0F2447CDA6838E2F4ED1BC10E2357532 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6_tD6D7047C0F2447CDA6838E2F4ED1BC10E2357532__padding[6];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D604_t372D4AAE874FE15EB9C24CB442EEA334EF7867B9 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D604_t372D4AAE874FE15EB9C24CB442EEA334EF7867B9__padding[604];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D6144_t2F27BB3780FC03FFDAE22412B5C3033D6BA3FEA9 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D6144_t2F27BB3780FC03FFDAE22412B5C3033D6BA3FEA9__padding[6144];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D632_tB5A159894E2C51EE5CA356469010B4BD9DEE9045 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D632_tB5A159894E2C51EE5CA356469010B4BD9DEE9045__padding[632];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4__padding[64];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D640_tF41F5D29544607E509B8C49645EB0B18D42D0586 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D640_tF41F5D29544607E509B8C49645EB0B18D42D0586__padding[640];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D68_t318EC812D5290F3EAE538193B28FD23FE95F673E 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D68_t318EC812D5290F3EAE538193B28FD23FE95F673E__padding[68];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D7040_t2134457E804FC7BC13527BC4CA58B8E829FB1CCD 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D7040_t2134457E804FC7BC13527BC4CA58B8E829FB1CCD__padding[7040];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D72_t6FF675DF5424D33F817FCFBA5F96A93536229EFD 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D72_t6FF675DF5424D33F817FCFBA5F96A93536229EFD__padding[72];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D740_t45F3298C3D4F0DA8314CFEEF5E6FF15D40EBBB9C 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D740_t45F3298C3D4F0DA8314CFEEF5E6FF15D40EBBB9C__padding[740];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D76_t2E1B361EC88495D058431EBCFF5B0B864454EB2D 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D76_t2E1B361EC88495D058431EBCFF5B0B864454EB2D__padding[76];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D764_tA01D1A13B714515B29B943B65CB9E2357C8BEB6A 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D764_tA01D1A13B714515B29B943B65CB9E2357C8BEB6A__padding[764];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D80_t61FC199E6DFD14137060F918DE5B788D5B66E75D 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D80_t61FC199E6DFD14137060F918DE5B788D5B66E75D__padding[80];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D92_tAA93827A0F3444A283A14C37D07A5776CB73B56D 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D92_tAA93827A0F3444A283A14C37D07A5776CB73B56D__padding[92];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D952_t087ACD64AE11F8E5E58A24915A94C445F11544D1 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D952_t087ACD64AE11F8E5E58A24915A94C445F11544D1__padding[952];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D96_tB67E0A3E5FE860E65CA2C893456B2783E43C6BF9 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D96_tB67E0A3E5FE860E65CA2C893456B2783E43C6BF9__padding[96];
	};
};
#pragma pack(pop, tp)
struct Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791  : public X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76
{
};
struct Asn1Sequence_t90471C8D847766B1B77A7AB098A9E8A7DF38A263  : public Asn1Object_t88A4CCA55F497755711F405EE399C0720E53976B
{
	Asn1EncodableU5BU5D_t9868017FA96E51C6C30CD73898AFD5C50CF466AF* ___elements;
};
struct Asn1TaggedObject_t4172254B2BA64C9B954F304147C2E057FCE20DD2  : public Asn1Object_t88A4CCA55F497755711F405EE399C0720E53976B
{
	int32_t ___m_explicitness;
	int32_t ___m_tagClass;
	int32_t ___m_tagNo;
	Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389* ___m_object;
};
struct DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7  : public Asn1Object_t88A4CCA55F497755711F405EE399C0720E53976B
{
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___m_contents;
	String_t* ___m_identifier;
};
struct Exception_t  : public RuntimeObject
{
	String_t* ____className;
	String_t* ____message;
	RuntimeObject* ____data;
	Exception_t* ____innerException;
	String_t* ____helpURL;
	RuntimeObject* ____stackTrace;
	String_t* ____stackTraceString;
	String_t* ____remoteStackTraceString;
	int32_t ____remoteStackIndex;
	RuntimeObject* ____dynamicMethods;
	int32_t ____HResult;
	String_t* ____source;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces;
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips;
	int32_t ___caught_in_unmanaged;
};
struct Exception_t_marshaled_pinvoke
{
	char* ____className;
	char* ____message;
	RuntimeObject* ____data;
	Exception_t_marshaled_pinvoke* ____innerException;
	char* ____helpURL;
	Il2CppIUnknown* ____stackTrace;
	char* ____stackTraceString;
	char* ____remoteStackTraceString;
	int32_t ____remoteStackIndex;
	Il2CppIUnknown* ____dynamicMethods;
	int32_t ____HResult;
	char* ____source;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces;
	Il2CppSafeArray* ___native_trace_ips;
	int32_t ___caught_in_unmanaged;
};
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className;
	Il2CppChar* ____message;
	RuntimeObject* ____data;
	Exception_t_marshaled_com* ____innerException;
	Il2CppChar* ____helpURL;
	Il2CppIUnknown* ____stackTrace;
	Il2CppChar* ____stackTraceString;
	Il2CppChar* ____remoteStackTraceString;
	int32_t ____remoteStackIndex;
	Il2CppIUnknown* ____dynamicMethods;
	int32_t ____HResult;
	Il2CppChar* ____source;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces;
	Il2CppSafeArray* ___native_trace_ips;
	int32_t ___caught_in_unmanaged;
};
struct FpCurve_t9C4D086FD235C2976984476721DF8DEAF8BA86A1  : public AbstractFpCurve_t6B02E9A3B6AC21D51607A74C4D3DF58EA16958C0
{
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___m_q;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___m_r;
	FpPoint_tE44568768E01C534DA64F9FE4CA0D52D97FD5588* ___m_infinity;
};
struct OrdinalIgnoreCaseComparer_t8BAE11990A4C855D3BCBBFB42F4EF8D45088FBB0  : public OrdinalComparer_tBB06915E213A5D4C8C617ED5478E8BF30C2B2170
{
};
struct DerSequence_tCC88E8724998083FAABF65D386A19D9C8C88C962  : public Asn1Sequence_t90471C8D847766B1B77A7AB098A9E8A7DF38A263
{
};
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};
struct ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
	String_t* ____paramName;
};
struct U3CPrivateImplementationDetailsU3E_t71B56FD9E998D4F52FFF04CC1A26784A143731EE_StaticFields
{
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___028827F0323F661DE5A79BB12878DE8AE5FED4D588B24EC29C384BF8747890D7;
	__StaticArrayInitTypeSizeU3D20_tBBC315B0EB56B25AE323C40681A72237C83BC90F ___02D050046D7A21C15C1A6D6755439B5EDAD147FABD9152C932359BA3A4E82039;
	__StaticArrayInitTypeSizeU3D128_t132F46F03AB9DDB64721E907BC73DEC9034183A1 ___04801C5DAE2B089F05A4334CFE18CCBD987A6EDE98113B9499E8D7054DC38252;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___04D9D643E5A26924DC98C9906714082D395DCB17CEE9674D5EBFEC15D098C2B5;
	int64_t ___054C37C1B2DD2633002F4E509CB0AA746718903189F9D7D92C6880B6A8EFFBB2;
	__StaticArrayInitTypeSizeU3D160_tD5E7DEAF0A9ACE47497C4D5203883BE2605035EC ___064CACC4978353DFD6979BF0975E06366B0F3E9C5631CFFF32EC2184BCF492E5;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___079628553AF0C7DA99DD2FAAD010CDDC162E2295DF3BB585A4B27B635AE9B81D;
	__StaticArrayInitTypeSizeU3D56_tB99B60BDC6F99AA8CB6DE18EE851EF73191EA65F ___07FA6E88C946B2528C09C16C2FB8E9CDA49AFFAFC601774C437FD9F2DF3ECE01;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___0828E49183AB306042032EA09C5BD83C67BC8CD26D7C08C504962517913861E2;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___083AE51829D2F17C30448BC49DF707978D0A45179340E9F72CE1F424996E5614;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___09D347461DA256C4CB0CA98D01A38ABF1FB444C058423E88FCCD72D6DA334473;
	__StaticArrayInitTypeSizeU3D72_t6FF675DF5424D33F817FCFBA5F96A93536229EFD ___0A4E21484641E9782ED1A1B3410F9F2DDC54F0F6D8807BF52C299807D9C4DF5C;
	__StaticArrayInitTypeSizeU3D56_tB99B60BDC6F99AA8CB6DE18EE851EF73191EA65F ___0AEC099C87E7062A57D1C79734EDDD28A07A1684746EE8640132BB252E6BF5A1;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___0AFCFE448EEBA81468EDE42DD0B9A0C17F1A006F43E0BE503CA27E8FBA2F05DC;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___0C9B53D0417CF5539EA98A771F400E930B4B4F55363A296A9E5A5F941AB71C86;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___0CF9F5E38BA692F10436AAC17AC6DCB62396F9AFC9CA013B3D640C1C95E31A39;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___0D34FD0D99957C53E4DC954DDAAE5934077F594E73C3FDA29F2FFF1B232D549A;
	__StaticArrayInitTypeSizeU3D604_t372D4AAE874FE15EB9C24CB442EEA334EF7867B9 ___0D5E04033CFDF0B51FE1A2D8683C03A198BF47BF9BC4FBC586C14349A912F3E8;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___0F10DA9FE40DFDDC86A1A973CACED2185D40F154E7C19B2E0E4A5A8C4DEF4309;
	__StaticArrayInitTypeSizeU3D128_t132F46F03AB9DDB64721E907BC73DEC9034183A1 ___10D88DF8B06E47788DBE42F876B642A928A3AD8EEFFE352C59F031CCFFECD49D;
	__StaticArrayInitTypeSizeU3D19_t72AF42BF3C6C3B98D8DEF0B56A8AB18A714BA712 ___111B15B20E0428A22EEAA1E54B0D3B008A7A3E79C8F7F4E783710F569E9CEF15;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___119DB25A3AE14BBB9C54EBC40D0E50D796D16A86C921C41BE8732E4DA2EBFD74;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___120918433B634757ADE589703127976B7C5C2009E8DB37632123F45FA2B87F2E;
	__StaticArrayInitTypeSizeU3D640_tF41F5D29544607E509B8C49645EB0B18D42D0586 ___125CF2084D7EEC18DC9795BE4BAA221655C0EABAB89E90A74FB0370378A60293;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___12D12138094C22913962019E8B805D08113F8CAF43AD1E7A3923B6A60EFF89A4;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___12F486CDF1F8927BF7289003288EFE6D3B6C674279639905EEEE24D0B6EA9688;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___1313B2EF301CE83F294FFD8C012777CAA44B3F5F35B6CC5232786F43AEE29084;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___1381231A2021F331F1B9B69D2F5FF04A2CC77582D08B909FFC0ACE9530351722;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___13F3BEDA950D7627802C21F7681A2553BBC40FDD07FCA3F52FAB662EE65B6F90;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___141785EC78A7C41133C5B71ACBF4AA8735D3C599E0EA82B1AFA2C71A46FAD265;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___14A03AA4516FE146356EF9D4F549B8662F9C50A467974D3BFAC45B474D65B864;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___14A5D850C255623F9472E3C650ABCE0C78D32F0276B315B3A276A0462D97A1AC;
	__StaticArrayInitTypeSizeU3D56_tB99B60BDC6F99AA8CB6DE18EE851EF73191EA65F ___16599A4F9A12ECCBFC361B547840304B9E48CBC431F387243C6859C475427D16;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___1674D0631A2E37B60EC748BC3899FEE1550C108AF30B85CE96A70C5CA4682D03;
	__StaticArrayInitTypeSizeU3D44_t13DCADB8174913B4D9701699BF923C63704E55D9 ___1691A129931FB25BD1A6B61962B1BC2DBF4A35C6D0F66CAFB00E7D325A043AFD;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___17282F193FB351ED64406CAA4B44C71BAD6D20F37B65E016952A8FAA536BE4DC;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___1A0C6D8588AD2BCC8FE37D32AAD70981DD42ECA259D570799BEB531448D25ECE;
	__StaticArrayInitTypeSizeU3D952_t087ACD64AE11F8E5E58A24915A94C445F11544D1 ___1A656D546FA3EC25711E9938E57456DC405FC94FC365095CBF0EE67D64F0E0EE;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___1B91FC35DB55926C21BF2CBCDA751C03BACECC7457426E69B87AA51A9EE325AA;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___1B993D34192A33EB10CEE61C1093A6FE7D3D4B41F751F550CF8515DC4A215207;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___1C200EEC7E0EAFD19F23190A1ED9B95D60EDE114EDE39BD917D1565839CB48DC;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___1C3EF802E30F355C14A57C8AFAF6BFC22F3E40631D30059D87C30496D784EC9B;
	__StaticArrayInitTypeSizeU3D11_t28A27E28E6854B15E7BDDCC03EBBABD075CCB33C ___1E02B1D50A1940A51671DC0BE3D58967EF531D51879B55F71FB1B70C5DE3C92F;
	__StaticArrayInitTypeSizeU3D80_t61FC199E6DFD14137060F918DE5B788D5B66E75D ___2066D0CEF59F04322EDE720147716EC917E294CEB01402069AAF60DA01783589;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___20EC21674E2CB9D3BA7B2F74AA5874381BAD58B7EDF526BDE000F175D1C5E5F1;
	int32_t ___21B0817029E06E75D6C5BE22BC57089FD89B468AA7C549604EB9483F978CAD5B;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___229C1C9CF2A07A92FC0153D9FF4A7F9E71E08017C3F4565346D2EEC7AFE91E04;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___22A2E4D6D7465CBCDC69A916FC99505E257B0638264ABACF711F45C7BFE1581B;
	__StaticArrayInitTypeSizeU3D216_t0D931079AC5D95785A4A8844BC246984FD2B6674 ___23336B7714FCB641343969478B5579B807F7D3D2D6CBB4CECC3F94DB45CF0901;
	__StaticArrayInitTypeSizeU3D128_t132F46F03AB9DDB64721E907BC73DEC9034183A1 ___2338F1330733829B7EA276F350452E34DC02E35AEA142145F1294F417A412FE5;
	__StaticArrayInitTypeSizeU3D14_t2779C6F98DE4C55CC13496418B79E30D26219928 ___2381073588085DEC186E7DFAE3F577D65E9C0A020A4B17488C127A60897C6642;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___23B18FFDE0FCEA2ADC19F8AD8C6083205C929D4F31D93A3B0EE9B929547EAFE2;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___23DB15B6CAC9F0BF9102DAD4E3569A3208841F38E80BB37F8A0D9D70B77779F6;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___241460699808ED5F8D48C6B09EED448CDAE597EBF52B010901CD6F75A767B047;
	int64_t ___24B7E3A490F64223F93EC177ED5A641984B68F0783A289AC1F2C94D1D92DA684;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___24CB9F17C8326D8BB8EC908716519DF7F265AE825F0DD13BB04E03A90B07D90E;
	__StaticArrayInitTypeSizeU3D22_t5DD5387080650A0BE7578DFCAC7EFE4D422723C5 ___251E3E8D6C8623BD8E24A20F50BCD78E044188FA6C113F018525B266C15001C6;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___252A0A8312BC1F5202CB599BCF172BEFA6F2A40195E72E2410AF5D81AC8B69D5;
	__StaticArrayInitTypeSizeU3D496_t81C0DB88BF5776CB0A9ED7C072B5B520B5E5A238 ___2541BC30AEEE47C1E2CD6ED62C5E387CB159C19E357E6E846EC8AABAD5AA8B65;
	__StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 ___265DD563A3E754D1DA09E056E784DF1550AD6EFA252EDC9A8B6F2F537C4D98A9;
	__StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 ___29E73BE4AE667DAA40ECB03060CB774323057723391BBD27872966CB3CA33D2F;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___2A1FE1596DB0F5D1893C1AF50592965E1C09B1EE1E35A8A87C181A9EB2F391F5;
	__StaticArrayInitTypeSizeU3D740_t45F3298C3D4F0DA8314CFEEF5E6FF15D40EBBB9C ___2AFF8D585A4398C45D3094B6F34B73275BE74BE2891925D12AD161D042432DC3;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___2B371DA34C2A942BAF37EBEF498C2EE84813B4AB0266066695C4152B6E3FF082;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___2B5147D6B22724C5885188AAFFD14CED290364902FB9ABC779ECA096280B9686;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___2B99ACA47CE0E46AFB9C405667C607042EAB8B57C39D9A3E7CA9EE030340770E;
	__StaticArrayInitTypeSizeU3D6_tD6D7047C0F2447CDA6838E2F4ED1BC10E2357532 ___2C6817528AB91D87A379BAA924F0BB642ED5C1AE839AEA5BBAA51F1E950796C0;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___2C9D6B105EC78E8E02279FB1266932E3468A575B0EB9818985DBE9468B991CCE;
	__StaticArrayInitTypeSizeU3D48_tEC15BE247DDDB470663E08241EFD84020150BDB7 ___2CB8BD3C4438DC6FC81B152F9EA8D692C730E3DE74336D1372586280090F854E;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___2DE8C8D0A909D9D152B7752E0C1650FB43AFEBF9F20DF52091B69C394BD9F0FB;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___2DEB754CF447D0DE35ACB7F4311F9AAB1D4CFC0638E101CCAA8A0A4948AB3600;
	__StaticArrayInitTypeSizeU3D68_t318EC812D5290F3EAE538193B28FD23FE95F673E ___2EE1A8E6F4E2FE0BB69872A9C314399B1B0FDEE75772244F204ABF4D6DEC2F97;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___2EF83B43314F8CD03190EEE30ECCF048DA37791237F27C62A579F23EACE9FD70;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___2F2C71B5B311BF139FD4B797EF2308F928EFE5092D7E11DC070CF41038A079B9;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___301841C47353B7791BE25BC1F60F93D0EB2090AB5714FA5CA3939E8DFF75D218;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___30ACEDF5B212D20C6ADFB5519C3B552044F577A06A9F6295CEE081B21681D12A;
	__StaticArrayInitTypeSizeU3D128_t132F46F03AB9DDB64721E907BC73DEC9034183A1 ___3106DB6422088B231F12C749BC9CC57B08BBDE65695641C166393B42EFF14871;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___31A50A6B8C571138DD6EA37B040389216B779C1ED712CB7980DD199E3BEBEFEC;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___325BFEE33A593E28E54E20F8520E5DFEE122F719EF08FFE096A2AD31C4DC316F;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___33E4F63DC027896AC744AF692480649D9B3E69DED2DF02A59F78B5D39CDDADE0;
	__StaticArrayInitTypeSizeU3D48_tEC15BE247DDDB470663E08241EFD84020150BDB7 ___343E404502DABEA303159A8C37BB2A4405B20A9B230D1C3BA9D496F05879019E;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___345B9681D9B723AA36E8AE15EC6088879B054DE3223F7F7E3B5559DD43587FC3;
	__StaticArrayInitTypeSizeU3D216_t0D931079AC5D95785A4A8844BC246984FD2B6674 ___34D542F5ED8A11EC9DE5E3882B3F28866A9377F5A748F5D649B6404BCF8CCDAC;
	__StaticArrayInitTypeSizeU3D764_tA01D1A13B714515B29B943B65CB9E2357C8BEB6A ___34DF24B8F3948791F3063D3DBE0C1F9B09A8639D784786223234BEC089E8B850;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___350061C79A080C4C6291FD755189D20222C9AE15CEC6BC6C794FA2E3409DEA6E;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___356608BFA95B39A5AA624D4852D37A19FCFB1302C897E1A4B7EA5EFD1A20120E;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___36003BEAAFC125A5D50EBAE8C98A6971BE6134DD89E286DD699E012687422A22;
	__StaticArrayInitTypeSizeU3D511_t62A72AC54B1602A8784EF26AB5F3636C3A630BB4 ___360294D82A77697405846183BBB96E548B586D0710C5E2322E2CEF23E88C4122;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___360E7DADB4427717AF4CB4E82F530229193E21B1BFB8DBEDBC46F77FF251CA15;
	__StaticArrayInitTypeSizeU3D1152_tC1CC4A1DD00CBFF8FFC237F6E7216D5AC09D320B ___36B8FDA0BFB1D93A07326EE7CAC8EB99FF1AF237D234FFA3210F64D3EB774C38;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___3936FEF22F74028ACFB35A545386FE1C4F4A2C8CE55E88B5DF4FF605D474D8B4;
	__StaticArrayInitTypeSizeU3D24_t9C623179D452F924B08B5EF245F32CC43EB892F0 ___394D174022AB9D27D329C8DE3DADB69831311870F91A3896EFFB406411F7472C;
	__StaticArrayInitTypeSizeU3D28_t74A83EEED063F1FEFB3725CCE71DE1346BBAD6DB ___3B520286F14349D61645FDE2088337584857AF213F1E9694D9DA4F5C489E6A0E;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___3B721FB05904288533A0C3F44162AA4631530BCFEBD22C3E57DBC4994E38F83D;
	__StaticArrayInitTypeSizeU3D512_t49E2483AE6711806E3693BD916C7186613C3AC86 ___3BC7BBAE9618E2066112B9F16B3CDBB856ADE0020C732ED2D56FD0D0E38E1FC3;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___3CA024F96B6FA0D283063E7C2B60A83C2B07F26AE1788CDFAEEB231C7725ED92;
	__StaticArrayInitTypeSizeU3D44_t13DCADB8174913B4D9701699BF923C63704E55D9 ___3D0EBB5DC33F10BDCF03701C753BECBEAA73D3F1D739FDA70A09F5D78947149A;
	__StaticArrayInitTypeSizeU3D512_t49E2483AE6711806E3693BD916C7186613C3AC86 ___3E4FB5FE52BF269D6EE955711016291D6D327A4AAC39B2464C53C6BD0D73242A;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___3EAA30E2BB2D4FF7221734E2CFBD2A94F0A3D8EB435634724DBF207AFE0DE24B;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___3EE5FB45A658F4B65CBB9E36D32A02C3E230F6DA546F5F3CC1203AE04E13A5FC;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___3F310649468E28525C82D862CB39DDBAE514C6C7491AF3813079ABFDC9078EA7;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___3F580014EE7F27F024FC40AE120394FAC1A8E4CD1B40FD0F0089E89856152138;
	__StaticArrayInitTypeSizeU3D44_t13DCADB8174913B4D9701699BF923C63704E55D9 ___405634CB2F08D8E4A65CF85762D7BE4356E6DE1C0C941DA950F3034F3F8C6ED0;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___40866F3E76BC07D89281D2593C42AD252FB2D84E0A91FA3C78CA3C1FCC17643D;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___40B7C5D256B01A9D3CAEEB482C3EFAC0DF7C77D82AC844FA48581AF28CDDEADF;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___427C844BAD080C6008BDA87BD6ACA4F6F342F6FCA9472A6D63C31B69F6F85054;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___42E57F5A26F7D53A24B69FF9F56FA7550A23B8BAB2F5993357E4C53EA06141E9;
	__StaticArrayInitTypeSizeU3D128_t132F46F03AB9DDB64721E907BC73DEC9034183A1 ___439231575950B2ADCDE8DC0E470192ADA8DDC05EB55836596636563B6403A6B5;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___43B9A42341477A51FB3329FA943298FB43910D3EDCAB69BFB04D0E68D84C0247;
	__StaticArrayInitTypeSizeU3D24_t9C623179D452F924B08B5EF245F32CC43EB892F0 ___43C8B8D4786B6331EFC85B21E235E3E39940C427680D2C6B73ABF19738EF5186;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___463BE4A1C16EB914C411F194E1F87BEF5A9081C49888860A682E8543D4B35F58;
	__StaticArrayInitTypeSizeU3D24_t9C623179D452F924B08B5EF245F32CC43EB892F0 ___4654FB5A717A0EB7C9078754B15F325C2F156519CFF821AAA495495741FF153A;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___46C4093363C4109023D6EA9DD8A19196F26A0181443826957232A505C5330410;
	__StaticArrayInitTypeSizeU3D44_t13DCADB8174913B4D9701699BF923C63704E55D9 ___46C4D06226E74471A7B52BA70A1533E5FB906912EA7D01D88D2142B3EB30B918;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___46D730A1677C2254C7FBB70CFD27BB5F231631EC4D295FD3211E051E4C7C34FD;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___4867A63842B7AFBF7A120C1310940F29048D0B73EF1747E15AD67A3AA0D9AF9D;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___490FE1E40F08589851422F9DAF5012280352D005653B5D2CE872268A1A743ACF;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___4993FAA5B9060FAAFC80E9F6CE26AF291965C0B1E8F5BBDC87C6CE9AEF6FFA24;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___49D826830B5D920448F74BBBEDB361F17C9DD461E0DC3DED114E6E13E0173981;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___49FC72E2BC139CDC89193D013C3CBAD8CB75E78CA254B8466A03D26581D08BE4;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___4A0ED776150EEA4229879A81FF299D350CBA701C65A83D9A5C9D9A685417E819;
	__StaticArrayInitTypeSizeU3D28_t74A83EEED063F1FEFB3725CCE71DE1346BBAD6DB ___4A469C2E6B7CE676E83B801DBCE6BB3D1E49DF84214DD1A394D4B2446B6D2779;
	__StaticArrayInitTypeSizeU3D6_tD6D7047C0F2447CDA6838E2F4ED1BC10E2357532 ___4A46F89E77BB78FA4C62BBA3B2F1EA2D55099E9ACCBC2AF76E05A4A07536FD67;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___4ADFA71AB9AF20C1C372C92FF8D26609A11DA05E167BBA62C0ABFD18F4C1EAF8;
	int64_t ___4BDBB22BAD3D42A49F9BA78CBBEB680CBD951AB9490A400EB7D3C02F2417AE6B;
	__StaticArrayInitTypeSizeU3D96_tB67E0A3E5FE860E65CA2C893456B2783E43C6BF9 ___4C2830C4E5B987FBFCC701F46320AC7D5B36188263033818292900BFB28BF7F9;
	__StaticArrayInitTypeSizeU3D24_t9C623179D452F924B08B5EF245F32CC43EB892F0 ___4D30B965ABF4D17A678F232D7C2491E37B61B203A6E807BB9E09C4BA2614F72D;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___4D487DD66D80886D292C3C7B64CC41FEBA1E313A26A9D05333B42E895741FF81;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___4D7CAD3DEBDBD349390304F3A59AD3F66B4A1FB02F88749C4E393FF3A1EDD411;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___4DDDF66BF035F6ECA9C5F21404F3E8DC72D452D7CC13051DAA425BC248F0F060;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___4F1B8E1CB953EA2DC9E5B3B1A88872BD0BF97BD1091B00456F43F1DFE4A0C976;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___4FDCD793D71BE141D8E85C48B61B68F00848ACEF69905D47D935786C44D07D92;
	__StaticArrayInitTypeSizeU3D20_tBBC315B0EB56B25AE323C40681A72237C83BC90F ___513BA924C2DC996CAF3DC690D42BEB815316271C0CDAC0812BC0F2C2ED45DEA4;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___5218271C2AB140CF1F8F18D070924F6EA8F08F5FB59C9E4F05AF36C0740D172A;
	__StaticArrayInitTypeSizeU3D48_tEC15BE247DDDB470663E08241EFD84020150BDB7 ___53487208337930F8682FC28413D71EFC4E4AF58BBCF31204D99CCB618594A403;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___53F2A64AF214B39B80717C930C811F14E55F7C3552BE64306D1355D13BE8E51A;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___542A4AD813E0CA853A400A4DC81182DB6803BEC6EA63ED0D54E0E90660DFDC2B;
	__StaticArrayInitTypeSizeU3D80_t61FC199E6DFD14137060F918DE5B788D5B66E75D ___550C3DFDE625907FEF7C0B4DB1A442ABDC106D409C562ACEEAFB8EA78B248C26;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___55D0BF716B334D123E0088CFB3F8E2FEA17AF5025BB527F95EEB09BA978EA329;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___55D90ED526CE6FF4309EE00834C4060B1A37CB6D5E65BAA01366C72FB2A68CB4;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___5686DFA3DBAE1C40758CFEA05B267DAC2FAF6CCDFADBC08A75D81088B777C751;
	__StaticArrayInitTypeSizeU3D96_tB67E0A3E5FE860E65CA2C893456B2783E43C6BF9 ___5708EA23084F9E6EB1F6F902A20183BD53AB053F59EE29EADC23AA440949833D;
	__StaticArrayInitTypeSizeU3D56_tB99B60BDC6F99AA8CB6DE18EE851EF73191EA65F ___5753A960C564DD99E5980C9A150CDDC39F8C36B65836D8D1AD0427F9B12480CE;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___58693A8E9871A51CE017A5CCB2219B320C7893E49E1761EF867DEB94E0466F4D;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___586CB2936F35105D4A71FF1186E81528F1980A2E5BB1E2719E0D86809CB1080A;
	__StaticArrayInitTypeSizeU3D80_t61FC199E6DFD14137060F918DE5B788D5B66E75D ___5905613E02E9ECB38A8A0FE322F32222D1A0E667D6B01BFAB40276929F0C618B;
	__StaticArrayInitTypeSizeU3D120_tAD8E3F872D03771C28D56F19BA94D0F528874DAE ___5961BF1FCF83803CE7775E15E9DB8D21AF741539B85CCFDD643F9E22CC7820D6;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___5A33FCFEC23C49D91BCF58CE2472DC9F3662CD086BD29FC44AF2E14567238A30;
	__StaticArrayInitTypeSizeU3D128_t132F46F03AB9DDB64721E907BC73DEC9034183A1 ___5A6EA34087D3D9F8963F478DEE9F2514C7ADF82018B6F95E5F0C9F35CA631AC0;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___5B4D9F89BCDD7A852E99D50089D7945FB13B705498B5DDCB4536512BB5484545;
	int64_t ___5B4DD911A09A289FAC83B4228401DCF4A4E7B9C16183EE897E78C96EAD025F9A;
	__StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 ___5BBB9265FCB4E834C68440328645B1A187F68D3275881FC46B21564B11326DA5;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___5D2BCB56850200B2EDECD280A83B6F489E2DE5771E609FABA8A5C4816206CB7A;
	__StaticArrayInitTypeSizeU3D384_t7C09BC68D7D62B97E3F3F0D128792BAC28ABD995 ___5D34088B4ABB1F3FE88DCF84DD5C145EFD5EA01DF1B05BB8FEAD12305B0979B7;
	__StaticArrayInitTypeSizeU3D124_t1CA7986512862C38F256E518A9043A19DAEABA20 ___5D6878AD6E68B2CCB04A7CD7942BE07C15F947CCA8824203021DD465D90712AD;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___5DF5DA3512C124C2BB3C3676FFEDA4E6CC5E902C2A6027D37EB4B5AB64ED4382;
	__StaticArrayInitTypeSizeU3D28_t74A83EEED063F1FEFB3725CCE71DE1346BBAD6DB ___5F2BD6104C620DE24A78538B659D1218F81A8DC50ED6645B5A040194E0CDC663;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___5FEFDE16F7F9C895FA38A2BEF3C77234B81B8CE82C1D3FD80F68907B2B951881;
	__StaticArrayInitTypeSizeU3D124_t1CA7986512862C38F256E518A9043A19DAEABA20 ___61358F81002F15B87F2746D4CD7FE28FD2CB45B8F0840B807B18C5A23F791CB1;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___6147FA0BC74757B54970DCDC5FEF39220A9176B9E30B9430F77772400CAF5E4A;
	__StaticArrayInitTypeSizeU3D48_tEC15BE247DDDB470663E08241EFD84020150BDB7 ___6164463B7C304A53288FDCF4DD2136E0E9A441D9930215FB691E3D2F692C3993;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___624B1BC8BE6B6F8CF39572731AD0E5BDF2AAB137D0AB4A0F473B3D00666D766B;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___62A191EC74B0F281659DD81132751B7656F065A2AD5A8E892CAE1A8E4DBFD0B5;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___62B0B99A90CF348B296641E627D3C0278DBD27D2F6FD5C66AA86BCA7BC32014E;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___631F2414F101F7C638D597F0D3BB96109426BB4F71374A956890535A6D7C2539;
	__StaticArrayInitTypeSizeU3D44_t13DCADB8174913B4D9701699BF923C63704E55D9 ___6532F51A82E7FD04EE2E8DCB8A6CA9D49B2AFC893C7B06813C9E7C4000FE85AF;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___663A2D6435FFB6C16D9382D8E22490E109514E59C03574E825F6B491AD8C8AAC;
	__StaticArrayInitTypeSizeU3D576_tB40BAF500EB62A2E3F5ADB313458530228A9A2E3 ___669C05E216A0B007F8BCC449F3B23766D63E5E1FC36219BC54378367849D8542;
	__StaticArrayInitTypeSizeU3D48_tEC15BE247DDDB470663E08241EFD84020150BDB7 ___6708B572BDBE5D5E79701DBB9744AF74B50FED7608218F2D7BF1B5D87E5A53ED;
	__StaticArrayInitTypeSizeU3D56_tB99B60BDC6F99AA8CB6DE18EE851EF73191EA65F ___675D55EB421ADAA655D122BEBC8F20ABC3E69A51DA5C6EE0FE699F356AEE7716;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___680EE365B35D1B3203A4ED488C96F08FD74E05062875A2D9B679D09C438AE9F8;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___684F74091FEFCDCDE9B282E317592631CA48B75F46340A11B95A474700E656D4;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___68A0E20D3F6DC9784851594347038C26D06E3DA1E6CD8A063384A422487CFE71;
	__StaticArrayInitTypeSizeU3D132_tFCBB1508E3F4AC3E15548F957F3C60AFADE17D8B ___68FD26731E838DCF7AD1E0A84240203E86B68360C20D8CB5C54D9E1EDC4235A1;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___6965AC9D48CA477E9A17658D33A217051648138AD7024514DE014D8001B533C3;
	__StaticArrayInitTypeSizeU3D17_t23DF19AEF84B77E016C2F3523830A08A46E6C217 ___69CBA35B6DD0AE098FC528BE4BD3E2C3C29A29C78129A11A716C8AFAF468B636;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___6A4F0EAFDA4E9404B327852ED3CB66A355F1424E5E13BE77300DC6D4621CAE06;
	__StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 ___6A5953C1A0D67EA77DC8A5D34D0055B3D6A7A3C4CA3F6FE599471C40B9B017FC;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___6A5EAF956411330E0B28CAD097D9698CBF4098D268AD1C0E04FE294F0607E53D;
	__StaticArrayInitTypeSizeU3D24_t9C623179D452F924B08B5EF245F32CC43EB892F0 ___6C99A25F0D00AE5BC45EE7F62CFC963F5DFC8FE4F7CE82ACEB0B5A1CDB51E3AB;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___6CB004567F6DA8572C921962EDECA8ABB063258E4B378E85E52017698A655056;
	__StaticArrayInitTypeSizeU3D3_tB267326290FFDB80F17947B498E53DFBE2E545B7 ___6CC6036CFAFD30F58F0D4CAF3BFEE97303CB9F3241AA9C3493367C2845BAC5C2;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___6D4410134564FFF8B73800035CA093523750DF325139D004701C67D97488B8D5;
	__StaticArrayInitTypeSizeU3D192_t619ED94196E112EB663AFC2D8F81A9DEA2E2450D ___6D736CA2760B98500A289548A45768A0347EDD83FFAF27A406ADE7F0DF13A883;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___6EA207010DDCDE29EEEDCF8B07C2FACD1DAC61F67DC9A48574DA28A93BF06956;
	__StaticArrayInitTypeSizeU3D28_t74A83EEED063F1FEFB3725CCE71DE1346BBAD6DB ___6F055E07140AF36F1C17E348B39AB7ACB0647B788E869F4236E285C89C2C89EF;
	__StaticArrayInitTypeSizeU3D428_t52DA8CA48AEBECCA0646D0532DA8C778DCB09026 ___6F3DEC14A777BFB31AA272349BEBD2C61CDF9EA31B79B5F6BDE4C8A0E5015DD3;
	__StaticArrayInitTypeSizeU3D632_tB5A159894E2C51EE5CA356469010B4BD9DEE9045 ___6F75933D3AC11E0F251AF6310500E30D147CF7CB8911C64CF8F401061EAAB88F;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___7017794F4FA285E1AA4C6FF9994A3DC0EDC472CB13D08BCA7B5CC287C51E0E3E;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___712618256BB5287925AAF70FA8EA2EBFFB72DF72B5D29A339CC9B8DB464EFC14;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___715F5EB09E6B0F8E87AD50BDAB729CF129E723B95990CE21DF233461DC29304E;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___725EE01C3EFD1FB0B1A3FE231F7E34D183EA74EAC657F5A4AB46E4996A7C343D;
	int32_t ___72B8B8D3A6D4A8C2F9364B2F944B36201C7D8A2B43DBF558D1A09883E969D74D;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___7409A8840065C2B1ABBC5ED7DCF94D2FAA1E32ECC5F39CBA5CA4D9F316E63CF6;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___74EF7306E7452D6859B6463CE496B8DF30925F69E1B2969E1F3F34BBC9C6AF04;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___7543B37FA53FDE2C84F07FD39F368555966AA1C0EB2F2FD26B294D79966E290E;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___75556680D9C8AC7D087BBE6B392CAEB69975DF125DDCDB353F3628A44A492813;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___76387D5ADFE12720EE7A21E3C30296DE8356BE159BD422D9900C8146F914CADE;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___7650DB5F7D0AD39E75A5BB512A98F9763E302328E120F06230159AC1AD1C6B5D;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___7655B973909B653A7FEE7F013F9EB663E580BB8C631AA14647A1AC28721B597D;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___778341AC4BAC1ECD286651C78ABAC8A7D09F9AC3B429ADBA58427C079C0F4DFB;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___7854BA783A61F0B4D74198AF22A2D285EF70C526BDE4BE78FEDF180436FB621D;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___78A70FA0E21C85F13BDFA3D7B70E095EC5B61E51F34D2C1D531142B0634B30E5;
	__StaticArrayInitTypeSizeU3D512_t49E2483AE6711806E3693BD916C7186613C3AC86 ___7919EF601386C08FC5EFB981B4A1E478D8413596173FC159B15739E87EE1BA50;
	__StaticArrayInitTypeSizeU3D68_t318EC812D5290F3EAE538193B28FD23FE95F673E ___79478AF14CB42EC0951669D5D1D864E43C211CBA631D8FFE7F6CD79FF60756C6;
	__StaticArrayInitTypeSizeU3D5400_t464D2484AC9397E2B5527968C0CBC08F017C5899 ___798A1121343367EBCFA95EB7F3DCB7D9BFBBDE36C5BE25D2B4E5BC5C4491D96A;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___7A36979350F6A229EB7206BEE580F51563EE52B71CD2132680B0AC33B3C31E27;
	__StaticArrayInitTypeSizeU3D576_tB40BAF500EB62A2E3F5ADB313458530228A9A2E3 ___7A7E2DA28EA306D72C66AF6CC2D3929CEFC609108B484742A63328A57DCB4B64;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___7B7F805F60EDC89C1F0B12BA905886CF68CB380E7489CADB248CF872FDFF8E85;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___7BDD3F08BE488F1D6DCBF05D01C69BA80DF49A9E9E415F0205D575B4B5F32A34;
	__StaticArrayInitTypeSizeU3D480_t0FCDCCC5474AD52A475FC222DE9FDD13BDA5A2A1 ___7C863A911E2AA252865A3EE4E316B05DC2041DF571B44D33DEEFAF79DE33E285;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___7C8975E1E60A5C8337F28EDF8C33C3B180360B7279644A9BC1AF3C51E6220BF5;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___7C971D039A1594DF7A9AE5ED1FCB03C5FB3CA960AEEE194470EDF9851CBE1A29;
	__StaticArrayInitTypeSizeU3D30_t544F9EB116E4E221AE72CCFF94E585AA9AB5E478 ___7C97C06F982D2F598F71BCF85A27244685EC039BC414EB391EC3EC449A619F37;
	__StaticArrayInitTypeSizeU3D116_tAFCAA0A6BBF3C7D612817453792131CAF1600CD7 ___7DDF88204E7E265240211841F0AB290A5E77EE4F9223EB2E39F9B89C30C41B9D;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___7E17A101F8DB2C2B3993E902D0354CB51FBB97DE38EC1FCF7E2D160B6FCDD59E;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___7F034CF9D8AC0293D0C9535AF26B5452C6E916CE62FBBF765077BD958E69B7B9;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___7F9D26619AB711CAA60DD2DBF8EED9D0F09E1707DD84FB7EAD266333E2B3CBB2;
	__StaticArrayInitTypeSizeU3D56_tB99B60BDC6F99AA8CB6DE18EE851EF73191EA65F ___7FA63BA1E20A9C8950945582B3A4C86272FCCE4341D0D29F1F68E75DB58AFB65;
	int32_t ___7FBDBBC598B2DE18BB5191FF9D66727F552A54412C9314CA5782824A150B3CD3;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___7FBE5B0653B594E77F474AD2E3F138CE810CAE9742BF46933C5ED613BA2BE32D;
	int64_t ___800BC83FA3E127EC8C14A6282B05D6F224B8A8E2B2D3DAB222E5BADAB4A31ACB;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___8025413D6A2DAB10571710EA544DCE79548C2874A83969663809CB859FDAC4A3;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___8051E141AAFE935BB42C7E96A9A525BAAC26F6867F1F455120DCC6E165AF17B1;
	__StaticArrayInitTypeSizeU3D20_tBBC315B0EB56B25AE323C40681A72237C83BC90F ___81C0F0D213E5735AED08C328CC9CC2E61B3AB83C9BF016A666B4144F8BF64457;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___83E8FA01EECEC207D12DA40AEFC466B15F6412FFE2C30DBEF0C65B9B7FF01672;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___84A542FC2A9940F8943D26023F80BCD27268FD77F7524DAD9AFB3CC3985E63FD;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___85332E00AFFFDBE44E7421AA4236274F9495AC6C2204C59B9ADFC872D4215E12;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___8546F1BCB5C4931501D810E34637ECD4C72DD549D16C439CE1B3C769A181A9A9;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___869512F4925E92315DFF511768B46527A5E189549284F0CE5DCEC5CD221385D3;
	__StaticArrayInitTypeSizeU3D48_tEC15BE247DDDB470663E08241EFD84020150BDB7 ___86ECB4ABD964707BAF5ABF589057FBB2ED63DB1D7F0115A7DA45921043B6A96F;
	int64_t ___871FBCF7AE8482A54E5A77A11F1E98A8DFCD9F4DADAD2167EB2C544AAC77382C;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___874805F2B76BD7887959F02FE0B2A8C07C1E1D41F41AAD4D2AA1D0AB21A1780F;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___8887A8C13E8406291F39913650F596AB592D817C993486EC86BCB26ED372004B;
	__StaticArrayInitTypeSizeU3D128_t132F46F03AB9DDB64721E907BC73DEC9034183A1 ___896FB86C1BE3DBE6C3CC4FBE122562D75A4EBC6616D3393CF1AFE12377908B95;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___89A391869039B1E1A150127E5E59949489BC50A03C37FCE49195F1B2B30CAF2E;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___89B6123A49A2BB61D4632A495C947A06422B652398C70C55E183B7971297ED00;
	__StaticArrayInitTypeSizeU3D6144_t2F27BB3780FC03FFDAE22412B5C3033D6BA3FEA9 ___8AE83CF30C3CEAC5F4B9F025200D65EFAEC851DE0098817DB69F0E547407C095;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___8B3E4AD98E68D077C6A580E0CE5B8BEA476668CC2303A9845D8EC347A4035FB8;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___8CB850AC259DD215242199AD8F1B0BC74C182B564124B84C0D56BDB7A8FF7286;
	__StaticArrayInitTypeSizeU3D124_t1CA7986512862C38F256E518A9043A19DAEABA20 ___8CC23383E91ECA5D902FB66829223822A4FAF6D15ABA095128864C0C26D2B690;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___8CC38DAE7E1F854FB74E0A2910E4781DABC17BFB28C0E2759599F647024FB680;
	int32_t ___8DE58994BA1BCCA412DBAD0B7F71CC1C77CFA1303F3224FA5371B5BE8A27AB5E;
	__StaticArrayInitTypeSizeU3D56_tB99B60BDC6F99AA8CB6DE18EE851EF73191EA65F ___8DECDB10AEFDB828B1325DD76119F2587EA785882269C22B7818DF39260394A9;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___8E63F8C394A1EE38340D3BE6E9A33B7B8C86D752498720DC80C223B02562E959;
	__StaticArrayInitTypeSizeU3D76_t2E1B361EC88495D058431EBCFF5B0B864454EB2D ___8FC498A953A183E1FE81A183AE59047435BB9B33D657C625FAB03D38BE19F92E;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___909B6F9B70C3E395F432CF36BB31095A710729723A9170EEB06F427A6CF171FA;
	__StaticArrayInitTypeSizeU3D112_t294E07C274A143443438C5FFF354DC21C14DC7D6 ___9185B9A22F0EC9A6E01EC78D9395C1D68BAD29F6501229B101F4E789225714B2;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___93631B0726F6FE6629DAA743EE51B49F4477ED07391B68EEEA0672A4A90018AA;
	__StaticArrayInitTypeSizeU3D120_tAD8E3F872D03771C28D56F19BA94D0F528874DAE ___9476220840D3CE82203B4A722E278773B1DA458A22F49FCB9FC45B851DF7D503;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___960F02F165C0EF9AC4C160FC98BE9273C245853BA32663BB1B6F1588077F95F4;
	__StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 ___96883F56416393F42C830BCD6033BE079690C482BC73119EBBE5DC98834BACBF;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___971488BE36A194DB27CE385237DEA12A31333F2FBD2BB3B60701BE542B27AAE2;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___97467041703B2DF5571DCACC6A702C0C6DE68CE9A9470AA240F57D69C09AF7D3;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___97C49ABA628782D4FE08840F38539E305C52D0A9C0E0882E77C23DECAB6BDBA6;
	__StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 ___97CAAC7D5BE7722352C1AB556BFE01D3F3CCA0F50B5A648195D6415006620C8F;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___9829726FEA4FBE8836ADA2143950510D3C214E70A13B9BF48009D97CD8485670;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___9931CA75F72BEF60A9E47A9BA988E4DF8B462EBBECCE1B321AB2EBDA269600F1;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___99E8C63009F88B142C11AB5F933B3773F78A2B27E15AD734EE5922DF2681C193;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___99FD541BFD4CF8DD3C18069CA1D5F4B622333DDBA412F00D5A6B642626034EDF;
	__StaticArrayInitTypeSizeU3D512_t49E2483AE6711806E3693BD916C7186613C3AC86 ___9AC0C32A73444A448170AE1EFE7F69A0D3A7F6E1335FA0E112D9F96EB7EE3CC7;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___9ADB15F070E9316F84A156DD2B7F2EB5E3881C621225107D0AB86E6BB18F8B59;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___9B0C8794D79FF87DDF1D5772BDBAEEB3D23DD8872279E82F0807E49724F2411A;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___9B29692916637801E748ABDF9CE9D3A8AED3F375C84C7346C11C2BB150BD720B;
	__StaticArrayInitTypeSizeU3D112_t294E07C274A143443438C5FFF354DC21C14DC7D6 ___9B673D6CD7A141CC75103328B1985B892CFF9C548D9C0F9A7964DD4C24D9095F;
	__StaticArrayInitTypeSizeU3D384_t7C09BC68D7D62B97E3F3F0D128792BAC28ABD995 ___9B803B35415DA0CD08CBEDF0C0E261F57E1266F0BA53C39D2208ADD2F38F5C85;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___9C28EF1A620E00413BBFC7E6ED40092033A3C250583DD7FAD355D2A306A7427B;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___9CB8BF99E695FF4E0E3E01770F3E96060FC79466F1325C60B375349C591ADF02;
	__StaticArrayInitTypeSizeU3D48_tEC15BE247DDDB470663E08241EFD84020150BDB7 ___9CCC11CED0AD12118416B3C3C73453E6480B671C0EF2F753B6900DA2060F5B44;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___9CFF23020B5A7E24467474237978D4CB52A601FBC0B3E504B9ABC64C062A499C;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___9DA5D1DADE776A467199AC96E9412AD7A5CA1E62BCDAC527079912B1185A5C63;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___9DA6DC3BEB35D0E411A5D1E407E011542CC032FD6BD76CFFEB5EF327331840FD;
	__StaticArrayInitTypeSizeU3D28_t74A83EEED063F1FEFB3725CCE71DE1346BBAD6DB ___9DC840E2F2B5B0E9B3B12C6585295C61986721911DA7AE18C9B18EFF2F01ABE5;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___9DDE3528C5CCCCD4D7C730403ABE54766C6645E371ED7ACB70584C320D87F670;
	__StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 ___9E313C7C2FE454E0FA78D32FCD05252CAD455632D79D6E730A5340196B6DEF5B;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___9EF35AA995725D55C515A807B1D2BE31B36269235146245330CFB2683FA9A840;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___9F9F5111F7B27A781F1F1DDDE5EBC2DD2B796BFC7365C9C28B548E564176929F;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___A08110AAECF4B88CA5A7F070B68E448ED895A5DE752B84C57651DA54D9BB5814;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___A0FF4E2F754A4E0A93B6CE53B7DB6E79A2377CBF601C9C92DD665B999F41F9E3;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___A1486177999A77F2FD76EAB02D2517803BFF5B0C5A01CFB95B3F1DA7B7D6DCE8;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___A1E99CA3768370397D301A3A1BABB70185BDD5979C7F92714B1CB5155E17BE17;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___A39EF033189E647DDABF6324B79FCD143D3FD7DFF7B247086B02EC33B9E3B15E;
	__StaticArrayInitTypeSizeU3D6_tD6D7047C0F2447CDA6838E2F4ED1BC10E2357532 ___A4083C0BA9FBB6424F75B75603FBC0855756A32C2F4497CE3D79702E675882A9;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___A4241EDCB474331DF94C96B4F24A3A8773452488FD0458F8C0C30F7C24B89E07;
	__StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 ___A42BA64DBDA4B838787133F8D5ECDDE0140E190925854CB602744F71B7E66192;
	__StaticArrayInitTypeSizeU3D80_t61FC199E6DFD14137060F918DE5B788D5B66E75D ___A504C689CF9091DF80B5A04BEEE2BE97822ED72E26518D7F3377E2D7AC6D522A;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___A54D748F60CD079179EA45855D5F795718EAAAE22D5D425B28F0AE1D160E4DF2;
	__StaticArrayInitTypeSizeU3D20648_t27EE9291CDFE60D18EDABFB517449291C19E5C5E ___A5730AE151442481A128E130F58AC702D6F79CD4BCA117B63A2B67199321B512;
	__StaticArrayInitTypeSizeU3D128_t132F46F03AB9DDB64721E907BC73DEC9034183A1 ___A589B8F1C215FA773BA2724EB825E8B2D1792FB955349574454E18C63537F2A6;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___A5A95A17B27BD47489B7BEB4A8AA145D08C838C183CD5A77A1A5EFD96AA3AB99;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___A719D1E9DB9C1628AF05DD49B4AA5D095CF0AD06D01C396B1777588A04D9D13E;
	int64_t ___A78AD77053B560CC929B80C90EC9451B333E9BA52FB3231EFA679745FFB59B0E;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___A80010A555E14F5B5B3D967E0F1621742AC2048F40EBF73EB76AD8991448EE21;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___A935391692FCA9A4CF6CE823B9DBED3DB708352BB4ACEDDA9EADCA78ADD23DB7;
	int32_t ___A9DD1527A5845A7B2E82F3427251AD622D570D0B8CDA4A0AF9644EB34E69B2FC;
	__StaticArrayInitTypeSizeU3D56_tB99B60BDC6F99AA8CB6DE18EE851EF73191EA65F ___AB3F6EE9927AC1C89E7F3D250F8DAC9A070249930D4A758B98F4D5B9A8E202B3;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___AB60EEBC1B8BF85AD5AF219745EEA4317E0C780D22675F7A33A0929A34D17DE5;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___AC7FC16C704375F0F1DB433E70C6C2B90B1D797B17806DD74E7664CAD8BFA778;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___AD220A499D7A196B0FE1E8BCDAA020369A8BA1C4CF21B15A3E21935ED7B09D54;
	__StaticArrayInitTypeSizeU3D24_t9C623179D452F924B08B5EF245F32CC43EB892F0 ___ADA8FCF87AF641D309489525EC896153F79D2AFEA9622C0BDCEDB7F8EAADD77F;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___AF0312129020B258B5B582D3F8EA8A3DAB35467AE93C8C6DBE5D6681CCD7D489;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___B0844D5980DE65F1FA3BBB87F49149DEC97F3C707AD42CEF201F41B6576233C2;
	__StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 ___B09C22565E577648AD57A0C5D1F44C38D4C01E2BF7727269A1E20A3717CC77D0;
	__StaticArrayInitTypeSizeU3D1032_t7114CFA064AF3D04F8E9AF9A3F185F34AB8419BA ___B176841533C118C3FEC94BA112AABF0AFFE5D2A7A389E913F2DFE68B266AE110;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___B17AF625BB8EA7C85766DBFB60A73BAE2A82560C68B4F03D08B576CD43E911DA;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___B1FFCD775449513A059F2F3A2DF44C39E6542C6474D6BAE3DC3FA1BC256255F0;
	int32_t ___B23838058CC37752D4144991C3B57839AB101CBB25919307DA2C4238A656286F;
	__StaticArrayInitTypeSizeU3D120_tAD8E3F872D03771C28D56F19BA94D0F528874DAE ___B23D510F520CB4BA8AFA847F8A40E757C40CB6A55B237EFA1AC6D3984911B114;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___B2A10F44FFEE5755DDFA6587F62D7F83F24AF24429D3ACA46E56F3F295251DEB;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___B3985439AEF8A4DF51A3369D7C0211EEF12CB0E510F6630E110C6DCC10B11A8E;
	__StaticArrayInitTypeSizeU3D11_t28A27E28E6854B15E7BDDCC03EBBABD075CCB33C ___B3C6C9A7B91F13134F7F6E0078C416CF1DE1E2CA7ED333F41696359779D6F3C8;
	__StaticArrayInitTypeSizeU3D48_tEC15BE247DDDB470663E08241EFD84020150BDB7 ___B4058E7F52AD2232429965C913DCD28A06AC0A2440960C66FE897CA0A2D64C8A;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___B48C3FCBDCCF3473EF494DC9D3E37B24CF9202AB511EEEB8FCBB48B626340059;
	__StaticArrayInitTypeSizeU3D336_t4E7B2D63CE5081C13619A5BCFD777B3C8B051420 ___B5B337AAF4AB1DA76C5AC52B6B47409154E519A0E609D31832654D4AD22E8D0F;
	__StaticArrayInitTypeSizeU3D160_tD5E7DEAF0A9ACE47497C4D5203883BE2605035EC ___B62054CCBB9806DA5278E1490083AE2AADB9FFD737147F5AA4A20D05AFCF9D1D;
	__StaticArrayInitTypeSizeU3D20_tBBC315B0EB56B25AE323C40681A72237C83BC90F ___B6338EE68F3FD438E03D596B6D76554714958953358DF7320F5073C3CB779758;
	__StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 ___B64D2D075DFBB6FBC13573A6EB98DB78FCD395B6E99E596048B56067DF90B104;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___B67F32BB12BAA41BEAF505F53EECD4F1409EF3E782F8CD077B511553999CE297;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___B68ACFAD2A3D93FE2B8963E29978E6D3FADFFF5E7FE6C13590E8888CCA7AD7DD;
	__StaticArrayInitTypeSizeU3D36_tC76222D9D03BE44B28CD1F000F06D5DE87E7D592 ___B698262446F5CFFF6CD492B8DBB1298BC61F0135EEE69366BD94E81ADE1D5C5A;
	__StaticArrayInitTypeSizeU3D36_tC76222D9D03BE44B28CD1F000F06D5DE87E7D592 ___B6FF5DB8C0FA699CD97E8EDF68219F0D1D9E509B4911710791D74AD7DF2DA1AD;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___B77E3D4019880A0C0F03E4214418983FD00C0107BB2632899FE6C3230F97B82C;
	int32_t ___B8A899AEBE8BCC739D363C17DD8CA885653263A59843BF7B7C85DE4DB10A9C92;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___B93B37392B6BF36325C2B8E6047E7EF31B87C67E59B16D30A60CAF2E18D9C73C;
	__StaticArrayInitTypeSizeU3D96_tB67E0A3E5FE860E65CA2C893456B2783E43C6BF9 ___B9D06DBC3EF70A5469EAC655358FDF24EA2B0D7B00CFAE2FAEB46E0435A75E75;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___B9D4AF390AFC6A0F149B843D651CFEBC1C4EC496A0263B72207836F9C525E1C4;
	__StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 ___BABB01CD1E2AD140F7D2AF35515DA38E8936F0BA0298BCF0245EB5692A3F69CC;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___BAD4C223EB92A3569CC2E4434A67D6A415A267524F88731BBB8424C305DC26C5;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___BB288FA00D4BBE2546E0D29F7D8BC6AF483CD4275D692B451B2E39A404E33EBF;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___BB3F32E62396D8E883006CEA60F19B92795D875F6854D7A98D1175E51B462345;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___BD01644841E6B289961C01E9D8319807BB25F791DB6A990F60D44DA36533D5AD;
	__StaticArrayInitTypeSizeU3D96_tB67E0A3E5FE860E65CA2C893456B2783E43C6BF9 ___BD0803FFBB094DC636588CB801AA84B33963BF6A1DE5BEEEAB20613CC326ABB4;
	__StaticArrayInitTypeSizeU3D48_tEC15BE247DDDB470663E08241EFD84020150BDB7 ___BD5EBD216C774AEB9E80C14603F773C6B840945F517694995DC5CB00BB7DBD02;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___BD6174A7C16E1AF07272966A9C071C8A3CF16B2726C50D672A09F60636B08901;
	__StaticArrayInitTypeSizeU3D56_tB99B60BDC6F99AA8CB6DE18EE851EF73191EA65F ___BD7BD1177FB5EC6CFD98E00E763A9AC1AA45B645130F051074D265485CDD8745;
	int32_t ___BDB11C99D6418180156EEA47B9EB755F8736055603C19E57F9DFB4CDFFCF8C3A;
	__StaticArrayInitTypeSizeU3D96_tB67E0A3E5FE860E65CA2C893456B2783E43C6BF9 ___BDB6BB299E50984399E0FD8F6BA60FC85EF8CAF6AD5154D53E6DAC34E2E0B381;
	__StaticArrayInitTypeSizeU3D56_tB99B60BDC6F99AA8CB6DE18EE851EF73191EA65F ___BDFF3716BE6B0C2920220FE8812E0EC9C0A9E716E1AB32606DCE45B69A1E91B4;
	__StaticArrayInitTypeSizeU3D11_t28A27E28E6854B15E7BDDCC03EBBABD075CCB33C ___BE24C09865A93221DF3247D5C3D727BCFDAB57D6B41C510D3AA53F0DE08E4C2C;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___BEDA989D5939125AA1831BFE35ED2E4F0F05DD63851049C646ACE173A2D0827C;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___BF3DB37CB55FF90EEE410403506E7AFB17B3B6ED163AEC657F4E4F47532D1E85;
	__StaticArrayInitTypeSizeU3D80_t61FC199E6DFD14137060F918DE5B788D5B66E75D ___BF62C56CED2420B3754F87B461864E919485B10766B29BE8DDC0C0C510745182;
	__StaticArrayInitTypeSizeU3D544_t0D948C327964649D914DC7E252F628592C2CCAEC ___C016C4D2C812AC5CA053CDEC62FB3011C7C881655DAF70265ABE7FA089B9119D;
	__StaticArrayInitTypeSizeU3D192_t619ED94196E112EB663AFC2D8F81A9DEA2E2450D ___C0BCAD52BB24771C2430DFC75321628F5503FDDC5A0E05E93F3DD624A60B10BD;
	__StaticArrayInitTypeSizeU3D20_tBBC315B0EB56B25AE323C40681A72237C83BC90F ___C0EE00302C997F25F7963B42E53EA31C48A41E1D5D50F1DAC01F6350AE1FF443;
	__StaticArrayInitTypeSizeU3D76_t2E1B361EC88495D058431EBCFF5B0B864454EB2D ___C133E473E5E653C5C4AEDB8BCC1C1A3A44D384FC0B6C0FCF04672B1B325EC01B;
	__StaticArrayInitTypeSizeU3D96_tB67E0A3E5FE860E65CA2C893456B2783E43C6BF9 ___C17FC0D6B61086A3027A72269EA83B74E5D4DE90109784012BCBD40D5E0A7F9E;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___C199D37D9A06A0F4C75F70A8ABD3D0C2FAEB17B255174C5E67392D81CF5FF0EC;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___C2D8E5EED6CBEBD8625FC18F81486A7733C04F9B0129FFBE974C68B90308B4F2;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___C4A6802D24BD0B4979A486C462DA989CD54F8816365BC0635A8A499FF421201C;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___C4E613C844059E36B3D4EE786C4236B8E641E7D49F520B003540BB274F7E12F7;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___C511AC322D19491FC1AEC0F16E619EA93B85F0A8B64D249B4FD2A36037ABFF21;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___C51FEE572495EE6ED75353FC262F765FF558EED94FCB9EE64987AEE6C8548E12;
	int32_t ___C71C8ED7041ABE0B7FD161D726857C18705285A9F1B478A006310909B4A6D900;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___C756EE85B4DA1713569FCED39D0AA9C7009DC57ED998306E5B308B52C3D708DE;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___C7A9E0E304D54F9BC620155E9082EC318075BD5F724319A94FE07FE3F7854EBD;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___C7B696E5CE2020FEC8F611D5047339850FECC787C3DBC5D36589354D8ADF98C1;
	__StaticArrayInitTypeSizeU3D20_tBBC315B0EB56B25AE323C40681A72237C83BC90F ___C7BF26D1D1E204D84FCE3AAB6A3F2EBF36C169312086FF1974E4210A48C44BE9;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___C854B091EA9394BFC39C4324F63BF827E324ABA5F68693B00E4DB8B247ED2023;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___C90D4186D61A339F3078D8C7A6CBB1F005BFF5039C44C37F526BDCE2E00200BE;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___C9541A54E9FAF920DF876495B731F33E63F59079E054F383D71B1AA755195DA0;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___CA25863AE5187A051BF6BAA4FB975EF2ADB6F98678EF6816EA5B350B15843B6F;
	__StaticArrayInitTypeSizeU3D96_tB67E0A3E5FE860E65CA2C893456B2783E43C6BF9 ___CB11E26FF20D3318A5A76F5FC0E725883F81ED29DAD7E32947BA8779AA3DE37F;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___CC07D4A4B4FC6D98A85528D499B36F8489D6438071F4AADA11701D134678AFB1;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___CC9BD9D64572985ECFB6E2A723B6DD347BB34F5AFECDBFB98444028D2DF4844D;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___CCD83CF06E3CE2D4A938C42BAEA101B30335EAF483F59E5EDCB41DF7C4807E1B;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___CDAEE7E50DA05783C75D649650C5CDC6ABB06447D6A9CFBB2E7212421547E53B;
	__StaticArrayInitTypeSizeU3D68_t318EC812D5290F3EAE538193B28FD23FE95F673E ___CF64D219C0BA56CECE4E41E0C8BF3AF538F4510FA9A2B00F38DA09E548270E5C;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___D15388770BF0AB6F544906F0A1A3375BE05BB7FEB2B215CED8A8E41C42972549;
	int64_t ___D17B14DF03300352F136B4013EACFCB8E6823957114F0B2335E9109E0B50488D;
	__StaticArrayInitTypeSizeU3D26_t1C03749A4A33D1E820B914DD0655B85875449DED ___D1F54F5BBFE626A7CD4332F4064FD2BFF36A5421C7224B12D7DC963DCE097FD5;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___D1FAE5443A715D6F96387473C649F557670B8A92E175272491DFC646754F4A9D;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___D38868B99B6C1AB9F2E65E75D07BA90CAF6DA7F4D74F9142A44413647473B362;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___D3ADE9315DD51AE00C4C2D80ED119F3D1B1743E64ADC7FDDDE505286274C877D;
	__StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 ___D418DB486BF8B92DFFFCE10BAD8A6FC3264E79F69335936023763DDA2F6DC2BB;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___D53AF9DDABA80012F885E4986DA0E68CE8B6C56ECA927CAE5097A1EBE8984DF3;
	__StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 ___D6380FB2FF2DD88C9290D88B81E78BCA13AAAB4BE827B103062AFBB1085B0793;
	int64_t ___D6DBCEA3A0C3EA4879712F5B58A2A2650F79C5418D6D74B5894CC456ADC0EE59;
	__StaticArrayInitTypeSizeU3D28_t74A83EEED063F1FEFB3725CCE71DE1346BBAD6DB ___D8E27A8A4D3F6436030A40EA98E1CF5EB03C41446DD9DBFBB4D15ED250149F53;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___D916EBA7A5A5D84BB85D26EE65A08CC18219FACEB615519209ABB469C0B35D32;
	__StaticArrayInitTypeSizeU3D56_tB99B60BDC6F99AA8CB6DE18EE851EF73191EA65F ___D9388FECE7A168D1EFFA85FA9C8DA1AEED48DDCE2E3799E07F41A9B60E389722;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___D981C2094C26B59AD566C6D0B0A6BFEDE90BF76F151CC9D70423A79EE717540D;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___D9FFE383B5494C43B58BAAF12EC32DACE0E661674BEA88E45787E99F79449B1C;
	__StaticArrayInitTypeSizeU3D336_t4E7B2D63CE5081C13619A5BCFD777B3C8B051420 ___DA5BDA15352EE630B7F2B5B73A13086272FD1AEBCD11CAF4DF6F23E8CD88A6D2;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___DB1CCE19AFD5123D31DCDAC6656A6DB5AF929CA39192511B8C042DD0513E7497;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___DC196B5ACC67550E50DDC963D9192AEB816537E6A74CFB7DFE3A0453E5F4FB82;
	__StaticArrayInitTypeSizeU3D24_t9C623179D452F924B08B5EF245F32CC43EB892F0 ___DC1CDED6204DF70AC5C64ED2C9958FE75479400FE1E1A80F77C3A5BC6FFAF0A5;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___DC94C6E04C48743338670B79BCDA997FEEBD39A7A2DBACD46FC5A27D37AD9F71;
	__StaticArrayInitTypeSizeU3D480_t0FCDCCC5474AD52A475FC222DE9FDD13BDA5A2A1 ___DCEC920EACDC77E3CD816EAF55038172FE433A419673896671BACC1260BAA287;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___DD2B2ACED61DA9A12AEE4F581C0D33D6D76A9722CDC6355B23FB5DF6B2032B18;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___DDA4EDB734C11C20B2D8A7BE67E2BB03E2F67B7928B0DAEB15EBF2EDD9A5150B;
	int32_t ___DE422551A0E0EF2710E43824DEFE8B2204F5FB76F83D7A496BA3F8D7D66569EB;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___DE56C9263420A7B28A6286EB62AFBE2C1D5164698C518792FD8CEAAF2CB2CD03;
	__StaticArrayInitTypeSizeU3D128_t132F46F03AB9DDB64721E907BC73DEC9034183A1 ___DE65050FB26A30C376E77EC7E5BE136721540CCE3760EAA1E14854746CADF7E8;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___DE7794C91146C2C522ED246AB61B9E93EEE9F714836C3ACF6F3146A90ABAA86D;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___DE9FD94010C5C932FEB066B288CE65EBEB81EDE0E87B31B900BF855BFA2A2B5D;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___DF93C334B40CA305A5AC46FA2BA7DB2C63404A699767F59982006C623702D568;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___E09120C0E7B92A532558D22D44D52452DFE460D6205D1009963B69AABD0F8185;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___E0B612D5DB93032752ADA2A0D5F44D2142ECC98ED4E6E4C47646FD64905A419B;
	int64_t ___E0E3CF58E8EBD3158219B64F434304727B1C71307D99BC27D059966A854CB749;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___E27F6E9719BC23F7138A27009F35243C6F94F6530A37FC26DE498AB64784DDDA;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___E33F0F47E0AF3A894FCCB1E7A0A361A5B755B6050AD72E9CEC692900EC7E5A15;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___E3668879E09A70694ED6FEE919E303D7EA1266D783955D4DEEDC99CCC5AA6A8D;
	int64_t ___E37A4C7B47BDD710723A94D9A8660903C91618B0F35881401EF08AA9E4674850;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___E37DD5BFC5A50748F147668D7E7DF9E118AEAE6A7FD32790737C9C5F8E2DFFDD;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___E3B4E767F318263A8E7A64E1D61F6D3368A3B59AF34CBB94E657E7FCC58F5BA5;
	__StaticArrayInitTypeSizeU3D20_tBBC315B0EB56B25AE323C40681A72237C83BC90F ___E3DB1BCE79976D586D8B218DCAC26CAD32EA98C00E0A2CD37172FCAF14157178;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___E45C3F86EC68FBD407D74FAEC9A50DFBAA5589A6FB63CCC5F7AA901D2ACA48B6;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___E5325952F9FF2214CCE24E320F2BCD6A71C6EB4127F6069352CE065883AE80D0;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___E5C7FE03A64774811CE619EFF7F420289E17179DCC1C546D7CDD59DBDAFB6A60;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___E75B7154F674F25D54E89CB7D91F35CC111C8A67EE585938E07D12CB4761637F;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___E7BFA12A4BD73B706BF3866FE18F93A4BC8C63A9EBE87405EDA03FDDF28BBB0A;
	__StaticArrayInitTypeSizeU3D28_t74A83EEED063F1FEFB3725CCE71DE1346BBAD6DB ___E7C6C4296C6C2B00D1062AA69054CAB58A57C3ACCE5ECFA4084E7E94CF805F87;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___E7FE43981DE57CAAC1EBA73514D470E93A9F63D5B4CC79639EC32B78561045DC;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___E87A3B2C4C5D7247B7E149126C3503D3AE2878A02155EA6153E057FE35D9656C;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___E8D46701532BBEB9F3FF8A63C0D74D8C417C782DEE2C085A6D940A8B5B38DA18;
	int32_t ___EB6B70CC36DAA15B37C45E6D0AE3CB7E8A9B1BA69E6BC44762F886192C725A5A;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___EBAC23898E4FCF07BD76B137DC6E3E05E846FD5AAAF62D38CCF133EFC010C029;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___EC103A6146A987638687685583404F2C172DE3138C47B820021673AD5DA00660;
	__StaticArrayInitTypeSizeU3D7040_t2134457E804FC7BC13527BC4CA58B8E829FB1CCD ___EC573F4ACC4947071C12F358C926626C0FE47C3FA07CE94EC382D50335579EE6;
	__StaticArrayInitTypeSizeU3D40_tFFC4D9A884F828385AEB328BC0FDA69C7FC68E68 ___ECA151F23D399F9FE976255B143D0691D0BE043E033B0E2814C0A547336D6545;
	__StaticArrayInitTypeSizeU3D92_tAA93827A0F3444A283A14C37D07A5776CB73B56D ___ECB6F584A076C87683B062D11A24AA1039660A11A111411947F1E8AF7D819856;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___ED9008AD818579446B5FC50357134EF59B401066A42AA7FCBF90F7D35C8C783A;
	__StaticArrayInitTypeSizeU3D1024_tDFC6F28476DD6178AF6033A1B5CA38AC81BC17F3 ___EE200BFE29520E257D3C3F868435743B2A90B987D4F8F54E9D5DFEAA35B3099A;
	__StaticArrayInitTypeSizeU3D80_t61FC199E6DFD14137060F918DE5B788D5B66E75D ___EE6E1CBF3DD198235B836FA8CEAE76D590D422037F6F8685E5B41CBAB8448A81;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___EEE64554D8EF8D44A58943A4EA40FC6C6F0F62C1489A6F716BCFC4C8503E6948;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___EF39C5A882F9477B2A250BA257247825CEB07FC53C3C984385F2C2E5F8222431;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___EF7F3D1F9F5FC7985E4AE3D48BF986867BA544C1D2FEA6A7745FEA4A2FCAB381;
	__StaticArrayInitTypeSizeU3D236_tEF7D1050144F7923187B26AB548E1192336CA9FF ___EF95503A698271C53358488353EE6F4CE342098F6F4359BBF238860CC0CBB9A2;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___F2CD85BDEF1E84E95EA92E2B686CEEC9FC518118A4342FAB46BECECE253D71F4;
	__StaticArrayInitTypeSizeU3D404_tFECF215CCE6D0F9CCE83B0EDCD25F93B2CBD119C ___F30ADE318602BE9DC38E68CA7E1FE366A1F173330197F71AFD9D555F765D47A7;
	__StaticArrayInitTypeSizeU3D56_tB99B60BDC6F99AA8CB6DE18EE851EF73191EA65F ___F322B1C31AC162C97BA845471208C5BAA8709C65BD446AC5BB11267912EAFFE5;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___F42A21A8C4DAEEB40F3FE4E0BB60A6312C546C94759E076AEBC0DA92D92D9D22;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___F509AE84659919EA56BFF8D829ED41E971314D3D46ED9B0F8F5670D72EB1688B;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___F51A5E73B47ACEDA9BB9091E80F92B81F5E98C82889DACD4D6C9DF8B230C6FB9;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___F542419C019F996698636B099E1479F11362C67C5331FF2CD10FB833EB5F8607;
	__StaticArrayInitTypeSizeU3D68_t318EC812D5290F3EAE538193B28FD23FE95F673E ___F5F4B262F18D007A4CBBCA61A959AFBC8426E7FE783E3C55CB0AAB80BCDAF3D8;
	__StaticArrayInitTypeSizeU3D116_tAFCAA0A6BBF3C7D612817453792131CAF1600CD7 ___F8D7861760C88CC514F66095AF0AED47ECBA063ADB65F47125ED07BCC2CF9842;
	__StaticArrayInitTypeSizeU3D192_t619ED94196E112EB663AFC2D8F81A9DEA2E2450D ___F8E6964ADFFC257265537256D21E93496A17B9271DC5D64F40869EEEAAFF6FBB;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___F915BC5619C5EB2C4D315B5409BC531F55FA3E3A6167F57B7A4F5F7F9C5F022B;
	__StaticArrayInitTypeSizeU3D64_t8E7B7D7B4C99F735EE75C6B84D57BD9C0B1CDBD4 ___F93A6E9A709B14FD05706C119D157EEE96F24DE3FDFA34F6D0AFF9537DE8ACC0;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___F9802236EEEDC067D453EFF003EBF4675DA9D722F352DFBAE6510DE431C6AAA9;
	__StaticArrayInitTypeSizeU3D72_t6FF675DF5424D33F817FCFBA5F96A93536229EFD ___FA1AD270B23BA640E88EE7F51CC9C0C1A6C6BB1F2B9025682A7D30FB3BDA64F0;
	__StaticArrayInitTypeSizeU3D12_t427F305DAFF72C2AA104110623684B469977584C ___FA624E5847BD2E5A00CCA1B3515D315FA5C3450C3E832077935CEBA18E9F5571;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___FA902FDBDFBCC8763875D512059F3073294FE8C37A17ED9F7AF4D83791E194BF;
	__StaticArrayInitTypeSizeU3D256_t2BB16B771F7D18982783AC59068316841167DDF4 ___FA9320F15F87607CD9E1033E6DD907B11EEDF491D7EBBFD29EC8D07E12A822CD;
	__StaticArrayInitTypeSizeU3D384_t7C09BC68D7D62B97E3F3F0D128792BAC28ABD995 ___FB3891EAFF6D32739F9BE64EBF38958D55D4F8146C3967D62F6ADC652CF41E88;
	__StaticArrayInitTypeSizeU3D2048_tE47C348E11AA52E12916036B8BFD7023BF361ED6 ___FB7A6532989D730F69E4757D690D25A43E71041FCFF7355747E274F956BB03E9;
	__StaticArrayInitTypeSizeU3D120_tAD8E3F872D03771C28D56F19BA94D0F528874DAE ___FC216F5C5AE2947D800794ECD5F752EE8381073C2E5D0D095FDA040F541702F3;
	__StaticArrayInitTypeSizeU3D96_tB67E0A3E5FE860E65CA2C893456B2783E43C6BF9 ___FC2D643DD9472B6F409E319DD565570A2F9F78DDE4B4B561B9DDE85C377CD8F6;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___FD85B0A9251A04B1D271AF14FD041B4606FA95DDB116F92CB14470653A62AD19;
	__StaticArrayInitTypeSizeU3D32_tA57049EC9C13D729E962243FD84572E6657F8CBE ___FF1F6EE5D67458CFAC950F62E93042E21FCB867E2234DCC8721801231064AD40;
	__StaticArrayInitTypeSizeU3D128_t132F46F03AB9DDB64721E907BC73DEC9034183A1 ___FF4D5896EDB9E12991ECB7FA7807EEB0F7ACD30F57ECB63DD9943F5245752A0F;
	__StaticArrayInitTypeSizeU3D16_t5ED1A5D9486B715E2E9E9B1DC5EFC25E6570E6ED ___FFC55B79B9BE7817954A4672A75C8603D1D1FE85A61786D63BD3B0AA239FFA58;
};
struct AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_StaticFields
{
	Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37* ___objIds;
	Dictionary_2_t182C3AD5398AA6215D318D59CC83975DFFD64E6F* ___curves;
	Dictionary_2_tCAD1DB714FE7CE64DFBD12C9AC15D2C23DDC6329* ___names;
};
struct AnssiObjectIdentifiers_t844F7B339921BED06CC1D1EE7670EB2FAAE8CD2B_StaticFields
{
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___FRP256v1;
};
struct Asn1EncodableVector_t57A80683315794E6BD14B3D2368B95FE29304EC3_StaticFields
{
	Asn1EncodableU5BU5D_t9868017FA96E51C6C30CD73898AFD5C50CF466AF* ___EmptyElements;
};
struct BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB_StaticFields
{
	Int32U5BU5DU5BU5D_t179D865D5B30EFCBC50F82C9774329C15943466E* ___primeLists;
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___primeProducts;
	UInt32U5BU5D_t02FBD658AD156A17574ECE6106CF1FBFCC9807FA* ___ZeroMagnitude;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___ZeroEncoding;
	BigIntegerU5BU5D_tFB92569678018498E1F36219AFF02E9250782B39* ___SMALL_CONSTANTS;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___Zero;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___One;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___Two;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___Three;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___Four;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___Five;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___Six;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___Ten;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___BitLengthTable;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___radix2;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___radix2E;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___radix8;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___radix8E;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___radix10;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___radix10E;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___radix16;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___radix16E;
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___ExpWindowThresholds;
};
struct ECPoint_t0CE8D3D2A088BBE0E67B3DD87EA91AA0956659BC_StaticFields
{
	ECFieldElementU5BU5D_t973BBFE465E171F079533BA486672CB4206ACDB9* ___EMPTY_ZS;
};
struct String_t_StaticFields
{
	String_t* ___Empty;
};
struct StringComparer_t6268F19CA34879176651429C0D8A3D0002BB8E06_StaticFields
{
	CultureAwareComparer_t5822A6535A6EB4C448D1B7736067D1188BAEE8CD* ___s_invariantCulture;
	CultureAwareComparer_t5822A6535A6EB4C448D1B7736067D1188BAEE8CD* ___s_invariantCultureIgnoreCase;
	OrdinalCaseSensitiveComparer_t581CA7CB51DCF00B6012A697A4B4B3067144521A* ___s_ordinal;
	OrdinalIgnoreCaseComparer_t8BAE11990A4C855D3BCBBFB42F4EF8D45088FBB0* ___s_ordinalIgnoreCase;
};
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	String_t* ___TrueString;
	String_t* ___FalseString;
};
struct X509Name_t02B17BA8A942D5E353104A8874F958D86337D136_StaticFields
{
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___C;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___O;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___OU;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___T;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___CN;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___Street;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___SerialNumber;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___L;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___ST;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___Surname;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___GivenName;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___Initials;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___Generation;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___UniqueIdentifier;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___BusinessCategory;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___PostalCode;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___DnQualifier;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___Pseudonym;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___DateOfBirth;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___PlaceOfBirth;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___Gender;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___CountryOfCitizenship;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___CountryOfResidence;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___NameAtBirth;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___PostalAddress;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___DmdName;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___TelephoneNumber;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___OrganizationIdentifier;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___Name;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___EmailAddress;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___UnstructuredName;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___UnstructuredAddress;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___E;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___DC;
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___UID;
	int64_t ___defaultReverse;
	RuntimeObject* ___DefaultSymbolsInternal;
	RuntimeObject* ___DefaultSymbols;
	RuntimeObject* ___RFC2253SymbolsInternal;
	RuntimeObject* ___RFC2253Symbols;
	RuntimeObject* ___RFC1779SymbolsInternal;
	RuntimeObject* ___RFC1779Symbols;
	RuntimeObject* ___DefaultLookupInternal;
	RuntimeObject* ___DefaultLookup;
};
struct Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791_StaticFields
{
	X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* ___Instance;
};
struct DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_StaticFields
{
	DerObjectIdentifierU5BU5D_tD2CFD10A2D7EFA54014F4A5FDA4A3DA8A852F42F* ___Cache;
};
struct DerSequence_tCC88E8724998083FAABF65D386A19D9C8C88C962_StaticFields
{
	DerSequence_tCC88E8724998083FAABF65D386A19D9C8C88C962* ___Empty;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m93FFFABE8FCE7FA9793F0915E2A8842C7CD0C0C1_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___0_key, RuntimeObject* ___1_value, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_mB2760A703784902BE10E873BC760166EC9693D63_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___0_comparer, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CollectionUtilities_GetValueOrNull_TisRuntimeObject_TisRuntimeObject_m79C18B610DEE39D9E4A2145DFACDB0969F105F92_gshared (RuntimeObject* ___0_d, RuntimeObject* ___1_k, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KeyCollection_tB45A861D090B15129521119AE48ED3813820A974* Dictionary_2_get_Keys_m72D290F90654BFD683FA7AA7C63D9F4F692218B6_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CollectionUtilities_Proxy_TisRuntimeObject_mC99E99AF42EB04F3E58EEDC16EC81FFFB5B186B5_gshared (RuntimeObject* ___0_e, const RuntimeMethod* method) ;

IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedCertificate__ctor_m1454059386597CC771DCCF4053DF9327EBF28CBC (LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23* __this, DigestInfo_tCA55423EC3809B4806046638EC406D849FFEC74A* ___0_digest, GeneralName_tF34D1432388833854897D2F99AE6DC6A9896C6A5* ___1_certLocation, X509Name_t02B17BA8A942D5E353104A8874F958D86337D136* ___2_certIssuer, GeneralNames_t9B20892C17FAA17729EE7445077B67D79DFEF3F4* ___3_caCerts, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Asn1Encodable__ctor_m500AAA6AB43FC53A5B630436B4C79C61D1AD7384 (Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DigestInfo_tCA55423EC3809B4806046638EC406D849FFEC74A* DigestInfo_GetInstance_m5D94B0A8E1B12F3050D43A18FEBB35414E12CA9F (RuntimeObject* ___0_obj, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GeneralName_tF34D1432388833854897D2F99AE6DC6A9896C6A5* GeneralName_GetInstance_m06242A492664A46E12F76BDFEF623D18083BE6E6 (RuntimeObject* ___0_obj, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Asn1TaggedObject_t4172254B2BA64C9B954F304147C2E057FCE20DD2* Asn1TaggedObject_GetInstance_m26FB2B8E74FEEA21AF87F9D2CE656092AAC8F8E4 (RuntimeObject* ___0_obj, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Asn1TaggedObject_get_TagNo_m7763F3E854407E34E059C9F23D35E1F92B72183F_inline (Asn1TaggedObject_t4172254B2BA64C9B954F304147C2E057FCE20DD2* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR X509Name_t02B17BA8A942D5E353104A8874F958D86337D136* X509Name_GetInstance_m111837FEBCFD8A7B6D5B8DC7DC947291ECD2392B (Asn1TaggedObject_t4172254B2BA64C9B954F304147C2E057FCE20DD2* ___0_obj, bool ___1_explicitly, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GeneralNames_t9B20892C17FAA17729EE7445077B67D79DFEF3F4* GeneralNames_GetInstance_mCE79EE1565CD18EC05FD723CA5796A4E275F57BF (Asn1TaggedObject_t4172254B2BA64C9B954F304147C2E057FCE20DD2* ___0_obj, bool ___1_explicitly, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465 (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* __this, String_t* ___0_message, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Asn1Sequence_t90471C8D847766B1B77A7AB098A9E8A7DF38A263* Asn1Sequence_GetInstance_m1964A044875938741B0C8FDBD1E03F3A801625BB (RuntimeObject* ___0_obj, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedCertificate__ctor_m3DB0B15D87C2BE7091C77D0B7BE13B0D827CBD50 (LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23* __this, Asn1Sequence_t90471C8D847766B1B77A7AB098A9E8A7DF38A263* ___0_seq, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Asn1EncodableVector__ctor_mB50955EC8FA621ACC95FDE90DA4BB971E4A334B6 (Asn1EncodableVector_t57A80683315794E6BD14B3D2368B95FE29304EC3* __this, Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389* ___0_element1, Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389* ___1_element2, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Asn1EncodableVector_AddOptionalTagged_mC8FC6F6C6C812D32F98AF6F07565E4766884F4AE (Asn1EncodableVector_t57A80683315794E6BD14B3D2368B95FE29304EC3* __this, bool ___0_isExplicit, int32_t ___1_tagNo, Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389* ___2_obj, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DerSequence__ctor_mDB684D58E08E4FC02FB853471F375D26B19CDC85 (DerSequence_tCC88E8724998083FAABF65D386A19D9C8C88C962* __this, Asn1EncodableVector_t57A80683315794E6BD14B3D2368B95FE29304EC3* ___0_elementVector, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* Hex_DecodeStrict_m070A7B6DEACA2BECD080ECC83ED8DE72EF84F217 (String_t* ___0_str, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void X9ECPoint__ctor_m639E182F441B11BD4B1301E82C901A6073E72C25 (X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F* __this, ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* ___0_c, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___1_encoding, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ECPoint_t0CE8D3D2A088BBE0E67B3DD87EA91AA0956659BC* X9ECPoint_get_Point_m9FC99A8641D07923532F54F0E272B7285CEB5694 (X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WNafUtilities_ConfigureBasepoint_mD7F39A6175127FBBF54CE3CB0EBADFF04E806E64 (ECPoint_t0CE8D3D2A088BBE0E67B3DD87EA91AA0956659BC* ___0_p, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BigInteger__ctor_m83A164BAE780C5E2F0C4BB5EA61D82252D713BA1 (BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* __this, int32_t ___0_sign, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___1_bytes, const RuntimeMethod* method) ;
inline void Dictionary_2_Add_m60955C5A4F99886288BAED2052459C790963A0A5 (Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37* __this, String_t* ___0_key, DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37*, String_t*, DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7*, const RuntimeMethod*))Dictionary_2_Add_m93FFFABE8FCE7FA9793F0915E2A8842C7CD0C0C1_gshared)(__this, ___0_key, ___1_value, method);
}
inline void Dictionary_2_Add_m6E2846ACFDB0C08FCDF5CA041846F3DD94AE525F (Dictionary_2_tCAD1DB714FE7CE64DFBD12C9AC15D2C23DDC6329* __this, DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___0_key, String_t* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tCAD1DB714FE7CE64DFBD12C9AC15D2C23DDC6329*, DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7*, String_t*, const RuntimeMethod*))Dictionary_2_Add_m93FFFABE8FCE7FA9793F0915E2A8842C7CD0C0C1_gshared)(__this, ___0_key, ___1_value, method);
}
inline void Dictionary_2_Add_mE8A0BC751794ED911A168EEC22705B6AE1281D52 (Dictionary_2_t182C3AD5398AA6215D318D59CC83975DFFD64E6F* __this, DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___0_key, X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* ___1_value, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t182C3AD5398AA6215D318D59CC83975DFFD64E6F*, DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7*, X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76*, const RuntimeMethod*))Dictionary_2_Add_m93FFFABE8FCE7FA9793F0915E2A8842C7CD0C0C1_gshared)(__this, ___0_key, ___1_value, method);
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR StringComparer_t6268F19CA34879176651429C0D8A3D0002BB8E06* StringComparer_get_OrdinalIgnoreCase_m071AA1B1747345CCA058A3879EBDEBBA2EA4B169_inline (const RuntimeMethod* method) ;
inline void Dictionary_2__ctor_m73D6F97375AB4C5B363D6E7F565EAAEBC8194468 (Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37* __this, RuntimeObject* ___0_comparer, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37*, RuntimeObject*, const RuntimeMethod*))Dictionary_2__ctor_mB2760A703784902BE10E873BC760166EC9693D63_gshared)(__this, ___0_comparer, method);
}
inline void Dictionary_2__ctor_mAB97FFC5A0BBFBF98AFCEE0A10E07EB1D77736FC (Dictionary_2_t182C3AD5398AA6215D318D59CC83975DFFD64E6F* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t182C3AD5398AA6215D318D59CC83975DFFD64E6F*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
inline void Dictionary_2__ctor_m3C0AEA796137AC0AE0830766066A09DB45BA1FA8 (Dictionary_2_tCAD1DB714FE7CE64DFBD12C9AC15D2C23DDC6329* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tCAD1DB714FE7CE64DFBD12C9AC15D2C23DDC6329*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnssiNamedCurves_DefineCurve_m082910EE3B85F2F0CA0341F884761A0960D7532F (String_t* ___0_name, DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___1_oid, X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* ___2_holder, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* AnssiNamedCurves_GetOid_m21DBA9A8777060CD3A3DE8D0A451EFB834CF043D (String_t* ___0_name, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88* AnssiNamedCurves_GetByOid_mC6F9F11A2AE97D821CFC48E54B748475C8502CBC (DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___0_oid, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* AnssiNamedCurves_GetByOidLazy_m4DFC69EBB4D13926DD809D68AA67428BAA4408AE (DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___0_oid, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88* X9ECParametersHolder_get_Parameters_m92C356231D52FD7985E656E342AFF03AEB1F86B5 (X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* __this, const RuntimeMethod* method) ;
inline X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* CollectionUtilities_GetValueOrNull_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_TisX9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76_mFFF6119751EFFE0F4BFE1CEABE6B98DE4BB7887D (RuntimeObject* ___0_d, DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___1_k, const RuntimeMethod* method)
{
	return ((  X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* (*) (RuntimeObject*, DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7*, const RuntimeMethod*))CollectionUtilities_GetValueOrNull_TisRuntimeObject_TisRuntimeObject_m79C18B610DEE39D9E4A2145DFACDB0969F105F92_gshared)(___0_d, ___1_k, method);
}
inline String_t* CollectionUtilities_GetValueOrNull_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_TisString_t_mD7022EF1BC5D7215073BAB672FE85947718874EA (RuntimeObject* ___0_d, DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___1_k, const RuntimeMethod* method)
{
	return ((  String_t* (*) (RuntimeObject*, DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7*, const RuntimeMethod*))CollectionUtilities_GetValueOrNull_TisRuntimeObject_TisRuntimeObject_m79C18B610DEE39D9E4A2145DFACDB0969F105F92_gshared)(___0_d, ___1_k, method);
}
inline DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* CollectionUtilities_GetValueOrNull_TisString_t_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_mFE4804AAB5D611FAA5449945BFB5D25099CFCB29 (RuntimeObject* ___0_d, String_t* ___1_k, const RuntimeMethod* method)
{
	return ((  DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* (*) (RuntimeObject*, String_t*, const RuntimeMethod*))CollectionUtilities_GetValueOrNull_TisRuntimeObject_TisRuntimeObject_m79C18B610DEE39D9E4A2145DFACDB0969F105F92_gshared)(___0_d, ___1_k, method);
}
inline KeyCollection_tF8B8A6459322625D7F08C99265BDC0AACB1B6682* Dictionary_2_get_Keys_mCEE30023E7EA59ED772D73F6CB371999419CB44F (Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37* __this, const RuntimeMethod* method)
{
	return ((  KeyCollection_tF8B8A6459322625D7F08C99265BDC0AACB1B6682* (*) (Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37*, const RuntimeMethod*))Dictionary_2_get_Keys_m72D290F90654BFD683FA7AA7C63D9F4F692218B6_gshared)(__this, method);
}
inline RuntimeObject* CollectionUtilities_Proxy_TisString_t_m6E2841A61A0D5E4BA6D3130680264F4BE597A573 (RuntimeObject* ___0_e, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, const RuntimeMethod*))CollectionUtilities_Proxy_TisRuntimeObject_mC99E99AF42EB04F3E58EEDC16EC81FFFB5B186B5_gshared)(___0_e, method);
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void X9ECParametersHolder__ctor_mF94548A7AB4CD57DA7A0D87FE9AE4F966855F863 (X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* AnssiNamedCurves_FromHex_m4E2FF2B5B36A33F1D17D4A84FADAE91B35B28DD5 (String_t* ___0_hex, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FpCurve__ctor_m1937532A16F7AAA88DD89168AADDC86CDEFA4383 (FpCurve_t9C4D086FD235C2976984476721DF8DEAF8BA86A1* __this, BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___0_q, BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___1_a, BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___2_b, BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___3_order, BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___4_cofactor, bool ___5_isInternal, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* AnssiNamedCurves_ConfigureCurve_mCCAFFB8A60199F7058A130F49FFA793F570E773B (ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* ___0_curve, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* X9ECParametersHolder_get_Curve_m981F739459C722DAAC44B6B0CE7E217D3C47231B (X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F* AnssiNamedCurves_ConfigureBasepoint_m23FC190A3A09918EA0D3EBF2432977A6C1F67D8E (ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* ___0_curve, String_t* ___1_encoding, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void X9ECParameters__ctor_m3318A29AC6F8998915CFFF30E088B0FECEA6690D (X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88* __this, ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* ___0_curve, X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F* ___1_g, BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___2_n, BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* ___3_h, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___4_seed, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Frp256v1Holder__ctor_m1D913952B061E660F7509F0DA5350EE2E0EF8D4E (Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DerObjectIdentifier__ctor_m53BD00BF63F38ED193D80D05618ED708AAC71B3F (DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* __this, String_t* ___0_identifier, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedCertificate__ctor_m56C08FC20FD6424C20A37042EC9FB1F3DE6FB49C (LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23* __this, DigestInfo_tCA55423EC3809B4806046638EC406D849FFEC74A* ___0_digest, GeneralName_tF34D1432388833854897D2F99AE6DC6A9896C6A5* ___1_certLocation, const RuntimeMethod* method) 
{
	{
		DigestInfo_tCA55423EC3809B4806046638EC406D849FFEC74A* L_0 = ___0_digest;
		GeneralName_tF34D1432388833854897D2F99AE6DC6A9896C6A5* L_1 = ___1_certLocation;
		LinkedCertificate__ctor_m1454059386597CC771DCCF4053DF9327EBF28CBC(__this, L_0, L_1, (X509Name_t02B17BA8A942D5E353104A8874F958D86337D136*)NULL, (GeneralNames_t9B20892C17FAA17729EE7445077B67D79DFEF3F4*)NULL, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedCertificate__ctor_m1454059386597CC771DCCF4053DF9327EBF28CBC (LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23* __this, DigestInfo_tCA55423EC3809B4806046638EC406D849FFEC74A* ___0_digest, GeneralName_tF34D1432388833854897D2F99AE6DC6A9896C6A5* ___1_certLocation, X509Name_t02B17BA8A942D5E353104A8874F958D86337D136* ___2_certIssuer, GeneralNames_t9B20892C17FAA17729EE7445077B67D79DFEF3F4* ___3_caCerts, const RuntimeMethod* method) 
{
	{
		Asn1Encodable__ctor_m500AAA6AB43FC53A5B630436B4C79C61D1AD7384(__this, NULL);
		DigestInfo_tCA55423EC3809B4806046638EC406D849FFEC74A* L_0 = ___0_digest;
		__this->___mDigest = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___mDigest), (void*)L_0);
		GeneralName_tF34D1432388833854897D2F99AE6DC6A9896C6A5* L_1 = ___1_certLocation;
		__this->___mCertLocation = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___mCertLocation), (void*)L_1);
		X509Name_t02B17BA8A942D5E353104A8874F958D86337D136* L_2 = ___2_certIssuer;
		__this->___mCertIssuer = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___mCertIssuer), (void*)L_2);
		GeneralNames_t9B20892C17FAA17729EE7445077B67D79DFEF3F4* L_3 = ___3_caCerts;
		__this->___mCACerts = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___mCACerts), (void*)L_3);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedCertificate__ctor_m3DB0B15D87C2BE7091C77D0B7BE13B0D827CBD50 (LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23* __this, Asn1Sequence_t90471C8D847766B1B77A7AB098A9E8A7DF38A263* ___0_seq, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&X509Name_t02B17BA8A942D5E353104A8874F958D86337D136_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Asn1TaggedObject_t4172254B2BA64C9B954F304147C2E057FCE20DD2* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Asn1Encodable__ctor_m500AAA6AB43FC53A5B630436B4C79C61D1AD7384(__this, NULL);
		Asn1Sequence_t90471C8D847766B1B77A7AB098A9E8A7DF38A263* L_0 = ___0_seq;
		NullCheck(L_0);
		Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389* L_1;
		L_1 = VirtualFuncInvoker1< Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389*, int32_t >::Invoke(19, L_0, 0);
		DigestInfo_tCA55423EC3809B4806046638EC406D849FFEC74A* L_2;
		L_2 = DigestInfo_GetInstance_m5D94B0A8E1B12F3050D43A18FEBB35414E12CA9F(L_1, NULL);
		__this->___mDigest = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___mDigest), (void*)L_2);
		Asn1Sequence_t90471C8D847766B1B77A7AB098A9E8A7DF38A263* L_3 = ___0_seq;
		NullCheck(L_3);
		Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389* L_4;
		L_4 = VirtualFuncInvoker1< Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389*, int32_t >::Invoke(19, L_3, 1);
		GeneralName_tF34D1432388833854897D2F99AE6DC6A9896C6A5* L_5;
		L_5 = GeneralName_GetInstance_m06242A492664A46E12F76BDFEF623D18083BE6E6(L_4, NULL);
		__this->___mCertLocation = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___mCertLocation), (void*)L_5);
		V_0 = 2;
		goto IL_0078;
	}

IL_002e:
	{
		Asn1Sequence_t90471C8D847766B1B77A7AB098A9E8A7DF38A263* L_6 = ___0_seq;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389* L_8;
		L_8 = VirtualFuncInvoker1< Asn1Encodable_tFEE0B0C4179DE5C1AA6C81FA97029DCE75244389*, int32_t >::Invoke(19, L_6, L_7);
		Asn1TaggedObject_t4172254B2BA64C9B954F304147C2E057FCE20DD2* L_9;
		L_9 = Asn1TaggedObject_GetInstance_m26FB2B8E74FEEA21AF87F9D2CE656092AAC8F8E4(L_8, NULL);
		V_1 = L_9;
		Asn1TaggedObject_t4172254B2BA64C9B954F304147C2E057FCE20DD2* L_10 = V_1;
		NullCheck(L_10);
		int32_t L_11;
		L_11 = Asn1TaggedObject_get_TagNo_m7763F3E854407E34E059C9F23D35E1F92B72183F_inline(L_10, NULL);
		V_2 = L_11;
		int32_t L_12 = V_2;
		if (!L_12)
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_13 = V_2;
		if ((((int32_t)L_13) == ((int32_t)1)))
		{
			goto IL_005a;
		}
	}
	{
		goto IL_0069;
	}

IL_004b:
	{
		Asn1TaggedObject_t4172254B2BA64C9B954F304147C2E057FCE20DD2* L_14 = V_1;
		il2cpp_codegen_runtime_class_init_inline(X509Name_t02B17BA8A942D5E353104A8874F958D86337D136_il2cpp_TypeInfo_var);
		X509Name_t02B17BA8A942D5E353104A8874F958D86337D136* L_15;
		L_15 = X509Name_GetInstance_m111837FEBCFD8A7B6D5B8DC7DC947291ECD2392B(L_14, (bool)0, NULL);
		__this->___mCertIssuer = L_15;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___mCertIssuer), (void*)L_15);
		goto IL_0074;
	}

IL_005a:
	{
		Asn1TaggedObject_t4172254B2BA64C9B954F304147C2E057FCE20DD2* L_16 = V_1;
		GeneralNames_t9B20892C17FAA17729EE7445077B67D79DFEF3F4* L_17;
		L_17 = GeneralNames_GetInstance_mCE79EE1565CD18EC05FD723CA5796A4E275F57BF(L_16, (bool)0, NULL);
		__this->___mCACerts = L_17;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___mCACerts), (void*)L_17);
		goto IL_0074;
	}

IL_0069:
	{
		ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* L_18 = (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465(L_18, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralBCDB7300C3DB42608F4FD5306F6DB1A47E67D4CB)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&LinkedCertificate__ctor_m3DB0B15D87C2BE7091C77D0B7BE13B0D827CBD50_RuntimeMethod_var)));
	}

IL_0074:
	{
		int32_t L_19 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_0078:
	{
		int32_t L_20 = V_0;
		Asn1Sequence_t90471C8D847766B1B77A7AB098A9E8A7DF38A263* L_21 = ___0_seq;
		NullCheck(L_21);
		int32_t L_22;
		L_22 = VirtualFuncInvoker0< int32_t >::Invoke(20, L_21);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_002e;
		}
	}
	{
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23* LinkedCertificate_GetInstance_m939156BA971A390B9E2698A676FC757D8F63C44B (RuntimeObject* ___0_obj, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___0_obj;
		if (!((LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23*)IsInstClass((RuntimeObject*)L_0, LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23_il2cpp_TypeInfo_var)))
		{
			goto IL_000f;
		}
	}
	{
		RuntimeObject* L_1 = ___0_obj;
		return ((LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23*)CastclassClass((RuntimeObject*)L_1, LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23_il2cpp_TypeInfo_var));
	}

IL_000f:
	{
		RuntimeObject* L_2 = ___0_obj;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		RuntimeObject* L_3 = ___0_obj;
		Asn1Sequence_t90471C8D847766B1B77A7AB098A9E8A7DF38A263* L_4;
		L_4 = Asn1Sequence_GetInstance_m1964A044875938741B0C8FDBD1E03F3A801625BB(L_3, NULL);
		LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23* L_5 = (LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23*)il2cpp_codegen_object_new(LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23_il2cpp_TypeInfo_var);
		LinkedCertificate__ctor_m3DB0B15D87C2BE7091C77D0B7BE13B0D827CBD50(L_5, L_4, NULL);
		return L_5;
	}

IL_001e:
	{
		return (LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23*)NULL;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DigestInfo_tCA55423EC3809B4806046638EC406D849FFEC74A* LinkedCertificate_get_Digest_mBD8D3430ED7744198FC0365A094C79399E2A135D (LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23* __this, const RuntimeMethod* method) 
{
	{
		DigestInfo_tCA55423EC3809B4806046638EC406D849FFEC74A* L_0 = __this->___mDigest;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GeneralName_tF34D1432388833854897D2F99AE6DC6A9896C6A5* LinkedCertificate_get_CertLocation_m71BA3E1A0FCFC2A28F653DB4437A02D8E297A090 (LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23* __this, const RuntimeMethod* method) 
{
	{
		GeneralName_tF34D1432388833854897D2F99AE6DC6A9896C6A5* L_0 = __this->___mCertLocation;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR X509Name_t02B17BA8A942D5E353104A8874F958D86337D136* LinkedCertificate_get_CertIssuer_m15425BFA4F325D9AFBFFA46989A08711CAA97756 (LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23* __this, const RuntimeMethod* method) 
{
	{
		X509Name_t02B17BA8A942D5E353104A8874F958D86337D136* L_0 = __this->___mCertIssuer;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GeneralNames_t9B20892C17FAA17729EE7445077B67D79DFEF3F4* LinkedCertificate_get_CACerts_m08A42CA994AC93C6B4FE94EC8AD207540719CDDF (LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23* __this, const RuntimeMethod* method) 
{
	{
		GeneralNames_t9B20892C17FAA17729EE7445077B67D79DFEF3F4* L_0 = __this->___mCACerts;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Asn1Object_t88A4CCA55F497755711F405EE399C0720E53976B* LinkedCertificate_ToAsn1Object_m9E74069A613AB7E0947929A5CE7F91571FD7AC54 (LinkedCertificate_t32F3B302E274D363B8655B9F731AEE02E3700A23* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Asn1EncodableVector_t57A80683315794E6BD14B3D2368B95FE29304EC3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DerSequence_tCC88E8724998083FAABF65D386A19D9C8C88C962_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DigestInfo_tCA55423EC3809B4806046638EC406D849FFEC74A* L_0 = __this->___mDigest;
		GeneralName_tF34D1432388833854897D2F99AE6DC6A9896C6A5* L_1 = __this->___mCertLocation;
		Asn1EncodableVector_t57A80683315794E6BD14B3D2368B95FE29304EC3* L_2 = (Asn1EncodableVector_t57A80683315794E6BD14B3D2368B95FE29304EC3*)il2cpp_codegen_object_new(Asn1EncodableVector_t57A80683315794E6BD14B3D2368B95FE29304EC3_il2cpp_TypeInfo_var);
		Asn1EncodableVector__ctor_mB50955EC8FA621ACC95FDE90DA4BB971E4A334B6(L_2, L_0, L_1, NULL);
		Asn1EncodableVector_t57A80683315794E6BD14B3D2368B95FE29304EC3* L_3 = L_2;
		X509Name_t02B17BA8A942D5E353104A8874F958D86337D136* L_4 = __this->___mCertIssuer;
		NullCheck(L_3);
		Asn1EncodableVector_AddOptionalTagged_mC8FC6F6C6C812D32F98AF6F07565E4766884F4AE(L_3, (bool)0, 0, L_4, NULL);
		Asn1EncodableVector_t57A80683315794E6BD14B3D2368B95FE29304EC3* L_5 = L_3;
		GeneralNames_t9B20892C17FAA17729EE7445077B67D79DFEF3F4* L_6 = __this->___mCACerts;
		NullCheck(L_5);
		Asn1EncodableVector_AddOptionalTagged_mC8FC6F6C6C812D32F98AF6F07565E4766884F4AE(L_5, (bool)0, 1, L_6, NULL);
		DerSequence_tCC88E8724998083FAABF65D386A19D9C8C88C962* L_7 = (DerSequence_tCC88E8724998083FAABF65D386A19D9C8C88C962*)il2cpp_codegen_object_new(DerSequence_tCC88E8724998083FAABF65D386A19D9C8C88C962_il2cpp_TypeInfo_var);
		DerSequence__ctor_mDB684D58E08E4FC02FB853471F375D26B19CDC85(L_7, L_5, NULL);
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F* AnssiNamedCurves_ConfigureBasepoint_m23FC190A3A09918EA0D3EBF2432977A6C1F67D8E (ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* ___0_curve, String_t* ___1_encoding, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Hex_tE1EF40FEEC6279861917D4DFB9CCC75FD8B36CA1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WNafUtilities_tBF72FF38EF1BD4A84B2D2430BEE2D65AFB43891E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* L_0 = ___0_curve;
		String_t* L_1 = ___1_encoding;
		il2cpp_codegen_runtime_class_init_inline(Hex_tE1EF40FEEC6279861917D4DFB9CCC75FD8B36CA1_il2cpp_TypeInfo_var);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_2;
		L_2 = Hex_DecodeStrict_m070A7B6DEACA2BECD080ECC83ED8DE72EF84F217(L_1, NULL);
		X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F* L_3 = (X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F*)il2cpp_codegen_object_new(X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F_il2cpp_TypeInfo_var);
		X9ECPoint__ctor_m639E182F441B11BD4B1301E82C901A6073E72C25(L_3, L_0, L_2, NULL);
		X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F* L_4 = L_3;
		NullCheck(L_4);
		ECPoint_t0CE8D3D2A088BBE0E67B3DD87EA91AA0956659BC* L_5;
		L_5 = X9ECPoint_get_Point_m9FC99A8641D07923532F54F0E272B7285CEB5694(L_4, NULL);
		il2cpp_codegen_runtime_class_init_inline(WNafUtilities_tBF72FF38EF1BD4A84B2D2430BEE2D65AFB43891E_il2cpp_TypeInfo_var);
		WNafUtilities_ConfigureBasepoint_mD7F39A6175127FBBF54CE3CB0EBADFF04E806E64(L_5, NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* AnssiNamedCurves_ConfigureCurve_mCCAFFB8A60199F7058A130F49FFA793F570E773B (ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* ___0_curve, const RuntimeMethod* method) 
{
	{
		ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* L_0 = ___0_curve;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* AnssiNamedCurves_FromHex_m4E2FF2B5B36A33F1D17D4A84FADAE91B35B28DD5 (String_t* ___0_hex, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Hex_tE1EF40FEEC6279861917D4DFB9CCC75FD8B36CA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___0_hex;
		il2cpp_codegen_runtime_class_init_inline(Hex_tE1EF40FEEC6279861917D4DFB9CCC75FD8B36CA1_il2cpp_TypeInfo_var);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1;
		L_1 = Hex_DecodeStrict_m070A7B6DEACA2BECD080ECC83ED8DE72EF84F217(L_0, NULL);
		BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* L_2 = (BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB*)il2cpp_codegen_object_new(BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB_il2cpp_TypeInfo_var);
		BigInteger__ctor_m83A164BAE780C5E2F0C4BB5EA61D82252D713BA1(L_2, 1, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnssiNamedCurves_DefineCurve_m082910EE3B85F2F0CA0341F884761A0960D7532F (String_t* ___0_name, DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___1_oid, X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* ___2_holder, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_m60955C5A4F99886288BAED2052459C790963A0A5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_m6E2846ACFDB0C08FCDF5CA041846F3DD94AE525F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_mE8A0BC751794ED911A168EEC22705B6AE1281D52_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37* L_0 = ((AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_StaticFields*)il2cpp_codegen_static_fields_for(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var))->___objIds;
		String_t* L_1 = ___0_name;
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_2 = ___1_oid;
		NullCheck(L_0);
		Dictionary_2_Add_m60955C5A4F99886288BAED2052459C790963A0A5(L_0, L_1, L_2, Dictionary_2_Add_m60955C5A4F99886288BAED2052459C790963A0A5_RuntimeMethod_var);
		Dictionary_2_tCAD1DB714FE7CE64DFBD12C9AC15D2C23DDC6329* L_3 = ((AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_StaticFields*)il2cpp_codegen_static_fields_for(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var))->___names;
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_4 = ___1_oid;
		String_t* L_5 = ___0_name;
		NullCheck(L_3);
		Dictionary_2_Add_m6E2846ACFDB0C08FCDF5CA041846F3DD94AE525F(L_3, L_4, L_5, Dictionary_2_Add_m6E2846ACFDB0C08FCDF5CA041846F3DD94AE525F_RuntimeMethod_var);
		Dictionary_2_t182C3AD5398AA6215D318D59CC83975DFFD64E6F* L_6 = ((AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_StaticFields*)il2cpp_codegen_static_fields_for(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var))->___curves;
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_7 = ___1_oid;
		X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* L_8 = ___2_holder;
		NullCheck(L_6);
		Dictionary_2_Add_mE8A0BC751794ED911A168EEC22705B6AE1281D52(L_6, L_7, L_8, Dictionary_2_Add_mE8A0BC751794ED911A168EEC22705B6AE1281D52_RuntimeMethod_var);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnssiNamedCurves__cctor_mBEBAB5A65DFF4BA30BC528DF91318876D0FE340D (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnssiObjectIdentifiers_t844F7B339921BED06CC1D1EE7670EB2FAAE8CD2B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m3C0AEA796137AC0AE0830766066A09DB45BA1FA8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m73D6F97375AB4C5B363D6E7F565EAAEBC8194468_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mAB97FFC5A0BBFBF98AFCEE0A10E07EB1D77736FC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t182C3AD5398AA6215D318D59CC83975DFFD64E6F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tCAD1DB714FE7CE64DFBD12C9AC15D2C23DDC6329_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringComparer_t6268F19CA34879176651429C0D8A3D0002BB8E06_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE7AAE942B604FEEFC2327E2399341CDCAD41B668);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(StringComparer_t6268F19CA34879176651429C0D8A3D0002BB8E06_il2cpp_TypeInfo_var);
		StringComparer_t6268F19CA34879176651429C0D8A3D0002BB8E06* L_0;
		L_0 = StringComparer_get_OrdinalIgnoreCase_m071AA1B1747345CCA058A3879EBDEBBA2EA4B169_inline(NULL);
		Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37* L_1 = (Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37*)il2cpp_codegen_object_new(Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m73D6F97375AB4C5B363D6E7F565EAAEBC8194468(L_1, L_0, Dictionary_2__ctor_m73D6F97375AB4C5B363D6E7F565EAAEBC8194468_RuntimeMethod_var);
		((AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_StaticFields*)il2cpp_codegen_static_fields_for(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var))->___objIds = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&((AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_StaticFields*)il2cpp_codegen_static_fields_for(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var))->___objIds), (void*)L_1);
		Dictionary_2_t182C3AD5398AA6215D318D59CC83975DFFD64E6F* L_2 = (Dictionary_2_t182C3AD5398AA6215D318D59CC83975DFFD64E6F*)il2cpp_codegen_object_new(Dictionary_2_t182C3AD5398AA6215D318D59CC83975DFFD64E6F_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mAB97FFC5A0BBFBF98AFCEE0A10E07EB1D77736FC(L_2, Dictionary_2__ctor_mAB97FFC5A0BBFBF98AFCEE0A10E07EB1D77736FC_RuntimeMethod_var);
		((AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_StaticFields*)il2cpp_codegen_static_fields_for(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var))->___curves = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_StaticFields*)il2cpp_codegen_static_fields_for(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var))->___curves), (void*)L_2);
		Dictionary_2_tCAD1DB714FE7CE64DFBD12C9AC15D2C23DDC6329* L_3 = (Dictionary_2_tCAD1DB714FE7CE64DFBD12C9AC15D2C23DDC6329*)il2cpp_codegen_object_new(Dictionary_2_tCAD1DB714FE7CE64DFBD12C9AC15D2C23DDC6329_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3C0AEA796137AC0AE0830766066A09DB45BA1FA8(L_3, Dictionary_2__ctor_m3C0AEA796137AC0AE0830766066A09DB45BA1FA8_RuntimeMethod_var);
		((AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_StaticFields*)il2cpp_codegen_static_fields_for(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var))->___names = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&((AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_StaticFields*)il2cpp_codegen_static_fields_for(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var))->___names), (void*)L_3);
		il2cpp_codegen_runtime_class_init_inline(AnssiObjectIdentifiers_t844F7B339921BED06CC1D1EE7670EB2FAAE8CD2B_il2cpp_TypeInfo_var);
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_4 = ((AnssiObjectIdentifiers_t844F7B339921BED06CC1D1EE7670EB2FAAE8CD2B_StaticFields*)il2cpp_codegen_static_fields_for(AnssiObjectIdentifiers_t844F7B339921BED06CC1D1EE7670EB2FAAE8CD2B_il2cpp_TypeInfo_var))->___FRP256v1;
		il2cpp_codegen_runtime_class_init_inline(Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791_il2cpp_TypeInfo_var);
		X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* L_5 = ((Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791_StaticFields*)il2cpp_codegen_static_fields_for(Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791_il2cpp_TypeInfo_var))->___Instance;
		AnssiNamedCurves_DefineCurve_m082910EE3B85F2F0CA0341F884761A0960D7532F(_stringLiteralE7AAE942B604FEEFC2327E2399341CDCAD41B668, L_4, L_5, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88* AnssiNamedCurves_GetByName_mF638DE9BA49C2C05DAE4700625EE7FDC2CB82D2C (String_t* ___0_name, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* V_0 = NULL;
	{
		String_t* L_0 = ___0_name;
		il2cpp_codegen_runtime_class_init_inline(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_1;
		L_1 = AnssiNamedCurves_GetOid_m21DBA9A8777060CD3A3DE8D0A451EFB834CF043D(L_0, NULL);
		V_0 = L_1;
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0011;
		}
	}
	{
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_3 = V_0;
		il2cpp_codegen_runtime_class_init_inline(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88* L_4;
		L_4 = AnssiNamedCurves_GetByOid_mC6F9F11A2AE97D821CFC48E54B748475C8502CBC(L_3, NULL);
		return L_4;
	}

IL_0011:
	{
		return (X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88*)NULL;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* AnssiNamedCurves_GetByNameLazy_mBF6DC5456BBE6BE3977F044E8C9C7EE7A0CA38B1 (String_t* ___0_name, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* V_0 = NULL;
	{
		String_t* L_0 = ___0_name;
		il2cpp_codegen_runtime_class_init_inline(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_1;
		L_1 = AnssiNamedCurves_GetOid_m21DBA9A8777060CD3A3DE8D0A451EFB834CF043D(L_0, NULL);
		V_0 = L_1;
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0011;
		}
	}
	{
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_3 = V_0;
		il2cpp_codegen_runtime_class_init_inline(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* L_4;
		L_4 = AnssiNamedCurves_GetByOidLazy_m4DFC69EBB4D13926DD809D68AA67428BAA4408AE(L_3, NULL);
		return L_4;
	}

IL_0011:
	{
		return (X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76*)NULL;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88* AnssiNamedCurves_GetByOid_mC6F9F11A2AE97D821CFC48E54B748475C8502CBC (DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___0_oid, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* G_B2_0 = NULL;
	X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* G_B1_0 = NULL;
	{
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_0 = ___0_oid;
		il2cpp_codegen_runtime_class_init_inline(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* L_1;
		L_1 = AnssiNamedCurves_GetByOidLazy_m4DFC69EBB4D13926DD809D68AA67428BAA4408AE(L_0, NULL);
		X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* L_2 = L_1;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_000c;
		}
		G_B1_0 = L_2;
	}
	{
		return (X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88*)NULL;
	}

IL_000c:
	{
		NullCheck(G_B2_0);
		X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88* L_3;
		L_3 = X9ECParametersHolder_get_Parameters_m92C356231D52FD7985E656E342AFF03AEB1F86B5(G_B2_0, NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* AnssiNamedCurves_GetByOidLazy_m4DFC69EBB4D13926DD809D68AA67428BAA4408AE (DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___0_oid, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CollectionUtilities_GetValueOrNull_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_TisX9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76_mFFF6119751EFFE0F4BFE1CEABE6B98DE4BB7887D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		Dictionary_2_t182C3AD5398AA6215D318D59CC83975DFFD64E6F* L_0 = ((AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_StaticFields*)il2cpp_codegen_static_fields_for(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var))->___curves;
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_1 = ___0_oid;
		X9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76* L_2;
		L_2 = CollectionUtilities_GetValueOrNull_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_TisX9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76_mFFF6119751EFFE0F4BFE1CEABE6B98DE4BB7887D(L_0, L_1, CollectionUtilities_GetValueOrNull_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_TisX9ECParametersHolder_tB20AE011CC2B6B9BB8908135BF4184161A85FD76_mFFF6119751EFFE0F4BFE1CEABE6B98DE4BB7887D_RuntimeMethod_var);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AnssiNamedCurves_GetName_m8CCDAF0AF116C4028DE3142DB3FB8957B82892B3 (DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* ___0_oid, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CollectionUtilities_GetValueOrNull_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_TisString_t_mD7022EF1BC5D7215073BAB672FE85947718874EA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		Dictionary_2_tCAD1DB714FE7CE64DFBD12C9AC15D2C23DDC6329* L_0 = ((AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_StaticFields*)il2cpp_codegen_static_fields_for(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var))->___names;
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_1 = ___0_oid;
		String_t* L_2;
		L_2 = CollectionUtilities_GetValueOrNull_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_TisString_t_mD7022EF1BC5D7215073BAB672FE85947718874EA(L_0, L_1, CollectionUtilities_GetValueOrNull_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_TisString_t_mD7022EF1BC5D7215073BAB672FE85947718874EA_RuntimeMethod_var);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* AnssiNamedCurves_GetOid_m21DBA9A8777060CD3A3DE8D0A451EFB834CF043D (String_t* ___0_name, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CollectionUtilities_GetValueOrNull_TisString_t_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_mFE4804AAB5D611FAA5449945BFB5D25099CFCB29_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37* L_0 = ((AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_StaticFields*)il2cpp_codegen_static_fields_for(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var))->___objIds;
		String_t* L_1 = ___0_name;
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_2;
		L_2 = CollectionUtilities_GetValueOrNull_TisString_t_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_mFE4804AAB5D611FAA5449945BFB5D25099CFCB29(L_0, L_1, CollectionUtilities_GetValueOrNull_TisString_t_TisDerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_mFE4804AAB5D611FAA5449945BFB5D25099CFCB29_RuntimeMethod_var);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AnssiNamedCurves_get_Names_mA3A535F238C537BDA22978AACE2CE8B3B5C67337 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CollectionUtilities_Proxy_TisString_t_m6E2841A61A0D5E4BA6D3130680264F4BE597A573_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Keys_mCEE30023E7EA59ED772D73F6CB371999419CB44F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		Dictionary_2_t8710E3DD63AA54EFB9D92C543A559BD7D1DF7F37* L_0 = ((AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_StaticFields*)il2cpp_codegen_static_fields_for(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var))->___objIds;
		NullCheck(L_0);
		KeyCollection_tF8B8A6459322625D7F08C99265BDC0AACB1B6682* L_1;
		L_1 = Dictionary_2_get_Keys_mCEE30023E7EA59ED772D73F6CB371999419CB44F(L_0, Dictionary_2_get_Keys_mCEE30023E7EA59ED772D73F6CB371999419CB44F_RuntimeMethod_var);
		RuntimeObject* L_2;
		L_2 = CollectionUtilities_Proxy_TisString_t_m6E2841A61A0D5E4BA6D3130680264F4BE597A573(L_1, CollectionUtilities_Proxy_TisString_t_m6E2841A61A0D5E4BA6D3130680264F4BE597A573_RuntimeMethod_var);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Frp256v1Holder__ctor_m1D913952B061E660F7509F0DA5350EE2E0EF8D4E (Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791* __this, const RuntimeMethod* method) 
{
	{
		X9ECParametersHolder__ctor_mF94548A7AB4CD57DA7A0D87FE9AE4F966855F863(__this, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* Frp256v1Holder_CreateCurve_m6F16120DAAFDC15EC7B013CD84F80E9EB0CBEF7B (Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FpCurve_t9C4D086FD235C2976984476721DF8DEAF8BA86A1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1614BBC64F278A24D44F6470C16864678D530B54);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2309CB260B39EE33B2041CC894F06287D8CACD0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB608113BA5D284442B18D8F123B8242EFFD9B50E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD7926AF4A18D1FAF85D168A423D77A8BEF3439BF);
		s_Il2CppMethodInitialized = true;
	}
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* V_0 = NULL;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* V_1 = NULL;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* V_2 = NULL;
	BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* V_3 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* L_0;
		L_0 = AnssiNamedCurves_FromHex_m4E2FF2B5B36A33F1D17D4A84FADAE91B35B28DD5(_stringLiteralD7926AF4A18D1FAF85D168A423D77A8BEF3439BF, NULL);
		BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* L_1;
		L_1 = AnssiNamedCurves_FromHex_m4E2FF2B5B36A33F1D17D4A84FADAE91B35B28DD5(_stringLiteral1614BBC64F278A24D44F6470C16864678D530B54, NULL);
		V_0 = L_1;
		BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* L_2;
		L_2 = AnssiNamedCurves_FromHex_m4E2FF2B5B36A33F1D17D4A84FADAE91B35B28DD5(_stringLiteral2309CB260B39EE33B2041CC894F06287D8CACD0B, NULL);
		V_1 = L_2;
		BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* L_3;
		L_3 = AnssiNamedCurves_FromHex_m4E2FF2B5B36A33F1D17D4A84FADAE91B35B28DD5(_stringLiteralB608113BA5D284442B18D8F123B8242EFFD9B50E, NULL);
		V_2 = L_3;
		il2cpp_codegen_runtime_class_init_inline(BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB_il2cpp_TypeInfo_var);
		BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* L_4 = ((BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB_StaticFields*)il2cpp_codegen_static_fields_for(BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB_il2cpp_TypeInfo_var))->___One;
		V_3 = L_4;
		BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* L_5 = V_0;
		BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* L_6 = V_1;
		BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* L_7 = V_2;
		BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* L_8 = V_3;
		FpCurve_t9C4D086FD235C2976984476721DF8DEAF8BA86A1* L_9 = (FpCurve_t9C4D086FD235C2976984476721DF8DEAF8BA86A1*)il2cpp_codegen_object_new(FpCurve_t9C4D086FD235C2976984476721DF8DEAF8BA86A1_il2cpp_TypeInfo_var);
		FpCurve__ctor_m1937532A16F7AAA88DD89168AADDC86CDEFA4383(L_9, L_0, L_5, L_6, L_7, L_8, (bool)1, NULL);
		ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* L_10;
		L_10 = AnssiNamedCurves_ConfigureCurve_mCCAFFB8A60199F7058A130F49FFA793F570E773B(L_9, NULL);
		return L_10;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88* Frp256v1Holder_CreateParameters_mA3B1BA0197DC706179367802E28FA62A65B95B3F (Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB5C4768C151D248CB78691E1386433A0653C5D1F);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_0 = NULL;
	ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* V_1 = NULL;
	X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F* V_2 = NULL;
	{
		V_0 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)NULL;
		ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* L_0;
		L_0 = X9ECParametersHolder_get_Curve_m981F739459C722DAAC44B6B0CE7E217D3C47231B(__this, NULL);
		V_1 = L_0;
		ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* L_1 = V_1;
		il2cpp_codegen_runtime_class_init_inline(AnssiNamedCurves_t0A49B49F167A598208C524191E3641F9A1EBD6FA_il2cpp_TypeInfo_var);
		X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F* L_2;
		L_2 = AnssiNamedCurves_ConfigureBasepoint_m23FC190A3A09918EA0D3EBF2432977A6C1F67D8E(L_1, _stringLiteralB5C4768C151D248CB78691E1386433A0653C5D1F, NULL);
		V_2 = L_2;
		ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* L_3 = V_1;
		X9ECPoint_t506751DDE41BA558B548E162F8CA1D8878B8AB4F* L_4 = V_2;
		ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* L_5 = V_1;
		NullCheck(L_5);
		BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* L_6;
		L_6 = VirtualFuncInvoker0< BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* >::Invoke(27, L_5);
		ECCurve_tDA893F8DFA8C4DF154CDD23F6F2DD757A4159094* L_7 = V_1;
		NullCheck(L_7);
		BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* L_8;
		L_8 = VirtualFuncInvoker0< BigInteger_tFB347038E1DDB9646205451BA66ABB1CD7F5A5FB* >::Invoke(28, L_7);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_9 = V_0;
		X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88* L_10 = (X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88*)il2cpp_codegen_object_new(X9ECParameters_t84305FC36F5C703C99C89BE0516198F60D15FB88_il2cpp_TypeInfo_var);
		X9ECParameters__ctor_m3318A29AC6F8998915CFFF30E088B0FECEA6690D(L_10, L_3, L_4, L_6, L_8, L_9, NULL);
		return L_10;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Frp256v1Holder__cctor_mD6C35772AAABF85E038A81C103768394EB5BF7E2 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791* L_0 = (Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791*)il2cpp_codegen_object_new(Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791_il2cpp_TypeInfo_var);
		Frp256v1Holder__ctor_m1D913952B061E660F7509F0DA5350EE2E0EF8D4E(L_0, NULL);
		((Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791_StaticFields*)il2cpp_codegen_static_fields_for(Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791_il2cpp_TypeInfo_var))->___Instance = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791_StaticFields*)il2cpp_codegen_static_fields_for(Frp256v1Holder_t7DC039A92E856A33565AC2B3BA09FCE08107A791_il2cpp_TypeInfo_var))->___Instance), (void*)L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnssiObjectIdentifiers__ctor_m68DB8FE527A19135D6EA1FFFEC7D682EC6B95941 (AnssiObjectIdentifiers_t844F7B339921BED06CC1D1EE7670EB2FAAE8CD2B* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnssiObjectIdentifiers__cctor_m82EB4E452162AB09C678FF916AA236337161B68E (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnssiObjectIdentifiers_t844F7B339921BED06CC1D1EE7670EB2FAAE8CD2B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB387E0FFBFB7595C7AC8F04331A06CE8077E4BE9);
		s_Il2CppMethodInitialized = true;
	}
	{
		DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7* L_0 = (DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7*)il2cpp_codegen_object_new(DerObjectIdentifier_t44979315B19C7F4898F248CEE1CDFB2AC30B52F7_il2cpp_TypeInfo_var);
		DerObjectIdentifier__ctor_m53BD00BF63F38ED193D80D05618ED708AAC71B3F(L_0, _stringLiteralB387E0FFBFB7595C7AC8F04331A06CE8077E4BE9, NULL);
		((AnssiObjectIdentifiers_t844F7B339921BED06CC1D1EE7670EB2FAAE8CD2B_StaticFields*)il2cpp_codegen_static_fields_for(AnssiObjectIdentifiers_t844F7B339921BED06CC1D1EE7670EB2FAAE8CD2B_il2cpp_TypeInfo_var))->___FRP256v1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((AnssiObjectIdentifiers_t844F7B339921BED06CC1D1EE7670EB2FAAE8CD2B_StaticFields*)il2cpp_codegen_static_fields_for(AnssiObjectIdentifiers_t844F7B339921BED06CC1D1EE7670EB2FAAE8CD2B_il2cpp_TypeInfo_var))->___FRP256v1), (void*)L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Asn1TaggedObject_get_TagNo_m7763F3E854407E34E059C9F23D35E1F92B72183F_inline (Asn1TaggedObject_t4172254B2BA64C9B954F304147C2E057FCE20DD2* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___m_tagNo;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR StringComparer_t6268F19CA34879176651429C0D8A3D0002BB8E06* StringComparer_get_OrdinalIgnoreCase_m071AA1B1747345CCA058A3879EBDEBBA2EA4B169_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringComparer_t6268F19CA34879176651429C0D8A3D0002BB8E06_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(StringComparer_t6268F19CA34879176651429C0D8A3D0002BB8E06_il2cpp_TypeInfo_var);
		OrdinalIgnoreCaseComparer_t8BAE11990A4C855D3BCBBFB42F4EF8D45088FBB0* L_0 = ((StringComparer_t6268F19CA34879176651429C0D8A3D0002BB8E06_StaticFields*)il2cpp_codegen_static_fields_for(StringComparer_t6268F19CA34879176651429C0D8A3D0002BB8E06_il2cpp_TypeInfo_var))->___s_ordinalIgnoreCase;
		return L_0;
	}
}
